package
{
	import com.milkmangames.nativeextensions.AdMob;
	import com.milkmangames.nativeextensions.AdMobAdType;
	import com.milkmangames.nativeextensions.AdMobAlignment;
	import com.milkmangames.nativeextensions.events.AdMobErrorEvent;
	import com.milkmangames.nativeextensions.events.AdMobEvent;
	import com.slotim.slotim.ui.common.components.base.BaseBG;
	
	import flash.events.MouseEvent;
	import flash.text.TextField;
	
	public class AdMobBanner extends BaseBG
	{
		private var _counter:int = 0;
		private var _textField:TextField;
		
		public function AdMobBanner()
		{
			super(400, 400, 0x00ff00);
			
			_textField = new TextField();
			_textField.width = 400;
			_textField.height = 400;
			_textField.addEventListener(MouseEvent.CLICK, onClick);
			addChild(_textField);
			
			if(AdMob.isSupported)
			{
				AdMob.init("ca-app-pub-6845341395144613/7723835887");
				
				AdMob.addEventListener(AdMobEvent.RECEIVED_AD, onReceiveAd);
				AdMob.addEventListener(AdMobEvent.SCREEN_PRESENTED, onScreenPresented);
				AdMob.addEventListener(AdMobEvent.SCREEN_DISMISSED, onScreenDismissed);
				AdMob.addEventListener(AdMobEvent.LEAVE_APPLICATION, onLeaveApplication);
				AdMob.addEventListener(AdMobErrorEvent.FAILED_TO_RECEIVE_AD, onFailedReceiveAd);
				
				Write("AdMob done");
			}
			else
			{
				Write("AdMob won't work on this platform!");
				Write("AdMob won't work on this platform!");
				return;
			}
		}
		
		private function onClick(event:MouseEvent):void
		{
			_counter++;
			if (_counter > 5)
			{
				_counter = 1;
			}
			
			AdMob.destroyAd();
			switch (_counter)
			{
				case 1:
					AdMob.showAd(AdMobAdType.BANNER, AdMobAlignment.CENTER, AdMobAlignment.BOTTOM);
					Write(String(AdMobAdType.getPixelSize(AdMobAdType.BANNER).width) + "-" + String(AdMobAdType.getPixelSize(AdMobAdType.BANNER).height));
					break;
				case 2:
					AdMob.showAd(AdMobAdType.IAB_BANNER, AdMobAlignment.CENTER, AdMobAlignment.BOTTOM);
					Write(String(AdMobAdType.getPixelSize(AdMobAdType.IAB_BANNER).width) + "-" + String(AdMobAdType.getPixelSize(AdMobAdType.IAB_BANNER).height));
					break;
				case 3:
					AdMob.showAd(AdMobAdType.IAB_LEADERBOARD, AdMobAlignment.CENTER, AdMobAlignment.BOTTOM);
					Write(String(AdMobAdType.getPixelSize(AdMobAdType.IAB_LEADERBOARD).width) + "-" + String(AdMobAdType.getPixelSize(AdMobAdType.IAB_LEADERBOARD).height));
					break;
				case 4:
					AdMob.showAd(AdMobAdType.IAB_MRECT, AdMobAlignment.CENTER, AdMobAlignment.BOTTOM);
					Write(String(AdMobAdType.getPixelSize(AdMobAdType.IAB_MRECT).width) + "-" + String(AdMobAdType.getPixelSize(AdMobAdType.IAB_MRECT).height));
					break;
				case 5:
					AdMob.showAd(AdMobAdType.SMART_BANNER, AdMobAlignment.CENTER, AdMobAlignment.BOTTOM);
					Write(String(AdMobAdType.getPixelSize(AdMobAdType.SMART_BANNER).width) + "-" + String(AdMobAdType.getPixelSize(AdMobAdType.SMART_BANNER).height));
					break;
			}
		}
		
		private function Write(text:String):void
		{
			_textField.text = text + "\n" + _textField.text;
		}
		
		private function onReceiveAd(e:AdMobEvent):void
		{
			Write("ad has loaded!");
		}
		private function onScreenPresented(e:AdMobEvent):void
		{
			Write("modal is showing...you might want to pause or stop sound here.");
		}
		private function onScreenDismissed(e:AdMobEvent):void
		{
			Write("modal closed. You might want to resume the game here if paused.");
		}
		private function onLeaveApplication(e:AdMobEvent):void
		{
			Write("ad took user out of app.");
		}
		private function onFailedReceiveAd(e:AdMobErrorEvent):void
		{
			Write("Ad failed to load.");
		}
	}
}