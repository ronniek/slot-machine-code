package
{
	import com.slotim.slotim.BaseSlotim;
	import com.slotim.slotim.utils.consts.AppHostsEnum;
	import com.slotim.slotim.utils.consts.Consts;
	
	[SWF(backgroundColor='#8330ba')]
	public class SlotimAmazon extends BaseSlotim
	{
		// class
		public function SlotimAmazon()
		{
			super();
			
			if (Consts.APP_HOST != AppHostsEnum.AMAZON)
			{
				throw new Error("Need to change compiler directives in project build settings to SLOTS::AMAZON.");
			}
		}
	}
}