package
{
	import com.slotim.slotim.BaseSlotim;
	import com.slotim.slotim.utils.consts.AppHostsEnum;
	import com.slotim.slotim.utils.consts.Consts;
	
	[SWF(backgroundColor='#8330ba')]
	public class SlotimGooglePlay extends BaseSlotim
	{
		// class
		public function SlotimGooglePlay()
		{
			super();
			
			if (Consts.APP_HOST != AppHostsEnum.GOOGLE_PLAY)
			{
				throw new Error("Need to change compiler directives in project build settings to SLOTS::GOOGLE_PLAY.");
			}
		}
	}
}