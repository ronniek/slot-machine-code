package
{
	import com.slotim.slotim.BaseSlotim;
	import com.slotim.slotim.utils.consts.AppHostsEnum;
	import com.slotim.slotim.utils.consts.Consts;
	
	[SWF(backgroundColor='#8330ba')]
	public class SlotimITunes extends BaseSlotim
	{
		// class
		public function SlotimITunes()
		{
			super();
			
			if (Consts.APP_HOST != AppHostsEnum.ITUNES)
			{
				throw new Error("Need to change compiler directives in project build settings to SLOTS::ITUNES.");
			}
		}
	}
}