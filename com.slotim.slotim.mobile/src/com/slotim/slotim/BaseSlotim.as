package com.slotim.slotim
{
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.ui.common.components.SpriteEx;
	import com.slotim.slotim.ui.main.MainPanel;
	import com.slotim.slotim.ui.notifications.NotificationsHandler;
	import com.slotim.slotim.ui.notifications.popup.connect.ConnectPopup;
	
	import flash.desktop.NativeApplication;
	import flash.desktop.SystemIdleMode;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	
	public class BaseSlotim extends SpriteEx
	{
		// class
		public function BaseSlotim()
		{
			//MonsterDebugger.initialize(this);
			//stage.align = StageAlign.TOP_LEFT;
			//stage.scaleMode = StageScaleMode.NO_SCALE;
			//stage.frameRate = 60;
			//Main.Instance.Init(stage);
			
			//return;
			super();
			
			CONFIG::DEBUG
				{
					MonsterDebugger.initialize(this);
				}
				
				Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT;
			
			Main.Instance.Init(stage);
			Main.Instance.XServices.Init();
			
			Main.Instance.XServices.GoogleAnalytics.TrackMainView();
			
			NotificationsHandler.Instance;
			
			//NativeApplication.nativeApplication.addEventListener(Event.ACTIVATE, OnActivate);
			//NativeApplication.nativeApplication.addEventListener(Event.DEACTIVATE, OnDeactivate);
			NativeApplication.nativeApplication.addEventListener(Event.NETWORK_CHANGE, OnNetworkChange);
			NativeApplication.nativeApplication.addEventListener(KeyboardEvent.KEY_DOWN, OnKeyDown);
			NativeApplication.nativeApplication.systemIdleMode = SystemIdleMode.KEEP_AWAKE;
			
			this.addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
		}
		
		// add to stage
		protected function OnAddedToStage(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			
			addChild(new MainPanel());
			
			// register require ui so register after ui is ready
			Main.Instance.XServices.LocalNotifications.Register();
			
			CONFIG::DEBUG
				{
					//addChild(new PerformanceMonitor());
					//addChild(LogPanel.Instance);
				}
				
				NotificationsHandler.Instance.ShowTimerBonusLocalNotificationPopup();
			
			Main.Instance.XServices.BookAdvisor.DoRequests();
		}
		
		// events
		protected function OnNetworkChange(event:Event):void
		{
			Main.Instance.XServices.InternetTime.UpdateMS();
		}
		protected function OnKeyDown(event:KeyboardEvent):void
		{
			switch (event.keyCode)
			{
				case Keyboard.BACK:
					event.preventDefault();
					if (NotificationsHandler.Instance.ShowingNotifications)
					{
						if (NotificationsHandler.Instance.NotificationType is ConnectPopup)
						{
							// don't close ConnectPopup by back button
						}
						else
						{
							NotificationsHandler.Instance.RemoveNotification();
						}
					}
					else if (Main.Instance.ActiveMachine)
					{
						Main.Instance.RemoveActiveMachine();
					}
					else
					{
						event.preventDefault();
						NotificationsHandler.Instance.ShowPromptOnClosePopup(null);
					}
					break;
				case Keyboard.MENU:
					event.preventDefault();
					NotificationsHandler.Instance.ShowMenuSettingsPopup(null);
					break;
			}
		}
	}
}