package com.slotim.slotim.assets.notifications.popup.buyChipsLevels
{
	public class BuyChipsLevelsEmbed
	{
		[Embed(source="ChipsLevelFree.png")] public static var ChipsLevelFree:Class;
		[Embed(source="ChipsLevel1.png")] public static var ChipsLevel1:Class;
		[Embed(source="ChipsLevel2.png")] public static var ChipsLevel2:Class;
		[Embed(source="ChipsLevel3.png")] public static var ChipsLevel3:Class;
		[Embed(source="ChipsLevel4.png")] public static var ChipsLevel4:Class;
	}
}