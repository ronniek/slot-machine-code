package com.slotim.slotim.data.application
{
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.data.application.appsList.AppsListData;
	import com.slotim.slotim.data.application.locale.L10NHandler;
	import com.slotim.slotim.data.application.message.MessageData;
	import com.slotim.slotim.data.application.prices.PricesData;
	import com.slotim.slotim.data.application.schedulers.SchedulersData;
	import com.slotim.slotim.data.application.ticker.TickerData;
	import com.slotim.slotim.data.machine.spline.SplinesData;
	import com.slotim.slotim.ui.notifications.NotificationsHandler;
	import com.slotim.slotim.utils.dataAdapter.api.GetMessageApi;
	import com.slotim.slotim.utils.dataAdapter.manager.DataAdapterHandler;
	
	import flash.events.Event;
	
	public class ApplicationData
	{
		// members
		private var _dataAdapter:DataAdapterHandler;
		private var _allowedBetsChips:Vector.<Number>;
		
		private var _message:MessageData;
		private var _appsList:AppsListData;
		private var _l10n:L10NHandler;
		private var _prices:PricesData;
		private var _ticker:TickerData;
		private var _schedulers:SchedulersData;
		private var _splines:SplinesData;
		
		// properties
		public function get Message():MessageData
		{
			return _message;
		}
		public function get AppsList():AppsListData
		{
			return _appsList;
		}
		public function get L10N():L10NHandler
		{
			return _l10n;
		}
		public function get Prices():PricesData
		{
			return _prices;
		}
		public function get Ticker():TickerData
		{
			return _ticker;
		}
		public function get Schedulers():SchedulersData
		{
			return _schedulers;
		}
		public function get Splines():SplinesData
		{
			return _splines;
		}
		
		public function get EnableJackpot():Boolean
		{
			return true;
		}
		public function get EnableTicker():Boolean
		{
			return true;
		}
		public function get FountainsLimit():int
		{
			return 30;
		}
		public function get EnableAppsList():Boolean
		{
			return false;
		}
		
		public function get EnableSpinButtonBit():Boolean
		{
			return true;
		}
		
		public function get AllowedBetsChips():Vector.<Number>
		{
			return _allowedBetsChips;
		}
		
		// class
		public function ApplicationData()
		{
			_message = new MessageData();
			_appsList = new AppsListData();
			_l10n = new L10NHandler();
			_prices = new PricesData();
			_ticker = new TickerData();
			_schedulers = new SchedulersData();
			_splines = new SplinesData();
			
			Update();
			InitAllowedBetChips();
		}
		private function InitAllowedBetChips():void
		{
			_allowedBetsChips = new Vector.<Number>();
			_allowedBetsChips.push(0.1);
			_allowedBetsChips.push(0.25);
			_allowedBetsChips.push(0.5);
			_allowedBetsChips.push(1);
			_allowedBetsChips.push(2);
			_allowedBetsChips.push(5);
			_allowedBetsChips.push(10);
			_allowedBetsChips.push(20);
			_allowedBetsChips.push(50);
			_allowedBetsChips.push(100);
			_allowedBetsChips.push(200);
			_allowedBetsChips.push(500);
			_allowedBetsChips.push(1000);
			_allowedBetsChips.push(2000);
			_allowedBetsChips.push(5000);
			_allowedBetsChips.push(10000);
			_allowedBetsChips.push(20000);
			_allowedBetsChips.push(50000);
			_allowedBetsChips.push(100000);
		}
		public function Dispose():void
		{
			while (_allowedBetsChips && _allowedBetsChips.length > 0)
			{
				_allowedBetsChips.pop();
			}
			_allowedBetsChips = null;
		}
		
		// methods
		private function Update():void
		{
			_dataAdapter = new DataAdapterHandler();
			_dataAdapter.AddApi(new GetMessageApi());
			_dataAdapter.addEventListener(Event.COMPLETE, OnDataAdapterComplete);
			_dataAdapter.addEventListener(DataAdapterHandler.ServerError, OnDataAdapterServerError);
			_dataAdapter.Execute();
		}
		public function GetBetIndexByBetChips(betChips:Number):int
		{
			for (var i:int = 0; i < _allowedBetsChips.length; i++)
			{
				if (_allowedBetsChips[i] == betChips)
				{
					return i;
				}
			}
			
			return 0;
		}
		
		// events
		protected function OnDataAdapterComplete(event:Event):void
		{
			try
			{
				_dataAdapter.removeEventListener(DataAdapterHandler.ServerError, OnDataAdapterServerError);
				_dataAdapter.removeEventListener(Event.COMPLETE, OnDataAdapterComplete);
				
				_message = _dataAdapter.GetResponse(GetMessageApi.URL);
				
				if (_message.active)
				{
					NotificationsHandler.Instance.ShowMessagePopup(Main.Instance.Application.Message.title, Main.Instance.Application.Message.body);
				}
			}
			catch (error:Error)
			{
			}
		}
		protected function OnDataAdapterServerError(event:Event):void
		{
			_dataAdapter.removeEventListener(DataAdapterHandler.ServerError, OnDataAdapterServerError);
			_dataAdapter.removeEventListener(Event.COMPLETE, OnDataAdapterComplete);
		}
	}
}