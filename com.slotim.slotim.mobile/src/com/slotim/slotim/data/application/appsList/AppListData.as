package com.slotim.slotim.data.application.appsList
{
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.utils.consts.DeviceTypeEnum;
	import com.slotim.slotim.utils.dataType.BaseIDData;
	import com.slotim.slotim.ui.machine.Symbol;
	
	import flash.display.Bitmap;

	public class AppListData extends BaseIDData
	{
		// members
		private var _description:String;
		private var _iTunes:String;
		private var _googlePlay:String;
		private var _largePhotoClass:Class;
		private var _largePhotoPng:Symbol;
		private var _self:Boolean;
		private var _active:Boolean;
		
		// properties
		public function get Description():String
		{
			return _description;
		}
		public function get URL():String
		{
			var url:String;
			
			switch (Main.Instance.Device.DeviceType)
			{
				case DeviceTypeEnum.ANDORID:
					url = _googlePlay;
					break;
				case DeviceTypeEnum.IPHONE:
					url = _iTunes;
					break;
				default:
					url = _googlePlay;
			}
			
			return url;
		}
		public function get LargePhotoPng():Bitmap
		{
			return new _largePhotoClass();
		}
		public function get Self():Boolean
		{
			return _self;
		}
		public function get Active():Boolean
		{
			return _active;
		}
		
		// class
		public function AppListData(id:int, description:String, iTunes:String, googlePlay:String, largePhotoClass:Class, self:Boolean, active:Boolean)
		{
			super(id);
			
			_description = description;
			_iTunes = iTunes;
			_googlePlay = googlePlay;
			_largePhotoClass = largePhotoClass;
			_self = self;
			_active = active;
		}
	}
}