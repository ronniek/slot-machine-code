package com.slotim.slotim.data.application.appsList
{
	import com.slotim.slotim.assets.appsList.AppsListEmbed;
	import com.slotim.slotim.utils.ex.EventDispatcherEx;

	public class AppsListData extends EventDispatcherEx
	{
		// memebrs
		private var _apps:Vector.<AppListData>;
		
		// properties
		public function get Apps():Vector.<AppListData>
		{
			return _apps;
		}
		public function get Count():Number
		{
			var result:Number = 0;

			if (_apps)
			{
				for (var i:int = 0; i < _apps.length; i++)
				{
					if (_apps[i].Active && !_apps[i].Self)
					{
						result++;
					}
				}
			}

			return result;
		}
		
		// class
		public function AppsListData()
		{
			super();

			_apps = new Vector.<AppListData>();
			_apps.push(new AppListData(0, "Slotim",
				"https://itunes.apple.com/us/app/slotim/id795214708?mt=8",
				"https://play.google.com/store/apps/details?id=air.com.slotim.slotim&hl=en",
				AppsListEmbed.Slotim_LargePhoto, // 398, 246
				false, true));
		}
		
		// methods
		public function Show():void
		{
			dispatchEvent(new AppsListEvent(AppsListEvent.Show));
		}
		public function Hide():void
		{
			dispatchEvent(new AppsListEvent(AppsListEvent.Hide));
		}
	}
}