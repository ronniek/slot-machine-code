package com.slotim.slotim.data.application.appsList
{
	import flash.events.Event;

	public class AppsListEvent extends Event
	{
		// events
		public static const Show:String = "ca9d932ddad1414b97fb715f961d8294";
		public static const Hide:String = "f0770066a32e48f08603275069d743cb";
		
		// class
		public function AppsListEvent(type:String)
		{
			super(type, false, false);
		}
	}
}