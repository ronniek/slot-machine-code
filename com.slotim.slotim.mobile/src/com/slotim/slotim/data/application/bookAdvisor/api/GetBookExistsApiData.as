package com.slotim.slotim.data.application.bookAdvisor.api
{
	import com.slotim.slotim.data.application.bookAdvisor.base.BaseData;
	
	public class GetBookExistsApiData extends BaseData
	{
		// consts
		public static const FALSE:String = "FALSE";
		
		// members
		public var exists:String;
		
		// class
		public function GetBookExistsApiData()
		{
		}
	}
}