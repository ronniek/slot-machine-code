package com.slotim.slotim.data.application.bookAdvisor.api
{
	import com.slotim.slotim.data.application.bookAdvisor.base.BaseData;
	
	public class GetDuplicateLinksApiData extends BaseData
	{
		// members
		public var duplicateLinks:String;
		
		// class
		public function GetDuplicateLinksApiData()
		{
		}
	}
}