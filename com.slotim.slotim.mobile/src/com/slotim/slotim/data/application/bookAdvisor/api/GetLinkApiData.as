package com.slotim.slotim.data.application.bookAdvisor.api
{
	import com.slotim.slotim.data.application.bookAdvisor.base.BaseData;
	
	public class GetLinkApiData extends BaseData
	{
		// memebrs
		public var LNK_ID:String;
		public var LNK_STATUS_ID:String;
		public var LNK_SITE_ID:String;
		public var LNK_ISBN:String;
		public var LNK_LINK:String;
		
		// class
		public function GetLinkApiData()
		{
		}
	}
}