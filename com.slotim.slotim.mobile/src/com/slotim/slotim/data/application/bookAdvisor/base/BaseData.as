package com.slotim.slotim.data.application.bookAdvisor.base
{
	import com.slotim.slotim.utils.error.NotImplementedError;
	import com.slotim.slotim.utils.ex.EventDispatcherEx;
	
	public class BaseData extends EventDispatcherEx
	{
		// consts
		public static const OK:String = "OK";
		public static const Failed:String = "Failed";
		
		// members
		public var ResultMessage:String;
		
		// properties
		public function get SQL():String
		{
			throw new NotImplementedError();
			
			//			var sql:String = "";
			//			sql += "INSERT INTO T_BOOK_CATEGORY";
			//			sql += "(";
			//			sql += "`BCY_BOOK_ID`,";
			//			sql += "`BCY_NAME`,";
			//			sql += "`BCY_LINK`,";
			//			sql += "`BCY_AMAZON_SALES_RANK`";
			//			sql += ")";
			//			sql += "VALUES";
			//			sql += "(";
			//			sql += "'" + bookID + "',";
			//			sql += "'" + this.name + "',";
			//			sql += "'" + this.link + "',";
			//			sql += "'" + this.amazonSalesRank + "'";
			//			sql += ");";
			//			return sql;
		}
		
		// class
		public function BaseData()
		{
		}
		
		// methods
		protected function Escape(string:String):String
		{
			try
			{
				return string.split("'").join("f5a2e736166f4a6e8f82cacaaa0d0722");
				return string.split('"').join("74414e1dc7cd413bb7d55a2a0a970707");
			}
			catch (error:Error)
			{
				return "";
			}
		}
	}
}