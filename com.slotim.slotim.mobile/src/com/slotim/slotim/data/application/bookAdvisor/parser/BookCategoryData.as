package com.slotim.slotim.data.application.bookAdvisor.parser
{
	import com.slotim.slotim.data.application.bookAdvisor.base.BaseData;
	
	public class BookCategoryData extends BaseData
	{
		// members
		public var book:BookData;
		public var parentName:String;
		public var name:String;
		public var link:String;
		public var amazonSalesRank:String;
		
		// properties
		public override function get SQL():String
		{
			var sql:String = "";
			sql += "INSERT IGNORE INTO T_BOOK_CATEGORY";
			sql += "(";
			sql += "`BCY_BOOK_ID`,";
			sql += "`BCY_PARENT_NAME`,";
			sql += "`BCY_NAME`,";
			sql += "`BCY_LINK`,";
			sql += "`BCY_AMAZON_SALES_RANK`";
			sql += ")";
			sql += "VALUES";
			sql += "(";
			sql += this.book.id + ",";
			sql += "'" + Escape(this.parentName) + "',";
			sql += "'" + Escape(this.name) + "',";
			sql += "'" + Escape(this.link) + "',";
			sql += "'" + this.amazonSalesRank + "'";
			sql += ");";
			return sql;
		}
		
		// class
		public function BookCategoryData(book:BookData, parentName:String, name:String, link:String, amazonSalesRank:String)
		{
			this.book = book;
			this.parentName = parentName;
			this.name = name;
			this.link = link;
			this.amazonSalesRank = amazonSalesRank;
		}
	}
}