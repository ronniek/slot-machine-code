package com.slotim.slotim.data.application.bookAdvisor.parser
{
	import com.slotim.slotim.data.application.bookAdvisor.base.BaseData;
	
	public class BookContributorData extends BaseData
	{
		// members
		public var book:BookData;
		public var name:String;
		public var type:String;
		
		// properties
		public override function get SQL():String
		{
			var sql:String = "";
			sql += "INSERT IGNORE INTO T_BOOK_CONTRIBUTOR";
			sql += "(";
			sql += "`BCR_BOOK_ID`,";
			sql += "`BCR_NAME`,";
			sql += "`BCR_TYPE`";
			sql += ")";
			sql += "VALUES";
			sql += "(";
			sql += this.book.id + ",";
			sql += "'" + Escape(this.name) + "',";
			sql += "'" + Escape(this.type) + "'";
			sql += ");";
			return sql;
		}
		
		// class
		public function BookContributorData(book:BookData, name:String, type:String)
		{
			this.book = book;
			this.name = name;
			this.type = type;
		}
	}
}