package com.slotim.slotim.data.application.bookAdvisor.parser
{
	public class BookContributorEnumType
	{
		public static const Author:String = "Author";
		public static const Illustrator:String = "Illustrator";
	}
}