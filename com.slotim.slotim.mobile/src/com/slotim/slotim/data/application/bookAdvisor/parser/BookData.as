package com.slotim.slotim.data.application.bookAdvisor.parser
{
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.data.application.bookAdvisor.api.GetBookExistsApiData;
	import com.slotim.slotim.data.application.bookAdvisor.api.GetDuplicateLinksApiData;
	import com.slotim.slotim.data.application.bookAdvisor.api.PostInsertIntoApiData;
	import com.slotim.slotim.data.application.bookAdvisor.base.BaseData;
	import com.slotim.slotim.utils.dataAdapter.api.bookAdvisor.GetBookExistsApi;
	import com.slotim.slotim.utils.dataAdapter.api.bookAdvisor.GetDuplicateLinksApi;
	import com.slotim.slotim.utils.dataAdapter.api.bookAdvisor.PostInsertIntoApi;
	import com.slotim.slotim.utils.dataAdapter.api.bookAdvisor.PostMultipleInsertIntoApi;
	import com.slotim.slotim.utils.dataAdapter.manager.DataAdapterHandler;
	
	import flash.events.Event;
	
	public class BookData extends BaseData
	{
		// members
		public var id:int;
		public var name:String;
		public var link:String;
		public var publishDate:String;
		public var averageScore:String;
		public var averageScoreOf:String;
		public var customerReviewsCount:String;
		public var description:String;
		public var ageRange:String;
		public var gradeLevel:String;
		public var lexileMeasure:String;
		public var pagesCount:String;
		public var publisher:String;
		public var edition:String;
		public var language:String;
		public var isbn10:String;
		public var isbn13:String;
		public var weight:String;
		public var formats:Vector.<BookFormatData>;
		private var categories:Vector.<BookCategoryData>;
		public var contributors:Vector.<BookContributorData>;
		private var relatedBooks:Vector.<BookRelatedBookData>;
		public var photos:Vector.<BookPhotoData>;
		
		// properties
		public override function get SQL():String
		{
			var sql:String = "";
			sql += "INSERT IGNORE INTO T_BOOK";
			sql += "(";
			sql += "`BOK_NAME`,";
			sql += "`BOK_LINK`,";
			sql += "`BOK_PUBLISH_DATE`,";
			sql += "`BOK_AVERAGE_SCORE`,";
			sql += "`BOK_AVERAGE_SCORE_OF`,";
			sql += "`BOK_CUSTOMER_REVIEWS_COUNT`,";
			sql += "`BOK_DESCRIPTION`,";
			sql += "`BOK_AGE_RANGE`,";
			sql += "`BOK_GRADE_LEVEL`,";
			sql += "`BOK_LEXILE_MEASURE`,";
			sql += "`BOK_PAGES_COUNT`,";
			sql += "`BOK_PUBLISHER`,";
			sql += "`BOK_EDITION`,";
			sql += "`BOK_LANGUAGE`,";
			sql += "`BOK_ISBN10`,";
			sql += "`BOK_ISBN13`,";
			sql += "`BOK_WEIGHT`,";
			sql += "`BOK_APP_VERSION`";
			sql += ")";
			sql += "VALUES";
			sql += "(";
			sql += "'" + Escape(this.name) + "',";
			sql += "'" + Escape(this.link) + "',";
			sql += "'" + Escape(this.publishDate) + "',";
			sql += "'" + this.averageScore + "',";
			sql += "'" + this.averageScoreOf + "',";
			sql += "'" + this.customerReviewsCount + "',";
			sql += "'" + Escape(this.description) + "',";
			sql += "'" + Escape(this.ageRange) + "',";
			sql += "'" + Escape(this.gradeLevel) + "',";
			sql += "'" + this.lexileMeasure + "',";
			sql += "'" + this.pagesCount + "',";
			sql += "'" + Escape(this.publisher) + "',";
			sql += "'" + Escape(this.edition) + "',";
			sql += "'" + Escape(this.language) + "',";
			sql += "'" + this.isbn10 + "',";
			sql += "'" + this.isbn13 + "',";
			sql += "'" + Escape(this.weight) + "',";
			sql += "'" + Main.Instance.CurrentVersionNumber + "'";
			sql += ");";
			return sql;
		}
		
		// class
		public function BookData()
		{
			super();
			
			this.id = -1;
			this.formats = new Vector.<BookFormatData>();
			this.categories = new Vector.<BookCategoryData>();
			this.contributors = new Vector.<BookContributorData>();
			this.relatedBooks = new Vector.<BookRelatedBookData>();
			this.photos = new Vector.<BookPhotoData>();
		}
		public override function Dispose():void
		{
			if (photos)
			{
				while (photos.length > 0)
				{
					photos.pop();
				}
				photos = null;
			}
			if (formats)
			{
				while (formats.length > 0)
				{
					formats.pop();
				}
				formats = null;
			}
			if (categories)
			{
				while (categories.length > 0)
				{
					categories.pop();
				}
				categories = null;
			}
			if (contributors)
			{
				while (contributors.length > 0)
				{
					contributors.pop();
				}
				contributors = null;
			}
			if (relatedBooks)
			{
				while (relatedBooks.length > 0)
				{
					relatedBooks.pop();
				}
				relatedBooks = null;
			}
		}
		
		// methods
		public function AddCategoryBook(categoryBook:BookCategoryData):void
		{
			for (var i:int = 0; i < categories.length; i++)
			{
				if (categories[i].name == categoryBook.name)
				{
					return;
				}
			}
			
			categories.push(categoryBook);
		}
		public function AddRelatedBook(relatedBook:BookRelatedBookData):void
		{
			if (this.isbn10 == relatedBook.isbn)
			{
				return;
			}
			
			for (var i:int = 0; i < relatedBooks.length; i++)
			{
				if (relatedBooks[i].isbn == relatedBook.isbn)
				{
					return;
				}
			}
			
			relatedBooks.push(relatedBook);
		}
		
		public function CheckBookExists():void
		{
			var sql:String = "SELECT `BOK_ID` FROM `T_BOOK` WHERE BOK_ISBN10 = '" + this.isbn10 + "' OR BOK_ISBN13 = '" + this.isbn13 + "';";
			
			var dataAdapter:DataAdapterHandler = new DataAdapterHandler();
			dataAdapter.AddApi(new GetBookExistsApi(sql));
			dataAdapter.addEventListener(Event.COMPLETE, OnPostBookExistsComplete);
			dataAdapter.addEventListener(DataAdapterHandler.ServerError, OnPostBookExistsServerError);
			dataAdapter.Execute();
		}
		protected function OnPostBookExistsComplete(event:Event):void
		{
			try
			{
				var dataAdapter:DataAdapterHandler = DataAdapterHandler(event.currentTarget);
				var getBookExistsApi:GetBookExistsApiData = dataAdapter.GetResponse(GetBookExistsApi.URL);
				if (getBookExistsApi.ResultMessage == BaseData.OK && getBookExistsApi.exists == GetBookExistsApiData.FALSE)
				{
					PostNewBook();
				}
				else
				{
					dispatchEvent(new BookDataEvent(BookDataEvent.Duplicate));
				}
			}
			catch (error:Error)
			{
				dispatchEvent(new BookDataEvent(BookDataEvent.Failed));
			}
			finally
			{
				dataAdapter.Dispose();
			}
		}
		protected function OnPostBookExistsServerError(event:Event):void
		{
			var dataAdapter:DataAdapterHandler = DataAdapterHandler(event.currentTarget);
			dataAdapter.Dispose();
			dispatchEvent(new BookDataEvent(BookDataEvent.Failed));
		}
		
		private function PostNewBook():void
		{
			var dataAdapter:DataAdapterHandler = new DataAdapterHandler();
			dataAdapter.AddApi(new PostInsertIntoApi(SQL));
			dataAdapter.addEventListener(Event.COMPLETE, OnPostNewBookComplete);
			dataAdapter.addEventListener(DataAdapterHandler.ServerError, OnPostNewBookServerError);
			dataAdapter.Execute();
		}
		protected function OnPostNewBookComplete(event:Event):void
		{
			try
			{
				var dataAdapter:DataAdapterHandler = DataAdapterHandler(event.currentTarget);
				
				var postInsertIntoApi:PostInsertIntoApiData = dataAdapter.GetResponse(PostInsertIntoApi.URL);
				if (postInsertIntoApi.ResultMessage == BaseData.OK)
				{
					id = int(postInsertIntoApi.NewID);
					
					Main.Instance.XServices.GoogleAnalytics.TrackBookAdvisorBookAdded();
					
					PostBookDetails();
				}
				else
				{
					dispatchEvent(new BookDataEvent(BookDataEvent.Failed));
				}
			}
			catch (error:Error)
			{
				dispatchEvent(new BookDataEvent(BookDataEvent.Failed));
			}
			finally
			{
				dataAdapter.Dispose();
			}
		}
		protected function OnPostNewBookServerError(event:Event):void
		{
			var dataAdapter:DataAdapterHandler = DataAdapterHandler(event.currentTarget);
			dataAdapter.Dispose();
			dispatchEvent(new BookDataEvent(BookDataEvent.Failed));
		}
		
		public function PostBookDetails():void
		{
			var sql:String = "";
			while (categories.length > 0)
			{
				sql += categories.pop().SQL + "\r\n";
			}
			while (contributors.length > 0)
			{
				sql += contributors.pop().SQL + "\r\n";
			}
			while (formats.length > 0)
			{
				sql += formats.pop().SQL + "\r\n";
			}
			while (photos.length > 0)
			{
				sql += photos.pop().SQL + "\r\n";
			}
			
			var dataAdapter:DataAdapterHandler = new DataAdapterHandler();
			dataAdapter.AddApi(new PostMultipleInsertIntoApi(sql));
			dataAdapter.addEventListener(Event.COMPLETE, OnPostBookDetailsComplete);
			dataAdapter.addEventListener(DataAdapterHandler.ServerError, OnPostBookDetailsServerError);
			dataAdapter.Execute();
		}
		protected function OnPostBookDetailsComplete(event:Event):void
		{
			var dataAdapter:DataAdapterHandler = DataAdapterHandler(event.currentTarget);
			dataAdapter.Dispose();
			GetDuplicateLinks();
		}
		protected function OnPostBookDetailsServerError(event:Event):void
		{
			var dataAdapter:DataAdapterHandler = DataAdapterHandler(event.currentTarget);
			dataAdapter.Dispose();
			GetDuplicateLinks();
		}
		
		public function GetDuplicateLinks():void
		{
			var sql:String = "SELECT `LNK_ISBN` FROM `T_LINK` WHERE LNK_ISBN in (";
			for (var i:int = 0; i < relatedBooks.length; i++)
			{
				sql += "'" + relatedBooks[i].isbn + "',";
			}
			sql += "'')";
			
			var dataAdapter:DataAdapterHandler = new DataAdapterHandler();
			dataAdapter.AddApi(new GetDuplicateLinksApi(sql));
			dataAdapter.addEventListener(Event.COMPLETE, OnGetDuplicateLinksComplete);
			dataAdapter.addEventListener(DataAdapterHandler.ServerError, OnGetDuplicateLinksServerError);
			dataAdapter.Execute();
		}
		protected function OnGetDuplicateLinksComplete(event:Event):void
		{
			try
			{
				var dataAdapter:DataAdapterHandler = DataAdapterHandler(event.currentTarget);
				var duplicateLinksApi:GetDuplicateLinksApiData = dataAdapter.GetResponse(GetDuplicateLinksApi.URL);
				if (duplicateLinksApi.ResultMessage == BaseData.OK)
				{
					var a_duplicateLinks:Array = duplicateLinksApi.duplicateLinks.split(",");
					while (a_duplicateLinks.length > 0)
					{
						var isbn:String = a_duplicateLinks.pop();
						for (var i:int = 0; i < relatedBooks.length; i++)
						{
							if (relatedBooks[i].isbn == isbn)
							{
								relatedBooks.removeAt(i);
								break;
							}
						}
					}
					
					PostNewLinks();
				}
				else
				{
					PostNewLinks();
				}
			}
			catch (error:Error)
			{
				PostNewLinks();
			}
			finally
			{
				dataAdapter.Dispose();
			}
		}
		protected function OnGetDuplicateLinksServerError(event:Event):void
		{
			var dataAdapter:DataAdapterHandler = DataAdapterHandler(event.currentTarget);
			dataAdapter.Dispose();
			PostNewLinks();
		}
		
		public function PostNewLinks():void
		{
			var sql:String = "";
			while (relatedBooks.length > 0)
			{
				sql += relatedBooks.pop().SQL + "\r\n";
			}
			
			var dataAdapter:DataAdapterHandler = new DataAdapterHandler();
			dataAdapter.AddApi(new PostMultipleInsertIntoApi(sql));
			dataAdapter.addEventListener(Event.COMPLETE, OnPostNewLinksComplete);
			dataAdapter.addEventListener(DataAdapterHandler.ServerError, OnPostNewLinksServerError);
			dataAdapter.Execute();
		}
		protected function OnPostNewLinksComplete(event:Event):void
		{
			var dataAdapter:DataAdapterHandler = DataAdapterHandler(event.currentTarget);
			dataAdapter.Dispose();
			
			Main.Instance.XServices.GoogleAnalytics.TrackBookAdvisorBookCompleted();
			
			dispatchEvent(new BookDataEvent(BookDataEvent.Complete));
		}
		protected function OnPostNewLinksServerError(event:Event):void
		{
			var dataAdapter:DataAdapterHandler = DataAdapterHandler(event.currentTarget);
			dataAdapter.Dispose();
			dispatchEvent(new BookDataEvent(BookDataEvent.Failed));
		}
	}
}