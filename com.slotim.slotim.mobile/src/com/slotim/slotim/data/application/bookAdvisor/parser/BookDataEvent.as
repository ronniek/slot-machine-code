package com.slotim.slotim.data.application.bookAdvisor.parser
{
	import flash.events.Event;
	
	public class BookDataEvent extends Event
	{
		// consts
		public static const Complete:String = "14629acc8a8a43d2b0614a131cd94a70";
		public static const Failed:String = "eb661a0fbdc54e3baa9f7478e7d57920";
		public static const Duplicate:String = "627c25158f65402393eef20fee1f70dd";
		
		// class
		public function BookDataEvent(type:String)
		{
			super(type);
		}
	}
}