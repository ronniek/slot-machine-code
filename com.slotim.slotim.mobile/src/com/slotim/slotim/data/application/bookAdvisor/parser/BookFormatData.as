package com.slotim.slotim.data.application.bookAdvisor.parser
{
	import com.slotim.slotim.data.application.bookAdvisor.base.BaseData;
	
	public class BookFormatData extends BaseData
	{
		// members
		public var book:BookData;
		public var name:String;
		public var link:String;
		
		// properties
		public override function get SQL():String
		{
			var sql:String = "";
			sql += "INSERT IGNORE INTO T_BOOK_FORMAT";
			sql += "(";
			sql += "`BFT_BOOK_ID`,";
			sql += "`BFT_NAME`,";
			sql += "`BFT_LINK`";
			sql += ")";
			sql += "VALUES";
			sql += "(";
			sql += this.book.id + ",";
			sql += "'" + Escape(this.name) + "',";
			sql += "'" + Escape(this.link) + "'";
			sql += ");";
			return sql;
		}
		
		// class
		public function BookFormatData(book:BookData, name:String, link:String)
		{
			this.book = book;
			this.name = name;
			this.link = link;
		}
	}
}