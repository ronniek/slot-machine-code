package com.slotim.slotim.data.application.bookAdvisor.parser
{
	import com.slotim.slotim.data.application.bookAdvisor.base.BaseData;
	
	public class BookPhotoData extends BaseData
	{
		// members
		public var book:BookData;
		public var link:String;
		
		// properties
		public override function get SQL():String
		{
			var sql:String = "";
			sql += "INSERT IGNORE INTO T_BOOK_PHOTO";
			sql += "(";
			sql += "`BPO_BOOK_ID`,";
			sql += "`BPO_LINK`";
			sql += ")";
			sql += "VALUES";
			sql += "(";
			sql += this.book.id + ",";
			sql += "'" + Escape(this.link) + "'";
			sql += ");";
			return sql;
		}
		
		// class
		public function BookPhotoData(book:BookData, link:String)
		{
			this.book = book;
			this.link = link;
		}
	}
}