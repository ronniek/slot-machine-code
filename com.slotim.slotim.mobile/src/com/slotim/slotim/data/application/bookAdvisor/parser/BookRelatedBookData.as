package com.slotim.slotim.data.application.bookAdvisor.parser
{
	import com.slotim.slotim.data.application.bookAdvisor.base.BaseData;
	
	public class BookRelatedBookData extends BaseData
	{
		// members
		public var book:BookData;
		public var relatedBookEnum:String;
		public var link:String;
		public var isbn:String;
		
		// properties
		public function get SiteID():int
		{
			if (link.toLowerCase().indexOf("amazon") >= 0)
			{
				return 1;
			}
			else if (link.toLowerCase().indexOf("barnesandnobel") >= 0)
			{
				return 2;
			}
			else
			{
				return 0;
			}
		}
		// methods
		public override function get SQL():String
		{
			var sql:String = "";
			sql += "INSERT IGNORE INTO T_LINK";
			sql += "(";
			sql += "`LNK_BOOK_ID`,";
			sql += "`LNK_STATUS_ID`,";
			sql += "`LNK_SITE_ID`,";
			sql += "`LNK_TYPE_ID`,";
			sql += "`LNK_ISBN`,";
			sql += "`LNK_LINK`";
			sql += ")";
			sql += "VALUES";
			sql += "(";
			sql += this.book.id + ",";
			sql += "1,";
			sql += SiteID + ",";
			sql += this.relatedBookEnum + ",";
			sql += "'" + this.isbn + "',";
			sql += "'" + Escape(this.link) + "'";
			sql += ");";
			return sql;
		}
		
		// class
		public function BookRelatedBookData(book:BookData, relatedBookEnum:String, link:String, isbn:String)
		{
			this.book = book;
			this.relatedBookEnum = relatedBookEnum;
			this.link = link;
			this.isbn = isbn;
		}
	}
}