package com.slotim.slotim.data.application.bookAdvisor.parser
{
	public class BookRelatedBookEnumType
	{
		public static const FrequentlyBoughtTogether:String = "1";
		public static const OtherFormats:String = "2";
		public static const CustomersWhoBoughtThisItemAlsoBought:String = "3";
		public static const OtherItemsCustomersBuyAfterViewingThisItem:String = "4";
	}
}