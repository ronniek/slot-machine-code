package com.slotim.slotim.data.application.prices
{
	import com.slotim.slotim.utils.dataType.BaseIDData;
	import com.slotim.slotim.utils.helpers.FormatterHelper;
	
	public class PriceOptionData extends BaseIDData
	{
		// members
		private var _productID:String;
		private var _chips:Number;
		
		// properties
		public function get ProductID():String
		{
			return _productID;
		}
		public function get Chips():Number
		{
			return _chips;
		}
		public function get ChipsAsMoney():String
		{
			return FormatterHelper.NumberToMoney(_chips, 0, 0);
		}
		
		// class
		public function PriceOptionData(id:int, productID:String, chips:Number)
		{
			super(id);
			
			_productID = productID;
			_chips = chips;
		}
	}
}