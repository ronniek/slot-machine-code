package com.slotim.slotim.data.application.prices
{
	import com.slotim.slotim.data.application.schedulers.scheduler.base.BaseScheduler;
	import com.slotim.slotim.utils.dataType.BaseIDData;
	
	public class PriceOptionsData extends BaseIDData
	{
		// memebrs
		private var _description:String;
		private var _priceOptions:Vector.<PriceOptionData>;
		private var _scheduler:BaseScheduler;
		
		// properties
		public function get Description():String
		{
			return _description;
		}
		public function get PriceOptions():Vector.<PriceOptionData>
		{
			return _priceOptions;
		}
		public function get FreePriceOption():PriceOptionData
		{
			return _priceOptions[0];
		}
		public function get Scheduler():BaseScheduler
		{
			return _scheduler;
		}
		
		// class
		public function PriceOptionsData(id:int, description:String, 
										 productID0:String, chips0:Number,
										 productID1:String, chips1:Number,
										 productID2:String, chips2:Number,
										 productID3:String, chips3:Number,
										 productID4:String, chips4:Number,
										 scheduler:BaseScheduler)
		{
			super(id);
			
			_description = description;
			_priceOptions = new Vector.<PriceOptionData>();
			_priceOptions.push(new PriceOptionData(0, productID0, 2500));			// chips0));
			_priceOptions.push(new PriceOptionData(1, productID1, chips1));
			_priceOptions.push(new PriceOptionData(2, productID2, chips2));
			_priceOptions.push(new PriceOptionData(3, productID3, chips3));
			_priceOptions.push(new PriceOptionData(4, productID4, chips4));
			_scheduler = scheduler;
		}
		public override function Dispose():void
		{
			super.Dispose();
			
			while (_priceOptions && _priceOptions.length > 0)
			{
				var priceOption:PriceOptionData = _priceOptions.pop();
				priceOption.Dispose();
				priceOption = null;
			}
			_priceOptions = null;
			
			if (_scheduler)
			{
				_scheduler.Dispose();
				_scheduler = null;
			}
		}
	}
}