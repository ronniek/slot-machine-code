package com.slotim.slotim.data.application.prices
{
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.data.application.schedulers.scheduler.AprilFoolsDayScheduler;
	import com.slotim.slotim.data.application.schedulers.scheduler.BlackFridayScheduler;
	import com.slotim.slotim.data.application.schedulers.scheduler.BoxingDayScheduler;
	import com.slotim.slotim.data.application.schedulers.scheduler.ChristmasScheduler;
	import com.slotim.slotim.data.application.schedulers.scheduler.ColumbusDayScheduler;
	import com.slotim.slotim.data.application.schedulers.scheduler.CyberMondayScheduler;
	import com.slotim.slotim.data.application.schedulers.scheduler.DefaultScheduler;
	import com.slotim.slotim.data.application.schedulers.scheduler.EasterScheduler;
	import com.slotim.slotim.data.application.schedulers.scheduler.EveryMondayScheduler;
	import com.slotim.slotim.data.application.schedulers.scheduler.EverySundayScheduler;
	import com.slotim.slotim.data.application.schedulers.scheduler.FifteenMinutesBeforeEndOfDayScheduler;
	import com.slotim.slotim.data.application.schedulers.scheduler.FifteenMinutesBeforeNoonScheduler;
	import com.slotim.slotim.data.application.schedulers.scheduler.FirstDayOfAutumn;
	import com.slotim.slotim.data.application.schedulers.scheduler.FirstDayOfSpring;
	import com.slotim.slotim.data.application.schedulers.scheduler.FirstDayOfSummer;
	import com.slotim.slotim.data.application.schedulers.scheduler.FirstDayOfTheMonthScheduler;
	import com.slotim.slotim.data.application.schedulers.scheduler.FirstDayOfWinter;
	import com.slotim.slotim.data.application.schedulers.scheduler.FourDaysBeforeEndOfTheMonth;
	import com.slotim.slotim.data.application.schedulers.scheduler.HalloweenScheduler;
	import com.slotim.slotim.data.application.schedulers.scheduler.IndependenceDayScheduler;
	import com.slotim.slotim.data.application.schedulers.scheduler.MothersDayScheduler;
	import com.slotim.slotim.data.application.schedulers.scheduler.NewYearScheduler;
	import com.slotim.slotim.data.application.schedulers.scheduler.ThanksGivingDayScheduler;
	import com.slotim.slotim.data.application.schedulers.scheduler.ThirtyMinutesFollowingTenScheduler;
	import com.slotim.slotim.data.application.schedulers.scheduler.ValentinesScheduler;
	import com.slotim.slotim.utils.ex.EventDispatcherEx;
	
	public class PricesData extends EventDispatcherEx
	{
		// memebrs
		private var _pricesOptions:Vector.<PriceOptionsData>;
		private var _defaultPriceOptions:PriceOptionsData;
		
		// properties
		private function get AddFifteenMinutesBeforeEndOfDayPriceOptions():PriceOptionsData {return new PriceOptionsData(1, "15 minutes to Midnight", ProductID.USD_FREE, 90000, ProductID.USD_001, 30000, ProductID.USD_005, 193000, ProductID.USD_010, 464000, ProductID.USD_020, 1241000, new FifteenMinutesBeforeEndOfDayScheduler());}
		private function get AddFifteenMinutesBeforeNoonPriceOptions():PriceOptionsData {return new PriceOptionsData(1, "15 minutes to Noon", ProductID.USD_FREE, 135000, ProductID.USD_001, 45000, ProductID.USD_020, 1871000, ProductID.USD_050, 8009000, ProductID.USD_100, 27497000, new FifteenMinutesBeforeNoonScheduler());}
		private function get AddThirtyMinutesFollowingTenPriceOptions():PriceOptionsData {return new PriceOptionsData(1, "22:00 - 22:30", ProductID.USD_FREE, 217500, ProductID.USD_001, 72500, ProductID.USD_010, 1108000, ProductID.USD_020, 3015000, ProductID.USD_050, 12940000, new ThirtyMinutesFollowingTenScheduler());}
		private function get AddAprilFoolsDayPriceOptions():PriceOptionsData {return new PriceOptionsData(2, "April Fools Sale", ProductID.USD_FREE, 54000, ProductID.USD_001, 18000, ProductID.USD_005, 115000, ProductID.USD_010, 273000, ProductID.USD_020, 748000, new AprilFoolsDayScheduler());}
		private function get AddBlackFridayPriceOptions():PriceOptionsData {return new PriceOptionsData(2, "Black Friday", ProductID.USD_FREE, 120000, ProductID.USD_001, 40000, ProductID.USD_005, 256000, ProductID.USD_010, 611000, ProductID.USD_020, 1631000, new BlackFridayScheduler());}
		private function get AddBoxingDayPriceOptions():PriceOptionsData {return new PriceOptionsData(2, "Boxing Day", ProductID.USD_FREE, 135000, ProductID.USD_001, 45000, ProductID.USD_005, 290000, ProductID.USD_010, 687000, ProductID.USD_020, 1871000, new BoxingDayScheduler());}
		private function get AddChristmasPriceOptions():PriceOptionsData {return new PriceOptionsData(2, "Xmas Sale", ProductID.USD_FREE, 75000, ProductID.USD_001, 25000, ProductID.USD_005, 161000, ProductID.USD_010, 389000, ProductID.USD_020, 1034000, new ChristmasScheduler());}
		private function get AddColumbusDayPriceOptions():PriceOptionsData {return new PriceOptionsData(2, "Columbus Day", ProductID.USD_FREE, 120000, ProductID.USD_001, 40000, ProductID.USD_020, 1631000, ProductID.USD_050, 7200000, ProductID.USD_100, 24603000, new ColumbusDayScheduler());}
		private function get AddCyberMondayPriceOptions():PriceOptionsData {return new PriceOptionsData(2, "Cyber Monday", ProductID.USD_FREE, 165000, ProductID.USD_001, 55000, ProductID.USD_010, 835000, ProductID.USD_020, 2287000, ProductID.USD_050, 9844000, new CyberMondayScheduler());}
		private function get AddEasterPriceOptions():PriceOptionsData {return new PriceOptionsData(2, "Easter Sale", ProductID.USD_FREE, 66000, ProductID.USD_001, 22000, ProductID.USD_005, 139000, ProductID.USD_010, 342000, ProductID.USD_020, 901000, new EasterScheduler());}
		private function get AddHalloweenPriceOptions():PriceOptionsData {return new PriceOptionsData(2, "Halloween Sale", ProductID.USD_FREE, 165000, ProductID.USD_001, 55000, ProductID.USD_010, 840000, ProductID.USD_020, 2287000, ProductID.USD_050, 9927000, new HalloweenScheduler());}
		private function get AddIndependenceDayPriceOptions():PriceOptionsData {return new PriceOptionsData(2, "Independece Day", ProductID.USD_FREE, 60000, ProductID.USD_001, 20000, ProductID.USD_002, 45000, ProductID.USD_005, 126000, ProductID.USD_010, 303000, new IndependenceDayScheduler());}
		private function get AddMothersDayPriceOptions():PriceOptionsData {return new PriceOptionsData(2, "Mother Day", ProductID.USD_FREE, 120000, ProductID.USD_001, 40000, ProductID.USD_010, 607000, ProductID.USD_020, 1663000, ProductID.USD_050, 7179000, new MothersDayScheduler());}
		private function get AddNewYearPriceOptions():PriceOptionsData {return new PriceOptionsData(2, "New Year", ProductID.USD_FREE, 210000, ProductID.USD_001, 70000, ProductID.USD_010, 1083000, ProductID.USD_020, 2896000, ProductID.USD_050, 12494000, new NewYearScheduler());}
		private function get AddThanksGivingDayPriceOptions():PriceOptionsData {return new PriceOptionsData(2, "Thanks Giving Day", ProductID.USD_FREE, 72000, ProductID.USD_001, 24000, ProductID.USD_005, 152000, ProductID.USD_010, 371000, ProductID.USD_020, 998000, new ThanksGivingDayScheduler());}
		private function get AddValentinesPriceOptions():PriceOptionsData {return new PriceOptionsData(2, "Lovers Sale", ProductID.USD_FREE, 75000, ProductID.USD_001, 25000, ProductID.USD_005, 162000, ProductID.USD_010, 382000, ProductID.USD_020, 1039000, new ValentinesScheduler());}
		private function get AddFirstDayOfAutumn():PriceOptionsData {return new PriceOptionsData(3, "Automn", ProductID.USD_FREE, 45000, ProductID.USD_001, 15000, ProductID.USD_002, 33000, ProductID.USD_010, 233000, ProductID.USD_050, 2692000, new FirstDayOfAutumn());}
		private function get AddFirstDayOfSpring():PriceOptionsData {return new PriceOptionsData(3, "Spring", ProductID.USD_FREE, 54000, ProductID.USD_001, 18000, ProductID.USD_002, 39000, ProductID.USD_010, 278000, ProductID.USD_050, 3203000, new FirstDayOfSpring());}
		private function get AddFirstDayOfSummer():PriceOptionsData {return new PriceOptionsData(3, "Hot Season .. Hot Sale", ProductID.USD_FREE, 63000, ProductID.USD_001, 21000, ProductID.USD_002, 48000, ProductID.USD_010, 323000, ProductID.USD_050, 3737000, new FirstDayOfSummer());}
		private function get AddFirstDayOfWinter():PriceOptionsData {return new PriceOptionsData(3, "Wintertime", ProductID.USD_FREE, 66000, ProductID.USD_001, 22000, ProductID.USD_002, 49000, ProductID.USD_010, 338000, ProductID.USD_050, 3960000, new FirstDayOfWinter());}
		private function get AddEveryMondayPriceOptions():PriceOptionsData {return new PriceOptionsData(8, "Monday Sale", ProductID.USD_FREE, 150000, ProductID.USD_001, 50000, ProductID.USD_020, 2069000, ProductID.USD_050, 9025000, ProductID.USD_100, 30704000, new EveryMondayScheduler());}
		private function get AddEverySundayPriceOptions():PriceOptionsData {return new PriceOptionsData(8, "Sunday Sale", ProductID.USD_FREE, 75000, ProductID.USD_001, 25000, ProductID.USD_002, 56000, ProductID.USD_005, 162000, ProductID.USD_010, 389000, new EverySundayScheduler());}
		private function get AddFirstDayOfTheMonthPriceOptions():PriceOptionsData {return new PriceOptionsData(8, "1st day of the Month", ProductID.USD_FREE, 90000, ProductID.USD_001, 30000, ProductID.USD_002, 68000, ProductID.USD_005, 190000, ProductID.USD_010, 467000, new FirstDayOfTheMonthScheduler());}
		private function get AddFourDaysBeforeEndOfTheMonth():PriceOptionsData {return new PriceOptionsData(8, "End of the month", ProductID.USD_FREE, 60000, ProductID.USD_001, 20000, ProductID.USD_002, 45000, ProductID.USD_005, 127000, ProductID.USD_010, 305000, new FourDaysBeforeEndOfTheMonth());}
		private function get AddDefaultPriceOptions():PriceOptionsData {return new PriceOptionsData(99, "Free Chips", ProductID.USD_FREE, 45000, ProductID.USD_001, 15000, ProductID.USD_010, 230000, ProductID.USD_050, 2669000, ProductID.USD_100, 9196000, new DefaultScheduler());}
		
		// class
		public function PricesData()
		{
			_pricesOptions = new Vector.<PriceOptionsData>();
			_pricesOptions.push(AddFifteenMinutesBeforeEndOfDayPriceOptions);
			_pricesOptions.push(AddFifteenMinutesBeforeNoonPriceOptions);
			_pricesOptions.push(AddThirtyMinutesFollowingTenPriceOptions);
			_pricesOptions.push(AddAprilFoolsDayPriceOptions);
			_pricesOptions.push(AddBlackFridayPriceOptions);
			_pricesOptions.push(AddBoxingDayPriceOptions);
			_pricesOptions.push(AddChristmasPriceOptions);
			_pricesOptions.push(AddColumbusDayPriceOptions);
			_pricesOptions.push(AddCyberMondayPriceOptions);
			_pricesOptions.push(AddEasterPriceOptions);
			_pricesOptions.push(AddHalloweenPriceOptions);
			_pricesOptions.push(AddIndependenceDayPriceOptions);
			_pricesOptions.push(AddMothersDayPriceOptions);
			_pricesOptions.push(AddNewYearPriceOptions);
			_pricesOptions.push(AddThanksGivingDayPriceOptions);
			_pricesOptions.push(AddValentinesPriceOptions);
			_pricesOptions.push(AddFirstDayOfAutumn);
			_pricesOptions.push(AddFirstDayOfSpring);
			_pricesOptions.push(AddFirstDayOfSummer);
			_pricesOptions.push(AddFirstDayOfWinter);
			_pricesOptions.push(AddEveryMondayPriceOptions);
			_pricesOptions.push(AddEverySundayPriceOptions);
			_pricesOptions.push(AddFirstDayOfTheMonthPriceOptions);
			_pricesOptions.push(AddFourDaysBeforeEndOfTheMonth);
			_pricesOptions.push(AddDefaultPriceOptions);
			
			_defaultPriceOptions = AddDefaultPriceOptions;
			_pricesOptions.push(_defaultPriceOptions);
		}
		public override function Dispose():void
		{
			super.Dispose();
			
			while (_pricesOptions && _pricesOptions.length > 0)
			{
				var priceOptions:PriceOptionsData = _pricesOptions.pop();
				priceOptions.Dispose();
				priceOptions = null;
			}
			_pricesOptions = null;
			
			if (_defaultPriceOptions)
			{
				_defaultPriceOptions.Dispose();
				_defaultPriceOptions = null;
			}
		}
		
		// methods
		public function GetPriceOptions(onlyByCurrentMS:Boolean):PriceOptionsData
		{
			if (onlyByCurrentMS)
			{
				priceOptions = _defaultPriceOptions;
			}
			else
			{
				var priceOptions:PriceOptionsData = _defaultPriceOptions;
				
				for (var i:int = 0; i < _pricesOptions.length; i++)
				{
					if (_pricesOptions[i].Scheduler.IsInRange(Main.Instance.XServices.InternetTime.CurrentLocalDateTime))
					{
						priceOptions = _pricesOptions[i];
						break;
					}
				}
			}
			
			return priceOptions;
		}
	}
}