package com.slotim.slotim.data.application.prices
{
	import com.slotim.slotim.utils.helpers.FormatterHelper;
	
	public class ProductID
	{
		// consts
		public static const USD_FREE:String = "usd_free";
		public static const USD_001:String = "usd_001";
		public static const USD_002:String = "usd_002";
		public static const USD_005:String = "usd_005";
		public static const USD_010:String = "usd_010";
		public static const USD_020:String = "usd_020";
		public static const USD_050:String = "usd_050";
		public static const USD_100:String = "usd_100a";
		
		// properties
		public static function get ItemIDs():Vector.<String>
		{
			var itemIDs:Vector.<String> = new Vector.<String>();
			itemIDs.push(USD_FREE);
			itemIDs.push(USD_001);
			itemIDs.push(USD_002);
			itemIDs.push(USD_005);
			itemIDs.push(USD_010);
			itemIDs.push(USD_020);
			itemIDs.push(USD_050);
			itemIDs.push(USD_100);
			return itemIDs;
		}
		
		// methods
		public static function DescriptionByProductID(productID:String):String
		{
			switch (productID)
			{
				case USD_FREE:
					return "Free";
				case USD_001:
				case USD_002:
				case USD_005:
				case USD_010:
				case USD_020:
				case USD_050:
				case USD_100:
					return "$" + FormatterHelper.NumberToMoney(ProductID.PriceByProductID(productID));
				default:
					return "$0.99";
			}
		}
		public static function PriceByProductID(productID:String):Number
		{
			switch (productID)
			{
				case USD_FREE:
					return 0;
				case USD_001:
					return 0.99;
				case USD_002:
					return 1.99;
				case USD_005:
					return 4.99;
				case USD_010:
					return 9.99;
				case USD_020:
					return 19.99;
				case USD_050:
					return 49.99;
				case USD_100:
					return 99.99;
				default:
					return 0.99;
			}
		}
	}
}