package com.slotim.slotim.data.application.schedulers.scheduler
{
	import com.slotim.slotim.assets.hud.frame.HudFrameEmbed;
	import com.slotim.slotim.data.application.schedulers.ScheduleData;
	import com.slotim.slotim.data.application.schedulers.scheduler.base.BaseScheduler;
	
	public class AprilFoolsDayScheduler extends BaseScheduler
	{
		// properties
		public override function get FrameClass():Class
		{
			return HudFrameEmbed.AprilFoolsDay;
		}
		
		// class
		public function AprilFoolsDayScheduler()
		{
			super(new ScheduleData(Number.NaN, 4, 1), new ScheduleData(Number.NaN, 4, 1));
		}
	}
}