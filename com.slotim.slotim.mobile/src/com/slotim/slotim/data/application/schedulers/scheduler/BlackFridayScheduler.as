package com.slotim.slotim.data.application.schedulers.scheduler
{
	import com.slotim.slotim.assets.hud.frame.HudFrameEmbed;
	import com.slotim.slotim.data.application.schedulers.ScheduleData;
	import com.slotim.slotim.data.application.schedulers.scheduler.base.BaseScheduler;

	public class BlackFridayScheduler extends BaseScheduler
	{
		// properties
		public override function get FrameClass():Class
		{
			return HudFrameEmbed.BlackFriday;
		}
		
		// class
		public function BlackFridayScheduler()
		{
			super(new ScheduleData(2014, 11, 28), new ScheduleData(2014, 11, 28));
		}
	}
}