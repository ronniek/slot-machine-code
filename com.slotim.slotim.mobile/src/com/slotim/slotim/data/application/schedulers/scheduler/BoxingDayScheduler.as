package com.slotim.slotim.data.application.schedulers.scheduler
{
	import com.slotim.slotim.data.application.schedulers.ScheduleData;
	import com.slotim.slotim.data.application.schedulers.scheduler.base.BaseScheduler;

	public class BoxingDayScheduler extends BaseScheduler
	{
		// class
		public function BoxingDayScheduler()
		{
			super(new ScheduleData(Number.NaN, 12, 26), new ScheduleData(Number.NaN, 12, 26));
		}
	}
}