package com.slotim.slotim.data.application.schedulers.scheduler
{
	import com.slotim.slotim.assets.hud.frame.HudFrameEmbed;
	import com.slotim.slotim.data.application.schedulers.ScheduleData;
	import com.slotim.slotim.data.application.schedulers.scheduler.base.BaseScheduler;

	public class ChristmasScheduler extends BaseScheduler
	{
		// properties
		public override function get FrameClass():Class
		{
			return HudFrameEmbed.Christmas;
		}
		
		// class
		public function ChristmasScheduler()
		{
			super(new ScheduleData(Number.NaN, 12, 22), new ScheduleData(Number.NaN, 12, 25));
		}
	}
}