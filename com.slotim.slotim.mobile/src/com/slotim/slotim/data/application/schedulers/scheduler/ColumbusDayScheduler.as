package com.slotim.slotim.data.application.schedulers.scheduler
{
	import com.slotim.slotim.assets.hud.frame.HudFrameEmbed;
	import com.slotim.slotim.data.application.schedulers.ScheduleData;
	import com.slotim.slotim.data.application.schedulers.scheduler.base.BaseScheduler;

	public class ColumbusDayScheduler extends BaseScheduler
	{
		// properties
		public override function get FrameClass():Class
		{
			return HudFrameEmbed.ColumbusDay;
		}
		
		// class
		public function ColumbusDayScheduler()
		{
			super(new ScheduleData(Number.NaN, 10, 14), new ScheduleData(Number.NaN, 10, 14));
		}
	}
}