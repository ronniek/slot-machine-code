package com.slotim.slotim.data.application.schedulers.scheduler
{
	import com.slotim.slotim.data.application.schedulers.ScheduleData;
	import com.slotim.slotim.data.application.schedulers.scheduler.base.BaseScheduler;
	
	public class CyberMondayScheduler extends BaseScheduler
	{
		// class
		public function CyberMondayScheduler()
		{
			super(new ScheduleData(2014, 12, 1), new ScheduleData(2014, 12, 2));
		}
	}
}