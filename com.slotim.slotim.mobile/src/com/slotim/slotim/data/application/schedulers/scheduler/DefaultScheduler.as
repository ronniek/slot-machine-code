package com.slotim.slotim.data.application.schedulers.scheduler
{
	import com.slotim.slotim.data.application.schedulers.ScheduleData;
	import com.slotim.slotim.data.application.schedulers.scheduler.base.BaseScheduler;
	
	public class DefaultScheduler extends BaseScheduler
	{
		// class
		public function DefaultScheduler()
		{
			super(new ScheduleData(), new ScheduleData());
		}
	}
}