package com.slotim.slotim.data.application.schedulers.scheduler
{
	import com.slotim.slotim.assets.hud.frame.HudFrameEmbed;
	import com.slotim.slotim.data.application.schedulers.ScheduleData;
	import com.slotim.slotim.data.application.schedulers.scheduler.base.BaseScheduler;
	
	public class EasterScheduler extends BaseScheduler
	{
		// properties
		public override function get FrameClass():Class
		{
			return HudFrameEmbed.Easter;
		}
		
		// class
		public function EasterScheduler()
		{
			super(new ScheduleData(Number.NaN, 3, 21), new ScheduleData(Number.NaN, 3, 24));
		}
	}
}