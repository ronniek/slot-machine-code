package com.slotim.slotim.data.application.schedulers.scheduler
{
	import com.slotim.slotim.data.application.schedulers.ScheduleData;
	import com.slotim.slotim.data.application.schedulers.scheduler.base.BaseScheduler;
	
	public class EverySundayScheduler extends BaseScheduler
	{
		// class
		public function EverySundayScheduler()
		{
			super(new ScheduleData(Number.NaN, Number.NaN, Number.NaN, 1), new ScheduleData(Number.NaN, Number.NaN, Number.NaN, 1));
		}
	}
}