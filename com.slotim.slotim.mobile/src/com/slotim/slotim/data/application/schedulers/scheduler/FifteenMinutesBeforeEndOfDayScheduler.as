package com.slotim.slotim.data.application.schedulers.scheduler
{
	import com.slotim.slotim.data.application.schedulers.ScheduleData;
	import com.slotim.slotim.data.application.schedulers.scheduler.base.BaseScheduler;

	public class FifteenMinutesBeforeEndOfDayScheduler extends BaseScheduler
	{
		// class
		public function FifteenMinutesBeforeEndOfDayScheduler()
		{
			super(new ScheduleData(Number.NaN, Number.NaN, Number.NaN, Number.NaN, 23, 45), new ScheduleData(Number.NaN, Number.NaN, Number.NaN, Number.NaN, 23, 59));
		}
	}
}