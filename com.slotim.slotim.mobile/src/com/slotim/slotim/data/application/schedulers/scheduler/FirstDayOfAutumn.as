package com.slotim.slotim.data.application.schedulers.scheduler
{
	import com.slotim.slotim.assets.hud.frame.HudFrameEmbed;
	import com.slotim.slotim.data.application.schedulers.ScheduleData;
	import com.slotim.slotim.data.application.schedulers.scheduler.base.BaseScheduler;

	public class FirstDayOfAutumn extends BaseScheduler
	{
		// properties
		public override function get FrameClass():Class
		{
			return HudFrameEmbed.FirstDayOfAutumn;
		}
		
		// class
		public function FirstDayOfAutumn()
		{
			super(new ScheduleData(Number.NaN, 9, 22), new ScheduleData(Number.NaN, 9, 2));
		}
	}
}