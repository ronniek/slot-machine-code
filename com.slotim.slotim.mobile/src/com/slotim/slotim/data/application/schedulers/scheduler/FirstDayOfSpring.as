package com.slotim.slotim.data.application.schedulers.scheduler
{
	import com.slotim.slotim.assets.hud.frame.HudFrameEmbed;
	import com.slotim.slotim.data.application.schedulers.ScheduleData;
	import com.slotim.slotim.data.application.schedulers.scheduler.base.BaseScheduler;
	
	public class FirstDayOfSpring extends BaseScheduler
	{
		// properties
		public override function get FrameClass():Class
		{
			return HudFrameEmbed.FirstDayOfSpring;
		}
		
		// class
		public function FirstDayOfSpring()
		{
			super(new ScheduleData(Number.NaN, 3, 20), new ScheduleData(Number.NaN, 3, 20));
		}
	}
}