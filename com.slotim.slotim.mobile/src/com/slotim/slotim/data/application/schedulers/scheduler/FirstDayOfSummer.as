package com.slotim.slotim.data.application.schedulers.scheduler
{
	import com.slotim.slotim.assets.hud.frame.HudFrameEmbed;
	import com.slotim.slotim.data.application.schedulers.ScheduleData;
	import com.slotim.slotim.data.application.schedulers.scheduler.base.BaseScheduler;

	public class FirstDayOfSummer extends BaseScheduler
	{
		// properties
		public override function get FrameClass():Class
		{
			return HudFrameEmbed.FirstDayOfSummer;
		}
		
		// class
		public function FirstDayOfSummer()
		{
			super(new ScheduleData(Number.NaN, 6, 21), new ScheduleData(Number.NaN, 6, 21));
		}
	}
}