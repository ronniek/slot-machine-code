package com.slotim.slotim.data.application.schedulers.scheduler
{
	import com.slotim.slotim.data.application.schedulers.ScheduleData;
	import com.slotim.slotim.data.application.schedulers.scheduler.base.BaseScheduler;
	
	public class FirstDayOfTheMonthScheduler extends BaseScheduler
	{
		// class
		public function FirstDayOfTheMonthScheduler()
		{
			super(new ScheduleData(Number.NaN, Number.NaN, 1), new ScheduleData(Number.NaN, Number.NaN, 1));
		}
	}
}