package com.slotim.slotim.data.application.schedulers.scheduler
{
	import com.slotim.slotim.assets.hud.frame.HudFrameEmbed;
	import com.slotim.slotim.data.application.schedulers.ScheduleData;
	import com.slotim.slotim.data.application.schedulers.scheduler.base.BaseScheduler;

	public class FirstDayOfWinter extends BaseScheduler
	{
		// properties
		public override function get FrameClass():Class
		{
			return HudFrameEmbed.FirstDayOfWinter;
		}
		
		// class
		public function FirstDayOfWinter()
		{
			super(new ScheduleData(Number.NaN, 12, 21), new ScheduleData(Number.NaN, 12, 21));
		}
	}
}