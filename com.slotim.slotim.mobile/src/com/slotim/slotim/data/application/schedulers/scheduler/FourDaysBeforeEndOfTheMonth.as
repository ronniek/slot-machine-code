package com.slotim.slotim.data.application.schedulers.scheduler
{
	import com.slotim.slotim.data.application.schedulers.ScheduleData;
	import com.slotim.slotim.data.application.schedulers.scheduler.base.BaseScheduler;
	
	public class FourDaysBeforeEndOfTheMonth extends BaseScheduler
	{
		// class
		public function FourDaysBeforeEndOfTheMonth()
		{
			super(new ScheduleData(Number.NaN, Number.NaN, 27), new ScheduleData(Number.NaN, Number.NaN, 31));
		}
	}
}