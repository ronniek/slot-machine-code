package com.slotim.slotim.data.application.schedulers.scheduler
{
	import com.slotim.slotim.assets.hud.frame.HudFrameEmbed;
	import com.slotim.slotim.data.application.schedulers.ScheduleData;
	import com.slotim.slotim.data.application.schedulers.scheduler.base.BaseScheduler;

	public class HalloweenScheduler extends BaseScheduler
	{
		// properties
		public override function get FrameClass():Class
		{
			return HudFrameEmbed.Halloween;
		}
		
		// class
		public function HalloweenScheduler()
		{
			super(new ScheduleData(Number.NaN, 10, 29), new ScheduleData(Number.NaN, 10, 31));
		}
	}
}