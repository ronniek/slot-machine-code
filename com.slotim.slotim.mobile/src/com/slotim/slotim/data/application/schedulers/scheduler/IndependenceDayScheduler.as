package com.slotim.slotim.data.application.schedulers.scheduler
{
	import com.slotim.slotim.assets.hud.frame.HudFrameEmbed;
	import com.slotim.slotim.data.application.schedulers.ScheduleData;
	import com.slotim.slotim.data.application.schedulers.scheduler.base.BaseScheduler;
	
	public class IndependenceDayScheduler extends BaseScheduler
	{
		// properties
		public override function get FrameClass():Class
		{
			return HudFrameEmbed.IndependenceDay;
		}
		
		// class
		public function IndependenceDayScheduler()
		{
			super(new ScheduleData(Number.NaN, 7, 4), new ScheduleData(Number.NaN, 7, 4));
		}
	}
}