package com.slotim.slotim.data.application.schedulers.scheduler
{
	import com.slotim.slotim.assets.hud.frame.HudFrameEmbed;
	import com.slotim.slotim.data.application.schedulers.ScheduleData;
	import com.slotim.slotim.data.application.schedulers.scheduler.base.BaseScheduler;
	
	public class MothersDayScheduler extends BaseScheduler
	{
		// properties
		public override function get FrameClass():Class
		{
			return HudFrameEmbed.MothersDay;
		}
		
		// class
		public function MothersDayScheduler()
		{
			super(new ScheduleData(Number.NaN, 5, 11), new ScheduleData(Number.NaN, 5, 11));
		}
	}
}