package com.slotim.slotim.data.application.schedulers.scheduler
{
	import com.slotim.slotim.assets.hud.frame.HudFrameEmbed;
	import com.slotim.slotim.data.application.schedulers.ScheduleData;
	import com.slotim.slotim.data.application.schedulers.scheduler.base.BaseScheduler;
	
	public class NewYearScheduler extends BaseScheduler
	{
		// properties
		public override function get FrameClass():Class
		{
			return HudFrameEmbed.NewYearsDay;
		}

		// class
		public function NewYearScheduler()
		{
			super(new ScheduleData(Number.NaN, 1, 1), new ScheduleData(Number.NaN, 1, 1));
		}
	}
}