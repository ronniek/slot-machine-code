package com.slotim.slotim.data.application.schedulers.scheduler
{
	import com.slotim.slotim.assets.hud.frame.HudFrameEmbed;
	import com.slotim.slotim.data.application.schedulers.ScheduleData;
	import com.slotim.slotim.data.application.schedulers.scheduler.base.BaseScheduler;
	
	public class ThanksGivingDayScheduler extends BaseScheduler
	{
		// properties
		public override function get FrameClass():Class
		{
			return HudFrameEmbed.ThanksgivingDay;
		}
		
		// class
		public function ThanksGivingDayScheduler()
		{
			super(new ScheduleData(2014, 11, 26), new ScheduleData(2014, 11, 27));
		}
	}
}