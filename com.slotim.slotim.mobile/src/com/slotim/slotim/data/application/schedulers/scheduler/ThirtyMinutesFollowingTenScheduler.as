package com.slotim.slotim.data.application.schedulers.scheduler
{
	import com.slotim.slotim.data.application.schedulers.ScheduleData;
	import com.slotim.slotim.data.application.schedulers.scheduler.base.BaseScheduler;

	public class ThirtyMinutesFollowingTenScheduler extends BaseScheduler
	{
		// class
		public function ThirtyMinutesFollowingTenScheduler()
		{
			super(new ScheduleData(Number.NaN, Number.NaN, Number.NaN, Number.NaN, 22, 0), new ScheduleData(Number.NaN, Number.NaN, Number.NaN, Number.NaN, 22, 30));
		}
	}
}