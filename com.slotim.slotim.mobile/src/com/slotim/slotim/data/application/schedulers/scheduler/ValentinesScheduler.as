package com.slotim.slotim.data.application.schedulers.scheduler
{
	import com.slotim.slotim.assets.hud.frame.HudFrameEmbed;
	import com.slotim.slotim.data.application.schedulers.ScheduleData;
	import com.slotim.slotim.data.application.schedulers.scheduler.base.BaseScheduler;
	
	public class ValentinesScheduler extends BaseScheduler
	{
		// properties
		public override function get FrameClass():Class
		{
			return HudFrameEmbed.ValentinesDay;
		}

		// class
		public function ValentinesScheduler()
		{
			super(new ScheduleData(Number.NaN, 2, 10), new ScheduleData(Number.NaN, 2, 14));
		}
	}
}