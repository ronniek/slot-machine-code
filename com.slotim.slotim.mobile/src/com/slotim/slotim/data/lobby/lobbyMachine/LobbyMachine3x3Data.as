package com.slotim.slotim.data.lobby.lobbyMachine
{
	import com.slotim.slotim.assets.notifications.animation.columnFrames.ColumnFramesEmebed;
	import com.slotim.slotim.assets.notifications.animation.payboxesFrames.PayboxesFramesEmbed;
	import com.slotim.slotim.assets.notifications.animation.symetricFrames.SymetricFramesEmbed;
	import com.slotim.slotim.data.lobby.lobbyMachine.base.BaseLobbyMachineData;
	import com.slotim.slotim.data.session.level.LevelData;
	import com.slotim.slotim.assets.paylines.paylines3x3.LinesEmbed3x3;
	import com.slotim.slotim.data.machine.paylines.paylines.Paylines3x3Data;
	
	public class LobbyMachine3x3Data extends BaseLobbyMachineData
	{
		// properties
		protected final override function get ColumnFramesClass():Vector.<Class>
		{
			var columnFramesClass:Vector.<Class> = new Vector.<Class>();
			columnFramesClass.push(ColumnFramesEmebed.ColumnFrame3x3a);
			columnFramesClass.push(ColumnFramesEmebed.ColumnFrame3x3b);
			return columnFramesClass;
		}
		protected final override function get SymetricFramesClass():Vector.<Class>
		{
			var symetricFramesClass:Vector.<Class> = new Vector.<Class>();
			symetricFramesClass.push(SymetricFramesEmbed.SymetricFrame3x3a);
			symetricFramesClass.push(SymetricFramesEmbed.SymetricFrame3x3b);
			return symetricFramesClass;
		}
		protected final override function get PayboxFramesClass():Vector.<Class>
		{
			var payboxFramesClass:Vector.<Class> = new Vector.<Class>();
			payboxFramesClass.push(PayboxesFramesEmbed.PayboxFrame3x3a);
			payboxFramesClass.push(PayboxesFramesEmbed.PayboxFrame3x3b);
			return payboxFramesClass;
		}
		protected final override function get PaylinesClass():Class
		{
			return Paylines3x3Data;
		}
		protected override function get MatrixPreviewClass():Class
		{
			return LinesEmbed3x3.MatrixPreview;
		}
		
		// class
		public function LobbyMachine3x3Data(id:int, machineName:String, normalSymbolsFunction:Function, bonusGameDataClass:Class, serverSymbolFileName:String, setPreviewSymbolID:int, factor:Number, maxPaylines:int, opensOnLevel:LevelData,
											isComingSoon:Boolean, depreciationRatio:Number, strikeValuator:Boolean, freeSpinsScatterValuator:Boolean, bombScatterValuator:Boolean, miniSpinScatterValuator:Boolean,
											collectiblesScatterValuator:Boolean, bonusGameValuator:Boolean, columnValuator:Boolean, symetricValuator:Boolean, multiplierScatterValuator:Boolean)
		{
			super(id, machineName, normalSymbolsFunction, bonusGameDataClass, serverSymbolFileName, setPreviewSymbolID, factor, maxPaylines, opensOnLevel, isComingSoon, depreciationRatio, strikeValuator, freeSpinsScatterValuator,
				bombScatterValuator, miniSpinScatterValuator, collectiblesScatterValuator, bonusGameValuator, columnValuator, symetricValuator, multiplierScatterValuator);
		}
	}
}