package com.slotim.slotim.data.lobby.lobbyMachine.dummy
{
	import com.slotim.slotim.data.lobby.lobbyMachine.base.BaseLobbyMachineData;
	import com.slotim.slotim.data.session.level.LevelData;
	
	public final class ComingSoonDummyData extends BaseLobbyMachineData
	{
		// class
		public function ComingSoonDummyData(id:int, normalSymbolsFunction:Function)
		{
			super(id, "dummy", normalSymbolsFunction, null, "", 0, 1, 25, new LevelData(1), true, 1,	false,	false,	false,	false,	false,	false,	false, false, false);
		}
	}
}