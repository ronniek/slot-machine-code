package com.slotim.slotim.data.machine.symbol
{
	import com.slotim.slotim.assets.machine.scatterPayboxes.ScatterPayboxesEmbed;
	import com.slotim.slotim.assets.machine.symbols.SymbolsEmbed;
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.data.machine.symbol.base.BaseScatterSymbolData;
	import com.slotim.slotim.data.machine.symbol.base.SymbolTypeEnum;
	
	public class BonusGameSymbolData extends BaseScatterSymbolData
	{
		// properties
		public override function get SpecialOdds():Number
		{
			return Main.Instance.Device.IsMobile ? 0.02 : 0.1 * _factor;
		}
		
		// class
		public function BonusGameSymbolData(id:int, pngClass:Class, factor:Number)
		{
			super(id, SymbolsEmbed.BonusGame, SymbolTypeEnum.BonusGame, factor, 20 * factor, 15 * factor, 10 * factor, 5 * factor, 0, "Bonus Game", "Win a bonus game", ScatterPayboxesEmbed.BonusGame);
		}
	}
}