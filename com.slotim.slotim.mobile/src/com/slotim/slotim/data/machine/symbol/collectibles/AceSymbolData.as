package com.slotim.slotim.data.machine.symbol.collectibles
{
	import com.slotim.slotim.assets.machine.scatterPayboxes.ScatterPayboxesEmbed;
	import com.slotim.slotim.assets.machine.symbols.SymbolsEmbed;
	import com.slotim.slotim.data.machine.symbol.base.BaseCollectiblesSymbolData;
	import com.slotim.slotim.data.machine.symbol.base.SymbolTypeEnum;
	
	public class AceSymbolData extends BaseCollectiblesSymbolData
	{
		// class
		public function AceSymbolData(id:int, pngClass:Class, factor:Number)
		{
			super(id, SymbolsEmbed.Ace, SymbolTypeEnum.Ace, factor, 1, 1, 1, 1, 1, "Ace", "Collect this symbol to win some free Chips", ScatterPayboxesEmbed.Ace);
		}
	}
}