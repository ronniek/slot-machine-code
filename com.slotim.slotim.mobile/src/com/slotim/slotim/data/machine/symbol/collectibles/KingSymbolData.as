package com.slotim.slotim.data.machine.symbol.collectibles
{
	import com.slotim.slotim.assets.machine.scatterPayboxes.ScatterPayboxesEmbed;
	import com.slotim.slotim.assets.machine.symbols.SymbolsEmbed;
	import com.slotim.slotim.data.machine.symbol.base.BaseCollectiblesSymbolData;
	import com.slotim.slotim.data.machine.symbol.base.SymbolTypeEnum;
	
	public class KingSymbolData extends BaseCollectiblesSymbolData
	{
		// class
		public function KingSymbolData(id:int, pngClass:Class, factor:Number)
		{
			super(id, SymbolsEmbed.King, SymbolTypeEnum.King, factor, 1, 1, 1, 1, 1, "King", "Collect this symbol to win some free Chips", ScatterPayboxesEmbed.King);
		}
	}
}