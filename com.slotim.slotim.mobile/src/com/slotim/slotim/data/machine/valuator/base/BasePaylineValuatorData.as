package com.slotim.slotim.data.machine.valuator.base
{
	import com.slotim.slotim.data.machine.paylines.payline.base.BasePaylineData;
	
	public class BasePaylineValuatorData extends BaseValuatorData
	{
		// members
		protected var _payline:BasePaylineData;										
		
		// properteis
		public function get Payline():BasePaylineData
		{
			return _payline;
		}
		
		// class
		public function BasePaylineValuatorData()
		{
			super();
		}
	}
}