package com.slotim.slotim.data.machine.valuator.bonusGame
{
	import com.slotim.slotim.data.machine.paybox.PayboxData;
	import com.slotim.slotim.data.machine.paylines.payline.base.BasePaylineData;
	import com.slotim.slotim.data.machine.valuator.base.BasePaylineValuatorData;
	
	public class BonusGameValuatorData extends BasePaylineValuatorData
	{
		// class
		public function BonusGameValuatorData(payboxes:Vector.<PayboxData>, payline:BasePaylineData)
		{
			super();
			
			_payboxes = payboxes;
			_payline = payline;
		}
	}
}