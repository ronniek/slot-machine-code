package com.slotim.slotim.data.machine.valuator.collectibles
{
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.data.machine.valuator.base.BaseScatterValuatorData;

	public class AceValuatorData extends BaseScatterValuatorData
	{
		// class
		public function AceValuatorData()
		{
			super(Main.Instance.ActiveMachine.LobbyMachine.Symbols.AceSymbol);
		}
	}
}