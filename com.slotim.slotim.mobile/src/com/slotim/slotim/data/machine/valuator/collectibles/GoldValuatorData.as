package com.slotim.slotim.data.machine.valuator.collectibles
{
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.data.machine.valuator.base.BaseScatterValuatorData;
	
	public class GoldValuatorData extends BaseScatterValuatorData
	{
		// class
		public function GoldValuatorData()
		{
			super(Main.Instance.ActiveMachine.LobbyMachine.Symbols.GoldSymbol);
		}
	}
}