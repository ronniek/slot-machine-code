package com.slotim.slotim.data.machine.valuator.collectibles
{
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.data.machine.valuator.base.BaseScatterValuatorData;
	
	public class KingValuatorData extends BaseScatterValuatorData
	{
		// class
		public function KingValuatorData()
		{
			super(Main.Instance.ActiveMachine.LobbyMachine.Symbols.KingSymbol);
		}
	}
}