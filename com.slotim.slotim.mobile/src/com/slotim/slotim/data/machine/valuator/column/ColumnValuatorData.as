package com.slotim.slotim.data.machine.valuator.column
{
	import com.slotim.slotim.data.machine.paybox.PayboxData;
	import com.slotim.slotim.data.machine.valuator.base.BaseValuatorData;
	
	public class ColumnValuatorData extends BaseValuatorData
	{
		// class
		public function ColumnValuatorData(payboxes:Vector.<PayboxData>)
		{
			super();
			
			_payboxes = payboxes;
		}
	}
}