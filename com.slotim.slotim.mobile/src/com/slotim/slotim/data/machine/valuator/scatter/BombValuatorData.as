package com.slotim.slotim.data.machine.valuator.scatter
{
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.data.machine.valuator.base.BaseScatterValuatorData;
	
	public class BombValuatorData extends BaseScatterValuatorData
	{
		// class
		public function BombValuatorData()
		{
			super(Main.Instance.ActiveMachine.LobbyMachine.Symbols.BombSymbol);
		}
	}
}