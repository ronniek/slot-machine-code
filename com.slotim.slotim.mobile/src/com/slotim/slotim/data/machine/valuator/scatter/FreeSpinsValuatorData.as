package com.slotim.slotim.data.machine.valuator.scatter
{
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.data.machine.valuator.base.BaseScatterValuatorData;
	
	public class FreeSpinsValuatorData extends BaseScatterValuatorData
	{
		// class
		public function FreeSpinsValuatorData()
		{
			super(Main.Instance.ActiveMachine.LobbyMachine.Symbols.FreeSpinsSymbol);
		}
	}
}