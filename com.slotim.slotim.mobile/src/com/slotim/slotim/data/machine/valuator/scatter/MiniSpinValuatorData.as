package com.slotim.slotim.data.machine.valuator.scatter
{
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.data.machine.valuator.base.BaseScatterValuatorData;
	
	public class MiniSpinValuatorData extends BaseScatterValuatorData
	{
		// class
		public function MiniSpinValuatorData()
		{
			super(Main.Instance.ActiveMachine.LobbyMachine.Symbols.MiniSpinSymbol);
		}
	}
}