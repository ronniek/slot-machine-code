package com.slotim.slotim.data.machine.valuator.scatter
{
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.data.machine.valuator.base.BaseScatterValuatorData;
	
	public class MultiplierValuatorData extends BaseScatterValuatorData
	{
		// class
		public function MultiplierValuatorData()
		{
			super(Main.Instance.ActiveMachine.LobbyMachine.Symbols.MultiplierSymbol);
		}
		
		// methods
		public override function CalculatePaylinesPayout(selectedPaylines:int, selectedBetChips:Number):void
		{
			Chips = Payout * selectedBetChips;
		}
	}
}