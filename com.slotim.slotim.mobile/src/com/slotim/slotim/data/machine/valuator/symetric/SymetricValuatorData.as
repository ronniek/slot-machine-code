package com.slotim.slotim.data.machine.valuator.symetric
{
	import com.slotim.slotim.data.machine.paylines.payline.base.BasePaylineData;
	import com.slotim.slotim.data.machine.valuator.base.BasePaylineValuatorData;
	
	public class SymetricValuatorData extends BasePaylineValuatorData
	{
		// class
		public function SymetricValuatorData(payline:BasePaylineData)
		{
			super();
			
			_payboxes = payline.Payboxes;
			_payline = payline;
		}
	}
}