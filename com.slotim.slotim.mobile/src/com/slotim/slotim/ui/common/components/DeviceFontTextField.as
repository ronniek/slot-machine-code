package com.slotim.slotim.ui.common.components
{
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.ui.common.components.base.BaseTextField;
	
	import flash.text.TextField;
	
	public class DeviceFontTextField extends BaseTextField
	{
		// properties
		protected override function get XFLTextField():TextField
		{
			return Main.Instance.TextFields.DeviceFont;
		}
		
		// class
		public function DeviceFontTextField(w:int, h:int, text:String)
		{
			super(w, h, text);
		}
	}
}