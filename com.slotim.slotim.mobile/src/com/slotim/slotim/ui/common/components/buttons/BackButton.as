package com.slotim.slotim.ui.common.components.buttons
{
	import com.slotim.slotim.assets.machinesList.MachinesListEmbed;
	import com.slotim.slotim.ui.common.components.base.BaseClickableButton;
	
	public class BackButton extends BaseClickableButton
	{
		// class
		public function BackButton(onClickDispatch:Function)
		{
			super(44, 44, onClickDispatch, null, new MachinesListEmbed.BackNormal(), new MachinesListEmbed.BackSelected(), Number.NaN);
		}
	}
}