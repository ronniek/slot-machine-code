package com.slotim.slotim.ui.common.components.freeActionScript.scrollable.base
{
	import com.slotim.slotim.ui.common.components.base.BaseBG;
	
	import flash.filters.BevelFilter;
	
	public class ScrollbarPad extends BaseBG
	{
		// properties
		protected override function get BGFilters():Array
		{
			return [new BevelFilter(4, 45, 0xffffff, 1, 0)];
		}
		protected override function get FrameCorner():Number
		{
			return W;
		}
		
		// class
		public function ScrollbarPad(w:int, h:int, bgColor:Number = Number.NaN)
		{
			super(w, h, bgColor);
		}
	}
}