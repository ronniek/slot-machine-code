package com.slotim.slotim.ui.common.components.freeActionScript.scrollable.base
{
	import com.slotim.slotim.ui.common.components.base.BaseBG;
	
	import flash.filters.BevelFilter;
	
	public class ScrollbarThread extends BaseBG
	{
		// properties
		protected override function get BGFilters():Array
		{
			return [new BevelFilter(4, 45, 0xffffff, 1, 0)];
		}
		protected override function get FrameCorner():Number
		{
			return W;
		}
		
		// class
		public function ScrollbarThread(w:int, h:int, bgColor:Number=Number.NaN)
		{
			super(w, h, bgColor);
		}
	}
}