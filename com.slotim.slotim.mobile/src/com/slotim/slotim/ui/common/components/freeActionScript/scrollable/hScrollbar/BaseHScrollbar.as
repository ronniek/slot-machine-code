package com.slotim.slotim.ui.common.components.freeActionScript.scrollable.hScrollbar
{
	import com.greensock.TweenLite;
	import com.greensock.easing.Cubic;
	import com.slotim.slotim.ui.common.components.base.BaseBG;
	import com.slotim.slotim.ui.common.components.base.BaseComponent;
	import com.slotim.slotim.ui.common.components.freeActionScript.scrollable.base.ScrollbarPad;
	import com.slotim.slotim.ui.common.components.freeActionScript.scrollable.base.ScrollbarThread;
	import com.slotim.slotim.utils.error.MustOverrideError;
	
	import flash.display.Shape;
	import flash.events.MouseEvent;
	
	public class BaseHScrollbar extends BaseBG
	{
		// members
		private var _mask:Shape;
		private var _content:BaseComponent;
		
		private var _mouseArmed:Boolean;
		private var _contentXArmed:int;
		private var _localXArmed:int;
		private var _recentMouseMoveX:int;
		
		private var _pad:ScrollbarPad;
		
		// properties
		protected override function get FrameCorner():Number
		{
			return 20;
		}
		protected function get Content():BaseComponent
		{
			throw new MustOverrideError();
		}
		
		// class
		public function BaseHScrollbar(w:int, h:int, bgColor:Number = 0x8330ba)
		{
			super(w, h, bgColor);
			
			_mask = new Shape();
			_mask.x = 6;
			_mask.y = 6;
			_mask.graphics.beginFill(0xff0000, 1);
			_mask.graphics.drawRect(0, 0, W - 12, H - 12);
			_mask.graphics.endFill();
			addChild(_mask);
			
			_content = Content;
			_content.x = 6;
			_content.y = 6;
			_content.mouseChildren = false;
			_content.mouseEnabled = false;
			_content.mask = _mask;
			addChild(_content);
			
			addEventListener(MouseEvent.MOUSE_DOWN, OnMouseDown);
			addEventListener(MouseEvent.MOUSE_UP, OnMouseUp);
			addEventListener(MouseEvent.MOUSE_MOVE, OnMouseMove);
			addEventListener(MouseEvent.ROLL_OUT, OnRollOut);
			
			var scrollbarThread:ScrollbarThread = new ScrollbarThread(_mask.width - 6, 4, 0xce117b);
			scrollbarThread.x = 9;
			scrollbarThread.y = H - 6 - 20 / 2 - 2;
			addChild(scrollbarThread);
			
			_pad = new ScrollbarPad(20, 20, 0xce117b);
			_pad.x = 6;
			_pad.y = H - 6 - 20;
			addChild(_pad);
		}
		public override function Dispose():void
		{
			removeEventListener(MouseEvent.MOUSE_DOWN, OnMouseDown);
			removeEventListener(MouseEvent.MOUSE_UP, OnMouseUp);
			removeEventListener(MouseEvent.MOUSE_MOVE, OnMouseMove);
			removeEventListener(MouseEvent.ROLL_OUT, OnRollOut);
		}
		
		// events
		protected function OnMouseDown(event:MouseEvent):void
		{
			_mouseArmed = true;
			_localXArmed = event.localX;
			_contentXArmed = _content.x;
		}
		protected function OnMouseUp(event:MouseEvent):void
		{
			_mouseArmed = false;
		}
		protected function OnMouseMove(event:MouseEvent):void
		{
			if (_mouseArmed)
			{
				var newContentX:int = _contentXArmed + event.localX - _localXArmed;
				if (newContentX < _mask.x + _mask.width - _content.width)
				{
					newContentX = _mask.x + _mask.width - _content.width - 20;
				}
				if (newContentX > _mask.x)
				{
					newContentX = _mask.x + 20;
				}
				
				TweenLite.to(_content, 0.5, {x:newContentX, ease:Cubic.easeOut, onUpdate:OnUpdate, onComplete:OnComplete});
				_recentMouseMoveX = event.localX;
			}
		}
		private function OnUpdate():void
		{
			var padSpan:int = W - 6 - _pad.width;
			
			var padX:int = 6 + (_mask.x - _content.x) / (_content.width - _mask.width) * padSpan;
			padX = padX < 6 ? 6 : padX;
			padX = padX > padSpan ? padSpan : padX;
			_pad.x = padX;
		}
		private function OnComplete():void
		{
			if (_content.x < _mask.x + _mask.width - _content.width)
			{
				TweenLite.to(_content, 0.2, {x:_mask.x + _mask.width - _content.width, ease:Cubic.easeOut});
			}
			if (_content.x > _mask.x)
			{
				TweenLite.to(_content, 0.2, {x:_mask.x, ease:Cubic.easeOut, onUpdate:OnUpdate});
			}
		}
		protected function OnRollOut(event:MouseEvent):void
		{
			_mouseArmed = false;
		}
	}
}