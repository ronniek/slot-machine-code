package com.slotim.slotim.ui.hud
{
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.ui.common.components.SpriteEx;
	import com.slotim.slotim.ui.hud.appsList.AppsListRibbon;
	import com.slotim.slotim.ui.hud.jackpot.JackpotRibbon;
	import com.slotim.slotim.ui.hud.tickerPanel.TickerPanel;
	import com.slotim.slotim.ui.hud.topPanel.TopPanel;
	
	public class HudPanel extends SpriteEx
	{
		// class
		public function HudPanel()
		{
			super();
			
			// jackpot
			var jackpot:JackpotRibbon = new JackpotRibbon();
			jackpot.x = (Main.Instance.Device.DesignRectangle.width - jackpot.width) / 2;
			jackpot.y = 28;
			addChild(jackpot);
			
			// top panel
			addChild(new TopPanel());
			
			// ticker panel
			var tickerPanel:TickerPanel = new TickerPanel();
			tickerPanel.x = (Main.Instance.Device.DesignRectangle.width - tickerPanel.width) / 2;
			tickerPanel.y = 330;
			addChild(tickerPanel);
			
			// apps List
			if (Main.Instance.Application.EnableAppsList && Main.Instance.Application.AppsList.Count > 0)
			{
				var appsListRibbon:AppsListRibbon = new AppsListRibbon();
				appsListRibbon.x = 775;
				appsListRibbon.y = 362;
				addChild(appsListRibbon);
			}

			// frame
			//addChild(new HudFrame());
		}
	}
}