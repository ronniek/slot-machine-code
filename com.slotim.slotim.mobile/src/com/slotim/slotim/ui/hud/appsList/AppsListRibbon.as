package com.slotim.slotim.ui.hud.appsList
{
	import com.greensock.TweenLite;
	import com.greensock.easing.Elastic;
	import com.greensock.easing.Quart;
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.data.application.appsList.AppsListEvent;
	import com.slotim.slotim.ui.common.components.base.BaseTextField;
	import com.slotim.slotim.ui.machine.freeSpins.BubblingFreeSpinsTextField;
	import com.slotim.slotim.ui.notifications.NotificationsHandler;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.BevelFilter;
	import flash.text.TextField;
	
	public class AppsListRibbon extends BaseTextField
	{
		// properties
		protected override function get BGFilters():Array
		{
			return [new BevelFilter(3, 45, 0xffffff, 1, 0)];
		}
		protected override function get FrameCorner():Number
		{
			return 30;
		}
		
		protected override function get XFLTextField():TextField
		{
			return Main.Instance.TextFields.MachineFreeSpinsRibbon;
		}
		protected override function get MaxTextFieldSize():int
		{
			return 60;
		}
		
		// class
		public function AppsListRibbon()
		{
			super(170, 30, " ", null, 0x8330ba);
			
			addEventListener(MouseEvent.CLICK, OnClick);
			Main.Instance.Application.AppsList.addEventListener(AppsListEvent.Show, OnShow);
			Main.Instance.Application.AppsList.addEventListener(AppsListEvent.Hide, OnHide);
			
			Update(Main.Instance.Application.AppsList.Count);
		}
		public override function Dispose():void
		{
			TweenLite.killTweensOf(this);
			Main.Instance.Application.AppsList.removeEventListener(AppsListEvent.Show, OnShow);
			Main.Instance.Application.AppsList.removeEventListener(AppsListEvent.Hide, OnHide);
			
			super.Dispose();
		}
		
		// methods
		private function Update(diffCount:int):void
		{
			if (Main.Instance.Application.AppsList.Count == 1)
			{
				Text = "New Apps";
			}
			else if (Main.Instance.Application.AppsList.Count > 1)
			{
				Text = "New Apps: " + Main.Instance.Application.AppsList.Count.toString();
			}
			else
			{
				x = 810;
				Text = " ";
			}
			
			if (diffCount != 0)
			{
				var bubblingFreeSpins:BubblingFreeSpinsTextField = new BubblingFreeSpinsTextField();
				bubblingFreeSpins.x = 220;
				bubblingFreeSpins.y = 362;
				bubblingFreeSpins.Text = (diffCount >= 0 ? "+" : "") + diffCount;
				if (parent)
				{
					parent.addChild(bubblingFreeSpins);
				}
				bubblingFreeSpins.Bubble(Number.NaN, 70);
			}
		}
		public function DoShow():void
		{
			TweenLite.to(this, 1, {x:650, delay:0.3, ease:Quart.easeOut});
		}
		public function DoHide():void
		{
			TweenLite.to(this, 1, {x:775, delay:0.3, ease:Elastic.easeOut});
		}
		
		// events
		private function OnClick(event:Event):void
		{
			if (Main.Instance.Application.AppsList.Count > 0 && x == 650)
			{
				Main.Instance.Application.AppsList.Hide();
				DoHide();
				NotificationsHandler.Instance.ShowAppsListPopup();
			}
			else if (Main.Instance.Application.AppsList.Count > 0 && x == 775)
			{
				DoShow();
			}
		}
		
		private function OnShow(event:Event):void
		{
			DoShow();
		}
		private function OnHide(event:Event):void
		{
			DoHide();
		}
	}
}