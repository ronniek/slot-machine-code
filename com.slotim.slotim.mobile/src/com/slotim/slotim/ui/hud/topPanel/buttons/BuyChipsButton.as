package com.slotim.slotim.ui.hud.topPanel.buttons
{
	import com.slotim.slotim.assets.hud.topPanel.TopPanelEmbed;
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.ui.common.components.base.BaseClickableButton;
	import com.slotim.slotim.ui.notifications.NotificationsHandler;
	import com.slotim.slotim.utils.sounds.SoundsManager;
	
	import flash.events.MouseEvent;
	
	public class BuyChipsButton extends BaseClickableButton
	{
		// class
		public function BuyChipsButton()
		{
			super(36, 36, null, null, new TopPanelEmbed.BuyChipsNormal(), new TopPanelEmbed.BuyChipsSelected());
		}
		
		// events
		protected override function OnClick(event:MouseEvent):void
		{
			Main.Instance.Sounds.Play(SoundsManager.TopPanel_Click);
			super.OnClick(event);
			NotificationsHandler.Instance.ShowBuyChipsPopup(false, null);
		}
	}
}