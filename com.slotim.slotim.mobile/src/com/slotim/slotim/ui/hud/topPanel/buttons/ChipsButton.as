package com.slotim.slotim.ui.hud.topPanel.buttons
{
	import com.slotim.slotim.assets.hud.topPanel.TopPanelEmbed;
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.ui.common.components.base.BaseClickableButton;
	import com.slotim.slotim.ui.notifications.NotificationsHandler;
	import com.slotim.slotim.utils.sounds.SoundsManager;
	
	import flash.events.MouseEvent;
	
	public class ChipsButton extends BaseClickableButton
	{
		// properties
		protected override function get PngFilters():Array
		{
			return null;
		}
		
		// class
		public function ChipsButton()
		{
			super(80, 66, null, null, new TopPanelEmbed.Chips());
		}
		
		// events
		protected override function OnClick(event:MouseEvent):void
		{
			Main.Instance.Sounds.Play(SoundsManager.TopPanel_Click);
			super.OnClick(event);
			NotificationsHandler.Instance.ShowBuyChipsPopup(false, null);
		}
	}
}