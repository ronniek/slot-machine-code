package com.slotim.slotim.ui.hud.topPanel.buttons
{
	import com.greensock.TweenLite;
	import com.slotim.slotim.assets.hud.topPanel.TopPanelEmbed;
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.ui.common.components.base.BaseClickableButton;
	import com.slotim.slotim.ui.notifications.NotificationsHandler;
	import com.slotim.slotim.utils.sounds.SoundsManager;
	import com.slotim.slotim.utils.xServices.social.SocialEvent;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	public class FacebookButton extends BaseClickableButton
	{
		// class
		public function FacebookButton()
		{
			super(44, 44, null, null, new TopPanelEmbed.FacebookNormal(), new TopPanelEmbed.FacebookSelected());
			
			ShowHide();
			
			Main.Instance.XServices.Social.addEventListener(SocialEvent.FacebookLogonChanged, OnFacebookLogonChanged);
		}
		public override function Dispose():void
		{
			Main.Instance.XServices.Social.removeEventListener(SocialEvent.FacebookLogonChanged, OnFacebookLogonChanged);
			
			super.Dispose();
		}
		
		// methods
		private function ShowHide():void
		{
			TweenLite.to(this, 0.5, {alpha:Main.Instance.XServices.Social.IsFacebookLogon ? 0 : 1});
		}
		
		// events
		protected function OnFacebookLogonChanged(event:Event):void
		{
			ShowHide();
		}
		
		protected override function OnClick(event:MouseEvent):void
		{
			Main.Instance.Sounds.Play(SoundsManager.TopPanel_Click);
			super.OnClick(event);
			NotificationsHandler.Instance.ShowConnectingPopup(null);
		}
	}
}