package com.slotim.slotim.ui.hud.topPanel.buttons
{
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.ui.common.components.base.BaseTextField;
	import com.slotim.slotim.ui.notifications.NotificationsHandler;
	import com.slotim.slotim.utils.sounds.SoundsManager;
	
	import flash.events.MouseEvent;
	import flash.filters.GlowFilter;
	import flash.text.TextField;
	
	public class FreeChipsRibbon extends BaseTextField
	{
		// properties
		protected override function get HasFrame():Boolean
		{
			return false;
		}
		protected override function get FrameFilters():Array
		{
			return null;
		}
		protected override function get FrameCorner():Number
		{
			return 30;
		}
		
		protected override function get XFLTextField():TextField
		{
			return Main.Instance.TextFields.MachineBottomPanelSpin;
		}
		protected override function get TextFieldOffsetY():int
		{
			return 11;
		}
		private function get GetText():String
		{
			return "Free Chips";
		}
		
		// class
		public function FreeChipsRibbon()
		{
			super(140, 55, GetText, null, 0x05e747);
			
			filters = [new GlowFilter(0xEC9EFE, 1, 5, 5)];
			
			addEventListener(MouseEvent.CLICK, OnClick);
		}
		public override function Dispose():void
		{
			removeEventListener(MouseEvent.CLICK, OnClick);
			super.Dispose();
		}
		
		// events
		protected function OnClick(event:MouseEvent):void
		{
			Main.Instance.Sounds.Play(SoundsManager.TopPanel_Click);
			NotificationsHandler.Instance.ShowBuyChipsPopup(false, null);
		}
	}
}