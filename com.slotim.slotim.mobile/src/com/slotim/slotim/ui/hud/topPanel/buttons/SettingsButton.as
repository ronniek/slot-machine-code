package com.slotim.slotim.ui.hud.topPanel.buttons
{
	import com.slotim.slotim.assets.hud.topPanel.TopPanelEmbed;
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.ui.common.components.base.BaseClickableButton;
	import com.slotim.slotim.ui.notifications.NotificationsHandler;
	import com.slotim.slotim.utils.sounds.SoundsManager;
	
	import flash.events.MouseEvent;
	
	public class SettingsButton extends BaseClickableButton
	{
		// class
		public function SettingsButton()
		{
			super(44, 44, null, null, new TopPanelEmbed.SettingsNormal(), new TopPanelEmbed.SettingsSelected());
		}
		
		// events
		protected override function OnClick(event:MouseEvent):void
		{
			Main.Instance.Sounds.Play(SoundsManager.TopPanel_Click);
			super.OnClick(event);
			NotificationsHandler.Instance.ShowSettingsPopup(null);
		}
	}
}