package com.slotim.slotim.ui.hud.topPanel.buttons
{
	import com.slotim.slotim.assets.hud.topPanel.TopPanelEmbed;
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.data.session.SessionDataEvent;
	import com.slotim.slotim.ui.notifications.popup.base.BaseVolumeButton;
	
	public class VolumeTopBarButton extends BaseVolumeButton
	{
		// class
		public function VolumeTopBarButton()
		{
			super(44, 44, new TopPanelEmbed.SoundOnOffNormal(), new TopPanelEmbed.SoundOnOffSelected());
			
			SetState();
			
			Main.Instance.Session.Wallet.addEventListener(SessionDataEvent.VolumeChanged, OnVolumeChanged);
		}
		public override function Dispose():void
		{
			Main.Instance.Session.Wallet.removeEventListener(SessionDataEvent.VolumeChanged, OnVolumeChanged);

			super.Dispose();
		}
	}
}