package com.slotim.slotim.ui.lobby.machineList.machinePreview.components
{
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.data.lobby.lobbyMachine.base.BaseLobbyMachineData;
	import com.slotim.slotim.data.session.machines.MachineSessionDataEvent;
	import com.slotim.slotim.ui.common.components.base.BaseTextField;
	import com.slotim.slotim.ui.common.components.gradientBG.RadialGradientBG;
	import com.slotim.slotim.utils.helpers.FormatterHelper;
	
	import flash.events.Event;
	import flash.text.TextField;
	
	public class FreeSpinsPendingRibbon extends BaseTextField
	{
		// members
		protected var _lobbyMachine:BaseLobbyMachineData;
		
		// properties
		protected override function get XFLTextField():TextField
		{
			return Main.Instance.TextFields.LobbyMachinePreviewFreeSpins;
		}
		
		// class
		public function FreeSpinsPendingRibbon(lobbyMachine:BaseLobbyMachineData)
		{
			_lobbyMachine = lobbyMachine;
			
			addChild(new RadialGradientBG(160, 50, 15, 0x53f0fb, 0x00adf9));
			
			super(160, 50, "\n");
			
			_lobbyMachine.MachineSession.addEventListener(MachineSessionDataEvent.FreeSpinsSpinStarted, OnFreeSpinsSpinStarted);
			_lobbyMachine.MachineSession.addEventListener(MachineSessionDataEvent.FreeSpinsCollectCount, OnFreeSpinsCollectCount);
			_lobbyMachine.MachineSession.addEventListener(MachineSessionDataEvent.FreeSpinsEndPopupClosed, OnFreeSpinsEndPopupClosed);
			
			Update();
		}
		public override function Dispose():void
		{
			_lobbyMachine.MachineSession.removeEventListener(MachineSessionDataEvent.FreeSpinsSpinStarted, OnFreeSpinsSpinStarted);
			_lobbyMachine.MachineSession.removeEventListener(MachineSessionDataEvent.FreeSpinsCollectCount, OnFreeSpinsCollectCount);
			_lobbyMachine.MachineSession.removeEventListener(MachineSessionDataEvent.FreeSpinsEndPopupClosed, OnFreeSpinsEndPopupClosed);
			
			super.Dispose();
		}
		
		// methods
		private function Update():void
		{
			if (_lobbyMachine.MachineSession.FreeSpinsPendingCount > 0)
			{
				Text = FormatterHelper.NumberToMoney( _lobbyMachine.MachineSession.FreeSpinsPendingCount, 0, 0) + "\nFree Spins";
			}
			else
			{
				Text = "\n";
			}
			
			visible = _lobbyMachine.IsOpen && _lobbyMachine.MachineSession.FreeSpinsPendingCount > 0;
		}
		
		// events
		protected function OnFreeSpinsSpinStarted(event:Event):void
		{
			Update();
		}
		protected function OnFreeSpinsCollectCount(event:Event):void
		{
			Update();
		}
		protected function OnFreeSpinsEndPopupClosed(event:Event):void
		{
			Update();
		}
	}
}