package com.slotim.slotim.ui.lobby.machineList.machinePreview.components
{
	import com.slotim.slotim.assets.machinesList.MachinesListEmbed;
	import com.slotim.slotim.data.lobby.lobbyMachine.base.BaseLobbyMachineData;
	import com.slotim.slotim.data.session.machines.MachineSessionDataEvent;
	import com.slotim.slotim.ui.common.components.base.BasePng;
	
	import flash.events.Event;
	
	public class NewPreviewPng extends BasePng
	{
		// members
		protected var _lobbyMachine:BaseLobbyMachineData;
		
		// class
		public function NewPreviewPng(lobbyMachine:BaseLobbyMachineData)
		{
			super(85, 75, new MachinesListEmbed.NewPreview());
			
			_lobbyMachine = lobbyMachine;
			
			visible = !_lobbyMachine.IsCommingSoon && _lobbyMachine.MachineSession.NewMachine && _lobbyMachine.IsOpen;
			
			_lobbyMachine.MachineSession.addEventListener(MachineSessionDataEvent.NewMachineChanged, OnNewMachineChanged);
		}
		public override function Dispose():void
		{
			_lobbyMachine.MachineSession.removeEventListener(MachineSessionDataEvent.NewMachineChanged, OnNewMachineChanged);
			
			super.Dispose();
		}
		
		// events
		protected function OnNewMachineChanged(event:Event):void
		{
			visible = !_lobbyMachine.IsCommingSoon && _lobbyMachine.MachineSession.NewMachine && _lobbyMachine.IsOpen;
		}
	}
}