package com.slotim.slotim.ui.lobby.machineList.machinePreview.components
{
	import com.slotim.slotim.assets.machinesList.MachinesListEmbed;
	import com.slotim.slotim.data.lobby.lobbyMachine.base.BaseLobbyMachineData;
	import com.slotim.slotim.data.machine.MachineDataEvent;
	import com.slotim.slotim.ui.common.components.base.BasePng;
	
	import flash.events.Event;
	
	public class PreviewInvitePng extends BasePng
	{
		// members
		private var _lobbyMachine:BaseLobbyMachineData;
		
		// class
		public function PreviewInvitePng(lobbyMachine:BaseLobbyMachineData)
		{
			super(60, 60, new MachinesListEmbed.FacebookInvite());
			
			_lobbyMachine = lobbyMachine;
			
			visible = !_lobbyMachine.IsOpen && !_lobbyMachine.IsCommingSoon;
			_lobbyMachine.addEventListener(MachineDataEvent.IsOpenChanged, OnIsOpenChanged);
		}
		public override function Dispose():void
		{
			_lobbyMachine.removeEventListener(MachineDataEvent.IsOpenChanged, OnIsOpenChanged);
			
			super.Dispose();
		}
		
		// events
		protected function OnIsOpenChanged(event:Event):void
		{
			visible = !_lobbyMachine.IsOpen && !_lobbyMachine.IsCommingSoon;
		}
	}
}