package com.slotim.slotim.ui.machine.bonusGameEngine.higherLower.risks
{
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.ui.common.components.base.BaseClickableButton;
	import com.slotim.slotim.utils.sounds.SoundsManager;
	import com.slotim.slotim.assets.bonusGame.higherLower.HigherLowerEmbed;
	
	import flash.events.MouseEvent;
	
	public class HigherButton extends BaseClickableButton
	{
		// class
		public function HigherButton()
		{
			super(60, 60, null, null, new HigherLowerEmbed.Higher(), new HigherLowerEmbed.Higher());
		}
		
		// events
		protected override function OnClick(event:MouseEvent):void
		{
			super.OnClick(event);
			Main.Instance.Sounds.Play(SoundsManager.Up);
		}
	}
}