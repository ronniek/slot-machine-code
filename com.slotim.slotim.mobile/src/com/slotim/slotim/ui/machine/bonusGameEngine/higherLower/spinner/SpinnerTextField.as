package com.slotim.slotim.ui.machine.bonusGameEngine.higherLower.spinner
{
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.ui.common.components.base.BaseTextField;
	import com.slotim.slotim.utils.helpers.FormatterHelper;
	
	import flash.text.TextField;
	
	public class SpinnerTextField extends BaseTextField
	{
		// properties
		protected override function get XFLTextField():TextField
		{
			return Main.Instance.TextFields.BonusGameHigherLowerSpinner;
		}
		protected override function get FrameCorner():Number
		{
			return 10;
		}
		
		// class
		public function SpinnerTextField(random:int)
		{
			super(100, 100, FormatterHelper.NumberToMoney(random, 0));
		}
	}
}