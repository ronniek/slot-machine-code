package com.slotim.slotim.ui.machine.bottomPanel.buttons
{
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.data.machine.MachineDataEvent;
	import com.slotim.slotim.ui.common.components.base.BaseSwitchableButton;
	import com.slotim.slotim.utils.sounds.SoundsManager;
	import com.slotim.slotim.ui.machine.bottomPanel.BottomPanelEvent;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	
	public class AutoSpinButton extends BaseSwitchableButton
	{
		// properties
		protected override function get XFLTextField():TextField
		{
			return Main.Instance.TextFields.MachineBottomPanelAuto;
		}
		
		protected override function get HasNormalButton():Boolean
		{
			return true;
		}
		protected override function get NormalButtonColor():Number
		{
			return 0x0096ff;
		}
		protected override function get NormalButtonFrameColor():Number
		{
			return 0x00adf9;
		}
		protected override function get NormalButtonCorner():Number
		{
			return width * .4;
		}
		
		protected override function get HasSelectedButton():Boolean
		{
			return true;
		}
		protected override function get SelectedButtonColor():Number
		{
			return 0x00adf9;
		}
		protected override function get SelectedButtonFrameColor():Number
		{
			return 0x0096ff;
		}
		protected override function get SelectedButtonCorner():Number
		{
			return width * .4;
		}
		
		// class
		public function AutoSpinButton()
		{
			super(90, 60, null, "Auto", "Stop\nAuto");
			
			Main.Instance.ActiveMachine.addEventListener(MachineDataEvent.IsAutoSpinChanged, OnIsAutoSpinChanged);
		}
		public override function Dispose():void
		{
			Main.Instance.ActiveMachine.removeEventListener(MachineDataEvent.IsAutoSpinChanged, OnIsAutoSpinChanged);
			
			super.Dispose();
		}
		
		// events
		protected override function OnNormal(event:MouseEvent):void
		{
			Main.Instance.Sounds.Play(SoundsManager.Machine_BottomPanel_Auto_On);
			Main.Instance.ActiveMachine.IsAutoSpin = false;
		}
		protected override function OnSelected(event:MouseEvent):void
		{
			Main.Instance.Sounds.Play(SoundsManager.Machine_BottomPanel_Auto_Off);
			Main.Instance.ActiveMachine.IsAutoSpin = true;
		}
		
		protected function OnIsAutoSpinChanged(event:Event):void
		{
			if (Main.Instance.ActiveMachine.IsAutoSpin)
			{
				SetSelected();
				dispatchEvent(new Event(BottomPanelEvent.AutoSpinButtonClicked));
			}
			else
			{
				SetNormal();
			}
		}
	}
}