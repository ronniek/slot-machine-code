package com.slotim.slotim.ui.machine.bottomPanel.buttons
{
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.ui.notifications.NotificationsHandler;
	import com.slotim.slotim.ui.notifications.popup.machine.machineInfo.MachineInfoPageEnum;
	import com.slotim.slotim.utils.sounds.SoundsManager;
	
	import flash.events.MouseEvent;
	import flash.text.TextField;
	
	public class PayTableButton extends BaseBottomPanelButton
	{
		// properties
		protected override function get XFLTextField():TextField
		{
			return Main.Instance.TextFields.MachineBottomPanelPayTable;
		}
		
		// class
		public function PayTableButton()
		{
			super(100, 100, "Pay\nTable");
		}
		
		// events
		protected override function OnClick(event:MouseEvent):void
		{
			super.OnClick(event);
			Main.Instance.Sounds.Play(SoundsManager.Machine_BottomPanel_PayTable);
			NotificationsHandler.Instance.ShowMachineInfoPopup(MachineInfoPageEnum.NormalSymbols, Main.Instance.ActiveMachine.LobbyMachine, null);
		}
	}
}