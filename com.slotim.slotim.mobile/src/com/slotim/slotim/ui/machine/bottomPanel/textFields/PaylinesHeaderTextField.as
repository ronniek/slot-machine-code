package com.slotim.slotim.ui.machine.bottomPanel.textFields
{
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.ui.common.components.base.BaseTextField;
	
	import flash.text.TextField;
	
	public class PaylinesHeaderTextField extends BaseTextField
	{
		// properties
		protected override function get XFLTextField():TextField
		{
			return Main.Instance.TextFields.MachineBottomPanelPaylinesHeader;
		}
		protected override function get TextFieldOffsetY():int
		{
			return -5;
		}
		
		// class
		public function PaylinesHeaderTextField()
		{
			super(100, 30, "Lines");
		}
	}
}