package com.slotim.slotim.ui.machine.bottomPanel.textFields
{
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.ui.common.components.base.BaseTextField;
	
	import flash.text.TextField;
	
	public class TotalBetHeaderTextField extends BaseTextField
	{
		// properties
		protected override function get XFLTextField():TextField
		{
			return Main.Instance.TextFields.MachineBottomPanelTotalBetHeader;
		}
		
		// class
		public function TotalBetHeaderTextField()
		{
			super(120, 70, "Total\NBet");
		}
	}
}