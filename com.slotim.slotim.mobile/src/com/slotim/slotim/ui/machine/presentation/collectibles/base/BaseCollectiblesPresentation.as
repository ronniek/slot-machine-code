package com.slotim.slotim.ui.machine.presentation.collectibles.base
{
	import com.slotim.slotim.ui.machine.SlotsMachineView;
	import com.slotim.slotim.ui.machine.presentation.base.BaseScatterPresentation;
	
	public class BaseCollectiblesPresentation extends BaseScatterPresentation
	{
		// class
		public function BaseCollectiblesPresentation(onClose:Function, view:SlotsMachineView)
		{
			super(onClose, view);
		}
	}
}