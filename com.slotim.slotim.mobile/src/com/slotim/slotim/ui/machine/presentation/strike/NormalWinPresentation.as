package com.slotim.slotim.ui.machine.presentation.strike
{
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.data.machine.valuator.strike.StrikeValuatorsData;
	import com.slotim.slotim.ui.machine.MachineView;
	import com.slotim.slotim.utils.helpers.FormatterHelper;
	import com.slotim.slotim.ui.machine.presentation.base.BasePresentation;
	
	public class NormalWinPresentation extends BasePresentation
	{
		// properties
		protected override function get ValuatorClass():Class
		{
			return StrikeValuatorsData;
		}
		protected override function get RemoveByTween():Boolean
		{
			return true;
		}
		
		// class
		public function NormalWinPresentation(onClick:Function, onClose:Function, view:MachineView)
		{
			super(306, onClick, onClose, view);
		}
		
		// methods
		protected override function DoPost():void
		{
			Main.Instance.ActiveMachine.Win += _valuatorsHandler.Strike.Chips;
			Main.Instance.Application.Ticker.PushMessage("You Won: " + FormatterHelper.NumberToMoney(Main.Instance.ActiveMachine.Win), "Normal Win: " + FormatterHelper.NumberToMoney(_valuatorsHandler.Strike.Chips), true);
			
			DoRemove();
		}
	}
}