package com.slotim.slotim.ui.main
{
	import com.slotim.slotim.ui.common.components.base.BasePng;
	import com.slotim.slotim.assets.common.SlotsEmbed;
	
	public class SplashBG extends BasePng
	{
		// properties
		protected override function get PngFilters():Array
		{
			return null;
		}
		
		// class
		public function SplashBG()
		{
			super(800, 494, new SlotsEmbed.Splash800x494(), 0);
		}
	}
}