package com.slotim.slotim.ui.notifications
{
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.data.application.prices.PriceOptionData;
	import com.slotim.slotim.data.lobby.lobbyMachine.base.BaseLobbyMachineData;
	import com.slotim.slotim.data.session.level.LevelData;
	import com.slotim.slotim.ui.notifications.base.BaseNotification;
	import com.slotim.slotim.ui.notifications.popup.ErrorPopup;
	import com.slotim.slotim.ui.notifications.popup.JackpotInfoPopup;
	import com.slotim.slotim.ui.notifications.popup.MessagePopup;
	import com.slotim.slotim.ui.notifications.popup.NewMachinePopup;
	import com.slotim.slotim.ui.notifications.popup.NoConnectionPopup;
	import com.slotim.slotim.ui.notifications.popup.PromptOnClosePopup;
	import com.slotim.slotim.ui.notifications.popup.TimerBonusLocalNotificationPopup;
	import com.slotim.slotim.ui.notifications.popup.WelcomePopup;
	import com.slotim.slotim.ui.notifications.popup.appsList.AppsListPopup;
	import com.slotim.slotim.ui.notifications.popup.bonusGame.BonusGameEndPopup;
	import com.slotim.slotim.ui.notifications.popup.bonusGame.BonusGameStartPopup;
	import com.slotim.slotim.ui.notifications.popup.buyChips.BuyingChipsPopup;
	import com.slotim.slotim.ui.notifications.popup.buyChips.ChipsBoughtPopup;
	import com.slotim.slotim.ui.notifications.popup.buyChips.CollectAmazonFreeChipsPopup;
	import com.slotim.slotim.ui.notifications.popup.buyChips.CollectFreeChipsPopup;
	import com.slotim.slotim.ui.notifications.popup.buyChips.NoChargePopup;
	import com.slotim.slotim.ui.notifications.popup.buyChips.buyChipsPopup.BuyChipsPopup;
	import com.slotim.slotim.ui.notifications.popup.connect.ConnectPopup;
	import com.slotim.slotim.ui.notifications.popup.connect.ConnectingPopup;
	import com.slotim.slotim.ui.notifications.popup.connect.LoggedOnPopup;
	import com.slotim.slotim.ui.notifications.popup.dailyBonus.DailyBonusPopup;
	import com.slotim.slotim.ui.notifications.popup.invite4Chips.Invite4ChipsPopup;
	import com.slotim.slotim.ui.notifications.popup.invite4Chips.Invited4ChipsPopup;
	import com.slotim.slotim.ui.notifications.popup.invite4Chips.Inviting4ChipsPopup;
	import com.slotim.slotim.ui.notifications.popup.invite4Unlock.Invite4UnlockPopup;
	import com.slotim.slotim.ui.notifications.popup.invite4Unlock.Invited4UnlockPopup;
	import com.slotim.slotim.ui.notifications.popup.invite4Unlock.Inviting4UnlockPopup;
	import com.slotim.slotim.ui.notifications.popup.level.LevelInfoPopup;
	import com.slotim.slotim.ui.notifications.popup.level.LevelUpPopup;
	import com.slotim.slotim.ui.notifications.popup.machine.FreeSpinsEndPopup;
	import com.slotim.slotim.ui.notifications.popup.machine.LowChipsPopup;
	import com.slotim.slotim.ui.notifications.popup.machine.NoChipsBuyChipsPopup;
	import com.slotim.slotim.ui.notifications.popup.machine.machineInfo.MachineInfoPopup;
	import com.slotim.slotim.ui.notifications.popup.settings.SettingsPopup;
	import com.slotim.slotim.utils.consts.AppHostsEnum;
	import com.slotim.slotim.utils.consts.Consts;
	import com.slotim.slotim.utils.ex.EventDispatcherEx;
	
	import flash.events.UncaughtErrorEvent;
	
	public class NotificationsHandler extends EventDispatcherEx
	{
		// singleton
		private static var _instance:NotificationsHandler;
		public static function get Instance():NotificationsHandler
		{
			if (_instance == null)
			{
				_instance = new NotificationsHandler();
			}
			
			return _instance;
		}
		
		// members
		protected var _notificationsPanel:NotificationsPanel;
		
		// properties
		public function get ShowingNotifications():Boolean
		{
			return (_notificationsPanel.numChildren > 0);
		}
		public function get NotificationType():BaseNotification
		{
			if (ShowingNotifications)
			{
				return BaseNotification(_notificationsPanel.getChildAt(0));
			}
			else
			{
				return null;
			}
		}
		
		// class
		public function NotificationsHandler()
		{
			super();
		}
		public function Init(notificationsPanel:NotificationsPanel):void
		{
			_notificationsPanel = notificationsPanel;
			
			Main.Instance.Device.DeviceStage.loaderInfo.uncaughtErrorEvents.addEventListener(UncaughtErrorEvent.UNCAUGHT_ERROR, OnUncaughtError);
		}
		
		// methods
		public function RemoveNotification():void
		{
			_notificationsPanel.RemoveNotifications();
		}
		
		// events
		protected function OnUncaughtError(event:UncaughtErrorEvent):void
		{
			ShowErrorPopup("UncaughtErrorEvent:" + event.error.toString(), null);
		}
		
		// popups
		public function ShowBuyChipsPopup(onlyCurrent:Boolean, onClose:Function):void
		{
			switch(Consts.APP_HOST)
			{
				case AppHostsEnum.AMAZON:
					_notificationsPanel.Push(new CollectAmazonFreeChipsPopup(onClose), true);
					break;
				case AppHostsEnum.GOOGLE_PLAY:
				case AppHostsEnum.ITUNES:
					_notificationsPanel.Push(new BuyChipsPopup(onlyCurrent, onClose), true);
					break;
				default:
					_notificationsPanel.Push(new BuyChipsPopup(onlyCurrent, onClose), true);
					break;
			}
		}
		public function ShowBuyingChipsPopup(priceOption:PriceOptionData, onClose:Function):void
		{
			_notificationsPanel.Push(new BuyingChipsPopup(priceOption, onClose), true);
		}
		public function ShowChipsBoughtPopup(priceOption:PriceOptionData, onClose:Function):void
		{
			_notificationsPanel.Push(new ChipsBoughtPopup(priceOption, onClose), true);
		}
		public function ShowCollectFreeChipsPopup(priceOption:PriceOptionData, onClose:Function):void
		{
			_notificationsPanel.Push(new CollectFreeChipsPopup(priceOption, onClose), true);
		}
		public function ShowNoChargePopup(onClose:Function):void
		{
			_notificationsPanel.Push(new NoChargePopup(onClose), true);
		}
		public function ShowConnectingPopup(onClose:Function):void
		{
			_notificationsPanel.Push(new ConnectingPopup(onClose), true);
		}
		public function ShowConnectPopup(onClose:Function):void
		{
			_notificationsPanel.Push(new ConnectPopup(onClose), true);
		}
		public function ShowDailyBonusPopup(onClose:Function):void
		{
			_notificationsPanel.Push(new DailyBonusPopup(onClose), true);
		}
		public function ShowErrorPopup(errorMessage:String, onClose:Function):void
		{
			_notificationsPanel.Push(new ErrorPopup(errorMessage, onClose), true);
		}
		
		public function ShowInvite4ChipsPopup(onClose:Function):void
		{
			_notificationsPanel.Push(new Invite4ChipsPopup(onClose), true);
		}
		public function ShowInviting4ChipsPopup(onClose:Function):void
		{
			_notificationsPanel.Push(new Inviting4ChipsPopup(onClose), true);
		}
		public function ShowInvited4ChipsPopup(count:int):void
		{
			_notificationsPanel.Push(new Invited4ChipsPopup(count), true);
		}
		
		public function ShowInvite4UnlockPopup(lobbyMachine:BaseLobbyMachineData, onClose:Function):void
		{
			_notificationsPanel.Push(new Invite4UnlockPopup(lobbyMachine, onClose), true);
		}
		public function ShowInviting4UnlockPopup(lobbyMachine:BaseLobbyMachineData, onClose:Function):void
		{
			_notificationsPanel.Push(new Inviting4UnlockPopup(lobbyMachine, onClose), true);
		}
		public function ShowInvited4UnlockPopup(lobbyMachine:BaseLobbyMachineData, count:int):void
		{
			_notificationsPanel.Push(new Invited4UnlockPopup(lobbyMachine, count), true);
		}
		
		public function ShowJackpotInfoPopup(onClose:Function):void
		{
			_notificationsPanel.Push(new JackpotInfoPopup(onClose));
		}
		public function ShowLevelInfoPopup(onClose:Function):void
		{
			_notificationsPanel.Push(new LevelInfoPopup(onClose));
		}
		public function ShowLevelUpPopup(level:LevelData, onClose:Function):void
		{
			_notificationsPanel.Push(new LevelUpPopup(level, onClose));
		}
		public function ShowLoggedOnPopup(onClose:Function):void
		{
			_notificationsPanel.Push(new LoggedOnPopup(onClose), true);
		}
		public function ShowLowChipsPopup(onClose:Function):void
		{
			if (Main.Instance.ActiveMachine)
			{
				_notificationsPanel.Push(new LowChipsPopup(onClose));
			}
		}
		public function ShowNewMachinePopup(lobbyMachine:BaseLobbyMachineData, onClose:Function):void
		{
			if (lobbyMachine.ID != Main.Instance.Session.Machines.MachinesSession[1].LobbyMachine.ID)
			{
				_notificationsPanel.Push(new NewMachinePopup(lobbyMachine, onClose));
			}
		}
		public function ShowNoChipsPopup(onClose:Function):void
		{
			if (Main.Instance.ActiveMachine)
			{
				_notificationsPanel.Push(new NoChipsBuyChipsPopup(onClose));
			}
		}
		public function ShowNoConnectionPopup(onClose:Function):void
		{
			_notificationsPanel.Push(new NoConnectionPopup(onClose));
		}
		public function ShowMachineInfoPopup(machineInfoPage:int, lobbyMachine:BaseLobbyMachineData, onClose:Function):void
		{
			_notificationsPanel.Push(new MachineInfoPopup(machineInfoPage, lobbyMachine, onClose));
		}
		public function ShowPromptOnClosePopup(onClose:Function):void
		{
			_notificationsPanel.Push(new PromptOnClosePopup(onClose));
		}
		public function ShowMenuSettingsPopup(onClose:Function):void
		{
			if (!ShowingNotifications)
			{
				_notificationsPanel.Push(new SettingsPopup(onClose));
			}
		}
		public function ShowSettingsPopup(onClose:Function):void
		{
			_notificationsPanel.Push(new SettingsPopup(onClose));
		}
		public function ShowTimerBonusLocalNotificationPopup():void
		{
			if (Main.Instance.Session.Rare.IsTimerBonusReady)
			{
				_notificationsPanel.Push(new TimerBonusLocalNotificationPopup());
			}
		}
		public function ShowWelcomePopup(onClose:Function):void
		{
			if (!Main.Instance.Session.Rare.IsWelcomeCollected)
			{
				_notificationsPanel.Push(new WelcomePopup(onClose));
			}
		}
		
		public function ShowMessagePopup(title:String, body:String):void
		{
			_notificationsPanel.Push(new MessagePopup(title, body));
		}
		public function ShowAppsListPopup():void
		{
			_notificationsPanel.Push(new AppsListPopup());
		}
		
		public function ShowBonusGameEndPopup(chips:Number, onClose:Function):void
		{
			_notificationsPanel.Push(new BonusGameEndPopup(chips, onClose));
		}
		public function ShowBonusGameStartPopup(onClose:Function):void
		{
			_notificationsPanel.Push(new BonusGameStartPopup(onClose));
		}
		public function ShowFreeSpinsEndPopup(onClose:Function):void
		{
			if (Main.Instance.ActiveMachine)
			{
				_notificationsPanel.Push(new FreeSpinsEndPopup(onClose));
			}
		}
	}
}