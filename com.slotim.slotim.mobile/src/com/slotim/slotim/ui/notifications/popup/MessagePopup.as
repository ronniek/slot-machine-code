package com.slotim.slotim.ui.notifications.popup
{
	import com.slotim.slotim.ui.common.components.base.BaseButton;
	import com.slotim.slotim.ui.notifications.popup.base.BasePopup;
	import com.slotim.slotim.ui.notifications.popup.base.PopupSizeTypeEnum;
	import com.slotim.slotim.ui.notifications.popup.buttons.MainPopupButton;
	
	import flash.events.MouseEvent;

	public class MessagePopup extends BasePopup
	{
		// members
		private var _title:String;
		private var _body:String;
		
		// properties
		protected override function get SizeType():String
		{
			return PopupSizeTypeEnum.MediumPopup;
		}
		
		protected override function get Title():String
		{
			return _title;
		}
		protected override function get Buttons():Vector.<BaseButton>
		{
			var result:Vector.<BaseButton> = new Vector.<BaseButton>();
			result.push(new MainPopupButton("OK", OnOKClick));
			return result;
		}
		
		protected override function get AutoCloseTimeout():int
		{
			return 0;
		}
		
		// class
		public function MessagePopup(title:String, body:String)
		{
			_title = title;
			_body = body;
			
			super(null);
		}
		protected override function AddBody():void
		{
			AddBlueMessageTextField(W, _body);
		}
		
		// events
		protected function OnOKClick(event:MouseEvent):void
		{
			DoRemove();
		}
	}
}