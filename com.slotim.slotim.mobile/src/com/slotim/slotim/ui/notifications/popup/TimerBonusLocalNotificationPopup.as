package com.slotim.slotim.ui.notifications.popup
{
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.ui.common.components.base.BaseButton;
	import com.slotim.slotim.ui.notifications.popup.base.BasePopup;
	import com.slotim.slotim.ui.notifications.popup.base.PopupSizeTypeEnum;
	import com.slotim.slotim.ui.notifications.popup.buttons.MainPopupButton;
	import com.slotim.slotim.utils.helpers.FormatterHelper;
	import com.slotim.slotim.data.machine.spline.SplinesData;
	
	import flash.events.Event;
	import flash.geom.Point;
	
	public class TimerBonusLocalNotificationPopup extends BasePopup
	{
		// properties
		protected override function get SizeType():String
		{
			return PopupSizeTypeEnum.MediumPopup;
		}
		
		protected override function get Title():String
		{
			return "Timer Bonus";
		}
		
		protected override function get Buttons():Vector.<BaseButton>
		{
			var result:Vector.<BaseButton> = new Vector.<BaseButton>();
			result.push(new MainPopupButton("Collect", OnCollectClick));
			return result;
		}
		
		// class
		public function TimerBonusLocalNotificationPopup()
		{
			super(null);
		}
		
		protected override function AddBody():void
		{
			AddBlueMessageTextField(W, "Collect your FREE");
			AddBlueMessageTextField(W, FormatterHelper.NumberToMoney(Main.Instance.Session.Wallet.GetLevel.TimerBonusChips));
			AddBlueMessageTextField(W, "chips");
		}
		
		// money trail
		protected override function DoPostMoneyTrails():void
		{
			Main.Instance.Session.Rare.DoCollectTimerBonus();
			DoRemove();
		}
		
		// events
		private function OnCollectClick(event:Event):void
		{
			DoMoneyTrails(new Point(Width / 2, Height - 50 - 6), new Point(SplinesData.BalancePoint.x - X, SplinesData.BalancePoint.y - Y + 48));
		}
	}
}