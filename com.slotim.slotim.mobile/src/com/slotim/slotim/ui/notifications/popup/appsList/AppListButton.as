package com.slotim.slotim.ui.notifications.popup.appsList
{
	import com.slotim.slotim.ui.common.components.base.BaseClickableButton;
	
	import flash.display.Bitmap;

	public class AppListButton extends BaseClickableButton
	{
		// class
		public function AppListButton(onClickDispatch:Function, normal:Bitmap, selected:Bitmap)
		{
			super(398, 246, onClickDispatch, "", normal, selected);
		}
	}
}