package com.slotim.slotim.ui.notifications.popup.appsList
{
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.ui.notifications.popup.base.BasePopup;
	import com.slotim.slotim.ui.notifications.popup.base.PopupSizeTypeEnum;
	
	import flash.events.Event;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;

	public class AppsListPopup extends BasePopup
	{
		// properties
		protected override function get SizeType():String
		{
			return PopupSizeTypeEnum.BigPopup;
		}
		
		protected override function get Title():String
		{
			return "New Free Apps";
		}
		
		protected override function get HasCloseButton():Boolean
		{
			return true;
		}
		
		// class
		public function AppsListPopup()
		{
			super(null);
		}
		protected override function AddBody():void
		{
			var appList:AppListButton = new AppListButton(OnAppClick, Main.Instance.Application.AppsList.Apps[0].LargePhotoPng, Main.Instance.Application.AppsList.Apps[0].LargePhotoPng);
			appList.x = (W - appList.width) / 2;
			appList.y = 50;
			addChild(appList);
		}
		
		// events
		private function OnAppClick(event:Event):void
		{
			var urlRequest:URLRequest = new URLRequest(Main.Instance.Application.AppsList.Apps[0].URL);
			navigateToURL(urlRequest, "_blank");
				
			DoRemove();
		}
	}
}