package com.slotim.slotim.ui.notifications.popup.base
{
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.data.MainEvent;
	import com.slotim.slotim.data.machine.MachineData;
	
	import flash.events.Event;
	
	public class BaseMachinePopup extends BasePopup
	{
		// members
		protected var _machine:MachineData;
		
		// class
		public function BaseMachinePopup(onClose:Function)
		{
			super(onClose);
			
			_machine = Main.Instance.ActiveMachine;
			
			Main.Instance.addEventListener(MainEvent.RemoveActiveMachineView, OnRemoveActiveMachineView);
		}
		public override function Dispose():void
		{
			Main.Instance.removeEventListener(MainEvent.RemoveActiveMachineView, OnRemoveActiveMachineView);
			
			super.Dispose();
		}
		
		// events
		protected function OnRemoveActiveMachineView(event:Event):void
		{
			DoRemove();
		}
	}
}