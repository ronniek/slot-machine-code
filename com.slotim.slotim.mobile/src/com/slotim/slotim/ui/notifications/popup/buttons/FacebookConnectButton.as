package com.slotim.slotim.ui.notifications.popup.buttons
{
	import com.slotim.slotim.assets.notifications.popup.PopupEmbed;
	import com.slotim.slotim.ui.common.components.base.BaseClickableButton;

	public class FacebookConnectButton extends BaseClickableButton
	{
		// class
		public function FacebookConnectButton(onClickDispatch:Function)
		{
			super(280, 109, onClickDispatch, "", new PopupEmbed.FacebookConnectNormal(), new PopupEmbed.FacebookConnectSelected());
		}
	}
}