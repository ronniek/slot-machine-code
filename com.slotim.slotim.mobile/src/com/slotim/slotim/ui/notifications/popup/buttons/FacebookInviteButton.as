package com.slotim.slotim.ui.notifications.popup.buttons
{
	import com.slotim.slotim.assets.notifications.popup.PopupEmbed;
	import com.slotim.slotim.ui.common.components.base.BaseClickableButton;
	
	public class FacebookInviteButton extends BaseClickableButton
	{
		// class
		public function FacebookInviteButton(onClickDispatch:Function)
		{
			super(230, 32, onClickDispatch, "", new PopupEmbed.FacebookInviteNormal(), new PopupEmbed.FacebookInviteSelected());
		}
	}
}