package com.slotim.slotim.ui.notifications.popup.buyChips
{
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.data.application.prices.PriceOptionData;
	import com.slotim.slotim.data.application.prices.ProductID;
	import com.slotim.slotim.ui.common.components.AndroidLoader;
	import com.slotim.slotim.ui.common.components.Spacer;
	import com.slotim.slotim.ui.notifications.NotificationsHandler;
	import com.slotim.slotim.ui.notifications.popup.base.BasePopup;
	import com.slotim.slotim.ui.notifications.popup.base.PopupSizeTypeEnum;
	import com.slotim.slotim.utils.xServices.inAppPurchase.base.InAppPurchaseHandlerEvent;
	
	import flash.events.Event;
	
	public class BuyingChipsPopup extends BasePopup
	{
		// members
		private var _priceOption:PriceOptionData;
		
		// properties
		protected override function get SizeType():String
		{
			return PopupSizeTypeEnum.SmallPopup;
		}
		
		protected override function get Title():String
		{
			return "Buy Chips";
		}
		
		protected override function get AutoCloseTimeout():int
		{
			return 60000; 
		}
		
		protected override function get HasCloseButton():Boolean
		{
			return true;
		}
		
		// class
		public function BuyingChipsPopup(priceOption:PriceOptionData, onClose:Function)
		{
			super(onClose);
			
			_priceOption = priceOption;
			
			Main.Instance.XServices.InAppPurchase.addEventListener(InAppPurchaseHandlerEvent.PurchaseSuccessed, OnPurchaseSuccessed);
			Main.Instance.XServices.InAppPurchase.addEventListener(InAppPurchaseHandlerEvent.PurchaseCanceled, OnPurchaseCanceled);
			Main.Instance.XServices.InAppPurchase.addEventListener(InAppPurchaseHandlerEvent.Failed, OnFailed);
			Main.Instance.XServices.InAppPurchase.Purchase(_priceOption.ProductID);
			
			Main.Instance.XServices.GoogleAnalytics.TrackPurchaseEventShowBuyingChipsPopup(ProductID.PriceByProductID(_priceOption.ProductID));
		}
		public override function Dispose():void
		{
			Main.Instance.XServices.InAppPurchase.removeEventListener(InAppPurchaseHandlerEvent.PurchaseSuccessed, OnPurchaseSuccessed);
			Main.Instance.XServices.InAppPurchase.removeEventListener(InAppPurchaseHandlerEvent.PurchaseCanceled, OnPurchaseCanceled);
			Main.Instance.XServices.InAppPurchase.removeEventListener(InAppPurchaseHandlerEvent.Failed, OnFailed);
			
			super.Dispose();
		}
		
		protected override function AddBody():void
		{
			AddComponent(new Spacer(6));
			AddBlueMessageTextField(W, "Connecting");
			AddComponent(new Spacer(6));
			var androidLoader:AndroidLoader = new AndroidLoader(100, 100);
			androidLoader.x = (W - androidLoader.width) / 2;
			AddComponent(androidLoader);
		}
		
		// events
		protected function OnPurchaseSuccessed(event:InAppPurchaseHandlerEvent):void
		{
			Main.Instance.XServices.InAppPurchase.removeEventListener(InAppPurchaseHandlerEvent.PurchaseSuccessed, OnPurchaseSuccessed);
			Main.Instance.XServices.InAppPurchase.removeEventListener(InAppPurchaseHandlerEvent.PurchaseCanceled, OnPurchaseCanceled);
			Main.Instance.XServices.InAppPurchase.removeEventListener(InAppPurchaseHandlerEvent.Failed, OnFailed);
			
			NotificationsHandler.Instance.ShowChipsBoughtPopup(_priceOption, null);
			Main.Instance.XServices.Parse.App.PurchaseComplete(_priceOption.ProductID, _priceOption.Chips);
			Main.Instance.XServices.GoogleAnalytics.TrackPurchaseTransaction(ProductID.DescriptionByProductID(_priceOption.ProductID), ProductID.PriceByProductID(_priceOption.ProductID), _priceOption.Chips, _priceOption.ProductID, "");
			Main.Instance.XServices.GoogleAnalytics.TrackPurchaseEventSuccessBuyingChipsPopup(ProductID.PriceByProductID(_priceOption.ProductID));
			DoRemove();
		}
		protected function OnPurchaseCanceled(event:Event):void
		{
			Main.Instance.XServices.InAppPurchase.removeEventListener(InAppPurchaseHandlerEvent.PurchaseSuccessed, OnPurchaseSuccessed);
			Main.Instance.XServices.InAppPurchase.removeEventListener(InAppPurchaseHandlerEvent.PurchaseCanceled, OnPurchaseCanceled);
			Main.Instance.XServices.InAppPurchase.removeEventListener(InAppPurchaseHandlerEvent.Failed, OnFailed);
			
			NotificationsHandler.Instance.ShowNoChargePopup(null);
			Main.Instance.XServices.GoogleAnalytics.TrackPurchaseEventFailedBuyingChipsPopup(ProductID.PriceByProductID(_priceOption.ProductID));
			DoRemove();
		}
		protected function OnFailed(event:Event):void
		{
			Main.Instance.XServices.InAppPurchase.removeEventListener(InAppPurchaseHandlerEvent.PurchaseSuccessed, OnPurchaseSuccessed);
			Main.Instance.XServices.InAppPurchase.removeEventListener(InAppPurchaseHandlerEvent.PurchaseCanceled, OnPurchaseCanceled);
			Main.Instance.XServices.InAppPurchase.removeEventListener(InAppPurchaseHandlerEvent.Failed, OnFailed);
			
			NotificationsHandler.Instance.ShowNoChargePopup(null);
			Main.Instance.XServices.GoogleAnalytics.TrackPurchaseEventCancelBuyingChipsPopup(ProductID.PriceByProductID(_priceOption.ProductID));
			DoRemove();
		}
		private function OnCancelClick(event:Event):void
		{
			NotificationsHandler.Instance.ShowNoChargePopup(null);
			Main.Instance.XServices.GoogleAnalytics.TrackPurchaseEventCancelBuyingChipsPopup(ProductID.PriceByProductID(_priceOption.ProductID));
			DoRemove();
		}
	}
}