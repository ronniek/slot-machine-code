package com.slotim.slotim.ui.notifications.popup.buyChips
{
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.data.application.prices.PriceOptionData;
	import com.slotim.slotim.ui.common.components.Spacer;
	import com.slotim.slotim.ui.common.components.base.BaseButton;
	import com.slotim.slotim.ui.notifications.popup.base.BasePopup;
	import com.slotim.slotim.ui.notifications.popup.base.PopupSizeTypeEnum;
	import com.slotim.slotim.ui.notifications.popup.buttons.MainPopupButton;
	import com.slotim.slotim.utils.helpers.FormatterHelper;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	public class CollectAmazonFreeChipsPopup extends BasePopup
	{
		// members
		private var _priceOption:PriceOptionData;
		
		// properties
		protected override function get SizeType():String
		{
			return PopupSizeTypeEnum.MediumPopup;
		}
		
		protected override function get Title():String
		{
			return "Actually Free Chips";
		}
		protected override function get Buttons():Vector.<BaseButton>
		{
			var result:Vector.<BaseButton> = new Vector.<BaseButton>();
			result.push(new MainPopupButton("Collect", OnCollectClick));
			return result;
		}
		
		// class
		public function CollectAmazonFreeChipsPopup(onClose:Function)
		{
			_priceOption = Main.Instance.Application.Prices.GetPriceOptions(false).FreePriceOption;
			
			super(onClose);
			
			Main.Instance.XServices.GoogleAnalytics.TrackPurchaseEventCollectAmazonFreeChipsPopup(_priceOption.Chips);
		}
		protected override function AddBody():void
		{
			AddBlueMessageTextField(W, "Collect your");
			AddComponent(new Spacer(4));
			AddBlueMessageTextField(W, FormatterHelper.NumberToMoney(_priceOption.Chips));
			AddComponent(new Spacer(4));
			AddBlueMessageTextField(W, "Free Chips");
		}
		
		// events
		private function OnCollectClick(event:MouseEvent):void
		{
			Main.Instance.Session.Wallet.CollectFreeChips(_priceOption.Chips);
			Main.Instance.XServices.GoogleAnalytics.TrackPurchaseEventCollectedAmazonFreeChipsPopup(SecondsSpent);
			DoRemove();
		}
		protected override function OnAutoCloseTimer(event:Event):void
		{
			Main.Instance.Session.Wallet.CollectFreeChips(_priceOption.Chips);
			Main.Instance.XServices.GoogleAnalytics.TrackPurchaseEventCollectedAmazonFreeChipsPopup(SecondsSpent);
			DoRemove();
		}
	}
}