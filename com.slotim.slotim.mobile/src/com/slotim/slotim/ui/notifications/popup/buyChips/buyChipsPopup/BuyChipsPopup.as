package com.slotim.slotim.ui.notifications.popup.buyChips.buyChipsPopup
{
	import com.slotim.slotim.assets.notifications.popup.buyChipsLevels.BuyChipsLevelsEmbed;
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.data.application.prices.PriceOptionsData;
	import com.slotim.slotim.data.application.prices.ProductID;
	import com.slotim.slotim.ui.notifications.NotificationsHandler;
	import com.slotim.slotim.ui.notifications.popup.base.BasePopup;
	import com.slotim.slotim.ui.notifications.popup.buyChips.buyChipsPopup.buyChipsItem.BuyChipsItem;
	import com.slotim.slotim.ui.notifications.popup.buyChips.buyChipsPopup.buyChipsItem.BuyChipsItemEvent;
	
	import flash.display.Bitmap;
	import flash.events.Event;
	
	public class BuyChipsPopup extends BasePopup
	{
		// memebrs
		private var _priceOptions:PriceOptionsData;
		
		// properties
		protected override function get Width():int
		{
			return 750;
		}
		protected override function get Height():int
		{
			return 350;
		}
		
		protected override function get AutoCloseTimeout():int
		{
			return 30000;
		}
		
		protected override function get Title():String
		{
			return _priceOptions.Description;
		}
		
		protected override function get HasCloseButton():Boolean
		{
			return true;
		}
		
		// class
		public function BuyChipsPopup(onlyCurrent:Boolean, onClose:Function)
		{
			_priceOptions = Main.Instance.Application.Prices.GetPriceOptions(onlyCurrent);
			
			super(onClose);
			
			Main.Instance.XServices.GoogleAnalytics.TrackPurchasePopupView();
			Main.Instance.XServices.GoogleAnalytics.TrackPurchaseEventShowBuyChipsPopup();
		}
		protected override function AddBody():void
		{
			addChild(AddBuyChipsItem(0, 11, 110, new BuyChipsLevelsEmbed.ChipsLevelFree(), 0.7, 0x05e747));
			addChild(AddBuyChipsItem(1, 129, 127, new BuyChipsLevelsEmbed.ChipsLevel1(), 0.6, 0x53f0fb));
			addChild(AddBuyChipsItem(2, 264, 139, new BuyChipsLevelsEmbed.ChipsLevel2(), 0.7, 0x53f0fb));
			addChild(AddBuyChipsItem(3, 412, 152, new BuyChipsLevelsEmbed.ChipsLevel3(), 0.7, 0x53f0fb));
			addChild(AddBuyChipsItem(4, 572, 167, new BuyChipsLevelsEmbed.ChipsLevel4(), 0.75, 0x53f0fb));
		}
		
		// methods
		private function AddBuyChipsItem(index:int, x:int, w:int, normalPng:Bitmap, scale:Number, bgColor:Number):BuyChipsItem
		{
			var buyChipsItem:BuyChipsItem = new BuyChipsItem(w, _priceOptions, _priceOptions.PriceOptions[index], normalPng, scale, bgColor);
			buyChipsItem.addEventListener(BuyChipsItemEvent.Clicked, OnBuyChipsItemClicked);
			buyChipsItem.x = x;
			buyChipsItem.y = 50;
			
			return buyChipsItem;
		}
		
		// events
		protected function OnBuyChipsItemClicked(event:BuyChipsItemEvent):void
		{
			if (event.PriceOption.ProductID == ProductID.USD_FREE)
			{
				NotificationsHandler.Instance.ShowCollectFreeChipsPopup(event.PriceOption, null);
			}
			else
			{
				Main.Instance.XServices.GoogleAnalytics.TrackPurchaseEventClickBuyChipsPopup(ProductID.PriceByProductID(event.PriceOption.ProductID));
				NotificationsHandler.Instance.ShowBuyingChipsPopup(event.PriceOption, null);
			}
			
			DoRemove();
		}
		private function OnCloseClick(event:Event):void
		{
			Main.Instance.XServices.GoogleAnalytics.TrackPurchaseEventCloseBuyChipsPopup();
			DoRemove();
		}
	}
}