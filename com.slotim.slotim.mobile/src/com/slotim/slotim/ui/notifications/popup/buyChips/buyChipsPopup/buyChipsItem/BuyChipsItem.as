package com.slotim.slotim.ui.notifications.popup.buyChips.buyChipsPopup.buyChipsItem
{
	import com.greensock.TweenLite;
	import com.slotim.slotim.data.application.prices.PriceOptionData;
	import com.slotim.slotim.data.application.prices.PriceOptionsData;
	import com.slotim.slotim.data.application.prices.ProductID;
	import com.slotim.slotim.ui.common.components.SpriteEx;
	import com.slotim.slotim.ui.common.components.base.BaseClickableButton;
	import com.slotim.slotim.ui.notifications.popup.buyChips.buyChipsPopup.buyChipsItem.textFields.BuyChipsChipsTextField;
	import com.slotim.slotim.ui.notifications.popup.buyChips.buyChipsPopup.buyChipsItem.textFields.BuyChipsPriceTextField;
	
	import flash.display.Bitmap;
	import flash.events.MouseEvent;
	import flash.filters.GlowFilter;
	
	public class BuyChipsItem extends BaseClickableButton
	{
		// members
		private var _priceOptions:PriceOptionsData;
		private var _priceOption:PriceOptionData;
		private var _bitSprite:SpriteEx;
		
		// properties
		protected override function get FrameCorner():Number
		{
			return 15;
		}
		
		// class
		public function BuyChipsItem(w:int, priceOptions:PriceOptionsData, priceOption:PriceOptionData, normalPng:Bitmap, scale:Number, bgColor:Number)
		{
			super(w, 277, null, "", null, null, bgColor);
			
			normalPng.scaleX = scale;
			normalPng.scaleY = scale;
			normalPng.smoothing = true;
			normalPng.x = (width - normalPng.width ) / 2;
			normalPng.y = 175 - normalPng.height;
			addChild(normalPng);
			
			_priceOptions = priceOptions;
			_priceOption = priceOption;
			
			var buyChipsChipsCountTextField:BuyChipsChipsTextField = new BuyChipsChipsTextField(width, _priceOption.ChipsAsMoney);
			buyChipsChipsCountTextField.y = height - 32 - 32 - 34;
			addChild(buyChipsChipsCountTextField);
			
			var buyChipsChipsTitleTextField:BuyChipsChipsTextField = new BuyChipsChipsTextField(width, "Chips");
			buyChipsChipsTitleTextField.height = 25;
			buyChipsChipsTitleTextField.y = height - 32 - 34;
			addChild(buyChipsChipsTitleTextField);
			
			var buyChipsPriceTextField:BuyChipsPriceTextField = new BuyChipsPriceTextField(width, ProductID.DescriptionByProductID(_priceOption.ProductID));
			buyChipsPriceTextField.y = height - 34;
			addChild(buyChipsPriceTextField);
			
			if (priceOption.ProductID == ProductID.USD_FREE)
			{
				_bitSprite = new SpriteEx();
				StartBit();
			}
		}
		public override function Dispose():void
		{
			StopBit();
			TweenLite.killTweensOf(_bitSprite);
			
			if (_bitSprite)
			{
				_bitSprite = null;
			}
			
			super.Dispose();
		}
		
		// methods
		private function StartBit():void
		{
			TweenLite.to(_bitSprite, 0.5, {x:30, delay:0.5, onUpdate:OnBitTweenUpdate, onComplete:OnBitTweenComplete});
		}
		private function StopBit():void
		{
			TweenLite.killTweensOf(_bitSprite);
		}
		
		// events
		protected override function OnMouseDown(event:MouseEvent):void
		{
			this.filters = [new GlowFilter(0xffffff, 1, 20, 20)];
		}
		protected override function OnMouseUp(event:MouseEvent):void
		{
			this.filters = null;
		}
		protected override function OnRollOut(event:MouseEvent):void
		{
			this.filters = null;
		}
		protected override function OnClick(event:MouseEvent):void
		{
			dispatchEvent(new BuyChipsItemEvent(BuyChipsItemEvent.Clicked, _priceOption));
		}
		
		private function OnBitTweenUpdate():void
		{
			if (_bitSprite)
			{
				this.filters = [new GlowFilter(0xffffff, 1, 30 - _bitSprite.x, 30 - _bitSprite.x)];
			}
		}
		private function OnBitTweenComplete():void
		{
			if (_bitSprite)
			{
				_bitSprite.x = 0;
				TweenLite.to(_bitSprite, 0.5, {x:30, delay:0.5, onUpdate:OnBitTweenUpdate, onComplete:OnBitTweenComplete});
			}
		}
	}
}