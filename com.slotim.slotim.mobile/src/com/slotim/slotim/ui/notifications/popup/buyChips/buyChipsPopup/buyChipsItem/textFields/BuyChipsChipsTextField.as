package com.slotim.slotim.ui.notifications.popup.buyChips.buyChipsPopup.buyChipsItem.textFields
{
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.ui.common.components.base.BaseTextField;
	
	import flash.text.TextField;
	
	public class BuyChipsChipsTextField extends BaseTextField
	{
		// properties
		protected override function get XFLTextField():TextField
		{
			return Main.Instance.TextFields.BuyChipsChips;
		}
		
		// class
		public function BuyChipsChipsTextField(w:int, text:String)
		{
			super(w, 34, text);
		}
	}
}