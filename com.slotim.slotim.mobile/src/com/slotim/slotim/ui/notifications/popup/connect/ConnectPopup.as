package com.slotim.slotim.ui.notifications.popup.connect
{
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.ui.common.components.Spacer;
	import com.slotim.slotim.ui.common.components.base.BaseButton;
	import com.slotim.slotim.ui.notifications.NotificationsHandler;
	import com.slotim.slotim.ui.notifications.popup.base.BasePopup;
	import com.slotim.slotim.ui.notifications.popup.base.PopupSizeTypeEnum;
	import com.slotim.slotim.ui.notifications.popup.buttons.MainPopupButton;
	import com.slotim.slotim.utils.helpers.FormatterHelper;
	
	import flash.events.Event;
	
	public class ConnectPopup extends BasePopup
	{
		// properties
		protected override function get SizeType():String
		{
			return PopupSizeTypeEnum.MediumPopup;
		}
		
		protected override function get Title():String
		{
			return "Welcome back";
		}
		protected override function get AutoCloseTimeout():int
		{
			return 60000; 
		}
		protected override function get Buttons():Vector.<BaseButton>
		{
			var result:Vector.<BaseButton> = new Vector.<BaseButton>();
			result.push(new MainPopupButton("Connect", OnFacebookClick));
			return result;
		}
		protected override function get HasCloseButton():Boolean
		{
			return true;
		}
		
		// class
		public function ConnectPopup(onClose:Function)
		{
			super(onClose);
		}
		protected override function AddBody():void
		{
			AddBlueMessageTextField(W, "Connect to Facebook");
			if (!Main.Instance.Session.Rare.IsFacebookLoginCollected)
			{
				AddBlueMessageTextField(W, "and get");
				AddComponent(new Spacer(10));
				AddBlueMessageTextField(W,  FormatterHelper.NumberToMoney(Main.Instance.Session.Wallet.GetLevel.FacebookConnectBonusChips, 0) + " Chips");
			}
		}
		
		// events
		private function OnFacebookClick(event:Event):void
		{
			NotificationsHandler.Instance.ShowConnectingPopup(null);
			DoRemove();
		}
	}
}