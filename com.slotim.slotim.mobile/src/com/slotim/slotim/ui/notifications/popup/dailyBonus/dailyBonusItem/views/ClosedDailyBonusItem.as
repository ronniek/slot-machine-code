package com.slotim.slotim.ui.notifications.popup.dailyBonus.dailyBonusItem.views
{
	import com.slotim.slotim.assets.notifications.popup.dailyBonus.DailyBonusEmbed;
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.ui.common.components.base.BasePng;
	import com.slotim.slotim.ui.notifications.popup.dailyBonus.dailyBonusItem.views.textFields.DailyBonusItemButtonTextField;
	import com.slotim.slotim.ui.notifications.popup.dailyBonus.dailyBonusItem.views.textFields.DailyBonusItemDayTextField;
	import com.slotim.slotim.ui.notifications.popup.dailyBonus.dailyBonusItem.views.textFields.DailyBonusItemPriceTextField;
	import com.slotim.slotim.utils.helpers.FormatterHelper;
	
	public class ClosedDailyBonusItem extends BasePng
	{
		// class
		public function ClosedDailyBonusItem(index:int)
		{
			super(119, 198, new DailyBonusEmbed.Violet());
			
			addChild(new DailyBonusItemDayTextField("Day " + (index + 1)));
			addChild(new DailyBonusItemPriceTextField(FormatterHelper.NumberToMoney(Main.Instance.Session.Wallet.GetLevel.LevelReachedBonusChips * (index + 1), 0, 0) + "\nChips"));
			addChild(new DailyBonusItemButtonTextField("Next_Day"));
		}
	}
}