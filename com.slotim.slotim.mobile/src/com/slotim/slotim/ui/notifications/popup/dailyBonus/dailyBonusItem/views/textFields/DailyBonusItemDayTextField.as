package com.slotim.slotim.ui.notifications.popup.dailyBonus.dailyBonusItem.views.textFields
{
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.ui.common.components.base.BaseTextField;
	
	import flash.text.TextField;
	
	public class DailyBonusItemDayTextField extends BaseTextField
	{
		// properties
		protected override function get XFLTextField():TextField
		{
			return Main.Instance.TextFields.DailyBonusDay;
		}
		
		// class
		public function DailyBonusItemDayTextField(text:String)
		{
			super(119, 38, text);
			
			x = 0;
			y = 57;
		}
	}
}