package com.slotim.slotim.ui.notifications.popup.machine
{
	import com.slotim.slotim.ui.common.components.base.BaseButton;
	import com.slotim.slotim.ui.notifications.NotificationsHandler;
	import com.slotim.slotim.ui.notifications.popup.base.BasePopup;
	import com.slotim.slotim.ui.notifications.popup.base.PopupSizeTypeEnum;
	import com.slotim.slotim.ui.notifications.popup.buttons.SecondaryPopupButton;
	import com.slotim.slotim.ui.notifications.popup.textFields.PopupBlueMessageTextField;
	
	import flash.events.Event;
	
	public class NoChipsBuyChipsPopup extends BasePopup
	{
		// members
		private var _popupBlueMessage:PopupBlueMessageTextField;
		
		// properties
		protected override function get SizeType():String
		{
			return PopupSizeTypeEnum.MediumPopup;
		}
		
		protected override function get HasCloseButton():Boolean
		{
			return true;
		}
		
		protected override function get Title():String
		{
			return "No Chips";
		}
		protected override function get Buttons():Vector.<BaseButton>
		{
			var result:Vector.<BaseButton> = new Vector.<BaseButton>();
			result.push(new SecondaryPopupButton("Free Chips", OnBuyChipsClick));
			return result;
		}
		
		// class
		public function NoChipsBuyChipsPopup(onClose:Function)
		{
			super(onClose);
		}
		protected override function AddBody():void
		{
			AddBlueMessageTextField(W, "You ran out of Chips");
		}
		
		// events
		private function OnBuyChipsClick(event:Event):void
		{
			NotificationsHandler.Instance.ShowBuyChipsPopup(false, null);
			DoRemove();
		}
	}
}