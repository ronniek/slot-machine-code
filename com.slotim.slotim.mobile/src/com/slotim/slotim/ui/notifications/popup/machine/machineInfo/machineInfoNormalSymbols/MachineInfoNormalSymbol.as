package com.slotim.slotim.ui.notifications.popup.machine.machineInfo.machineInfoNormalSymbols
{
	import com.slotim.slotim.data.lobby.lobbyMachine.base.BaseLobbyMachineData;
	import com.slotim.slotim.ui.common.components.base.BaseBG;
	import com.slotim.slotim.utils.helpers.FormatterHelper;
	import com.slotim.slotim.data.machine.symbol.base.BaseSymbolData;
	import com.slotim.slotim.ui.machine.Symbol;
	import com.slotim.slotim.ui.notifications.popup.machine.machineInfo.machineInfoNormalSymbols.textFields.MachineInfoNormalSymbolPayoutTextField;
	
	public class MachineInfoNormalSymbol extends BaseBG
	{
		// members
		private var _lobbyMachine:BaseLobbyMachineData;
		
		// properties
		protected override function get HasFrame():Boolean
		{
			return false;
		}
		protected override function get FrameCorner():Number
		{
			return 10;
		}
		
		// class
		public function MachineInfoNormalSymbol(lobbyMachine:BaseLobbyMachineData, symbol:BaseSymbolData)
		{
			_lobbyMachine = lobbyMachine;
			
			super(383, 116, 0xffa8cb);
			
			var png:Symbol = symbol.GetSymbol(_lobbyMachine.Paylines.Payboxes[0].Rect);
			png.x = 8;
			png.y = 8;
			png.width = 100;
			png.height = 100;
			addChild(png);
			
			var currentY:int = 8;
			var machineInfoSymbolPayout:MachineInfoNormalSymbolPayoutTextField;
			
			if (_lobbyMachine.Paylines.Paylines[0].ColumnsCount >= 5 && symbol.FiveInARowPayout > 0)
			{
				machineInfoSymbolPayout = new MachineInfoNormalSymbolPayoutTextField(275, 20, FormatterHelper.NumberToMoney(5, 0, 0) + " - " + FormatterHelper.NumberToMoney(symbol.FiveInARowPayout, 0, 0) + " Chips");
				machineInfoSymbolPayout.x = 108;
				machineInfoSymbolPayout.y = currentY;
				addChild(machineInfoSymbolPayout);
				currentY += machineInfoSymbolPayout.height;
			}
			
			if (_lobbyMachine.Paylines.Paylines[0].ColumnsCount >= 4 && symbol.FourInARowPayout > 0)
			{
				machineInfoSymbolPayout = new MachineInfoNormalSymbolPayoutTextField(275, 20, FormatterHelper.NumberToMoney(4, 0, 0) + " - " + FormatterHelper.NumberToMoney(symbol.FourInARowPayout, 0, 0) + " Chips");
				machineInfoSymbolPayout.x = 108;
				machineInfoSymbolPayout.y = currentY;
				addChild(machineInfoSymbolPayout);
				currentY += machineInfoSymbolPayout.height;
			}
			
			if (symbol.ThreeInARowPayout > 0)
			{
				machineInfoSymbolPayout = new MachineInfoNormalSymbolPayoutTextField(275, 20, FormatterHelper.NumberToMoney(3, 0, 0) + " - " + FormatterHelper.NumberToMoney(symbol.ThreeInARowPayout, 0, 0) + " Chips");
				machineInfoSymbolPayout.x = 108;
				machineInfoSymbolPayout.y = currentY;
				addChild(machineInfoSymbolPayout);
				currentY += machineInfoSymbolPayout.height;
			}
			
			if (symbol.TwoInARowPayout > 0)
			{
				machineInfoSymbolPayout = new MachineInfoNormalSymbolPayoutTextField(275, 20, FormatterHelper.NumberToMoney(2, 0, 0) + " - " + FormatterHelper.NumberToMoney(symbol.TwoInARowPayout, 0, 0) + " Chips");
				machineInfoSymbolPayout.x = 108;
				machineInfoSymbolPayout.y = currentY;
				addChild(machineInfoSymbolPayout);
				currentY += machineInfoSymbolPayout.height;
			}
		}
	}
}