package com.slotim.slotim.ui.notifications.popup.machine.machineInfo.machineInfoNormalSymbols.textFields
{
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.ui.common.components.base.BaseTextField;
	
	import flash.text.TextField;
	
	public class MachineInfoNormalSymbolPayoutTextField extends BaseTextField
	{
		// properties
		protected override function get XFLTextField():TextField
		{
			return Main.Instance.TextFields.MachineInfoNormalSymbolPayout;
		}
		
		// class
		public function MachineInfoNormalSymbolPayoutTextField(w:int, h:int, text:String)
		{
			super(w, h, text);
		}
	}
}