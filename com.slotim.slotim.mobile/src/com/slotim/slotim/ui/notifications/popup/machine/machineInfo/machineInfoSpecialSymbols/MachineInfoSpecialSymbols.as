package com.slotim.slotim.ui.notifications.popup.machine.machineInfo.machineInfoSpecialSymbols
{
	import com.slotim.slotim.data.lobby.lobbyMachine.base.BaseLobbyMachineData;
	import com.slotim.slotim.ui.common.components.base.BaseComponent;
	import com.slotim.slotim.ui.common.components.freeActionScript.scrollable.vScrollbar.BaseVScrollbar;
	import com.slotim.slotim.data.machine.symbol.SymbolsData;
	import com.slotim.slotim.data.machine.symbol.base.BaseSymbolData;
	
	public class MachineInfoSpecialSymbols extends BaseVScrollbar
	{
		// members
		private var _lobbyMachine:BaseLobbyMachineData;
		private var _symbols:SymbolsData;
		
		// properties
		protected override function get Content():BaseComponent
		{
			var content:BaseComponent = new BaseComponent(421, 100 * _symbols.SpecialSymbols.length);
			
			var i:int;
			var symbol:BaseSymbolData;
			var currentY:int;
			
			for (i = 0; i < _symbols.SpecialSymbols.length; i++)
			{
				symbol = _symbols.SpecialSymbols[i];
				
				var machineInfoSpecialSymbol:MachineInfoSpecialSymbol = new MachineInfoSpecialSymbol(_lobbyMachine, symbol);
				machineInfoSpecialSymbol.y = currentY;
				currentY += machineInfoSpecialSymbol.height + 2;
				
				content.addChild(machineInfoSpecialSymbol);
			}
			
			return content;
		}
		
		// class
		public function MachineInfoSpecialSymbols(lobbyMachine:BaseLobbyMachineData)
		{
			_lobbyMachine = lobbyMachine;
			_symbols = lobbyMachine.Symbols;
			
			super(421, 260);
		}
	}
}