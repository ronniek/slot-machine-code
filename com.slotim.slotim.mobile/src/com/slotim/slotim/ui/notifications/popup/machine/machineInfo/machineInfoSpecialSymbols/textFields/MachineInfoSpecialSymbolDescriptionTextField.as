package com.slotim.slotim.ui.notifications.popup.machine.machineInfo.machineInfoSpecialSymbols.textFields
{
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.ui.common.components.base.BaseTextField;
	
	import flash.text.TextField;
	
	public class MachineInfoSpecialSymbolDescriptionTextField extends BaseTextField
	{
		// properties
		protected override function get XFLTextField():TextField
		{
			return Main.Instance.TextFields.MachineInfoSymbolDescription;
		}
		
		// class
		public function MachineInfoSpecialSymbolDescriptionTextField(w:int, h:int, text:String)
		{
			super(w, h, text);
		}
	}
}