package com.slotim.slotim.ui.notifications.popup.settings.buttons
{
	import com.slotim.slotim.assets.notifications.popup.PopupEmbed;
	import com.slotim.slotim.assets.notifications.popup.settings.SettingsEmbed;
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.ui.common.components.base.BaseClickableButton;
	
	import flash.text.TextField;
	
	public class InviteFriendsButton extends BaseClickableButton
	{
		// properties
		protected override function get XFLTextField():TextField
		{
			return Main.Instance.TextFields.PopupButtonMain;
		}
		
		// class
		public function InviteFriendsButton()
		{
			super(36, 36, null, "", new SettingsEmbed.FacebookInviteNormal(), new SettingsEmbed.FacebookInviteSelected(), Number.NaN);
		}
	}
}