package com.slotim.slotim.ui.notifications.popup.textFields
{
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.ui.common.components.base.BaseTextField;
	
	import flash.text.TextField;
	
	public class PopupBlueMessageTextField extends BaseTextField
	{
		// properties
		protected override function get XFLTextField():TextField
		{
			return Main.Instance.TextFields.PopupMessageBlue;
		}
		
		// class
		public function PopupBlueMessageTextField(w:int, text:String)
		{
			super(w, 25, text);
		}
	}
}