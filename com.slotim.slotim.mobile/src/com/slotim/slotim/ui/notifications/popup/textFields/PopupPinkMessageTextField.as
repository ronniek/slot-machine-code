package com.slotim.slotim.ui.notifications.popup.textFields
{
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.ui.common.components.base.BaseTextField;
	
	import flash.text.TextField;
	
	public class PopupPinkMessageTextField extends BaseTextField
	{
		// properties
		protected override function get XFLTextField():TextField
		{
			return Main.Instance.TextFields.PopupMessagePink;
		}
		
		// class
		public function PopupPinkMessageTextField(w:int, text:String)
		{
			super(w, 25, text);
		}
	}
}