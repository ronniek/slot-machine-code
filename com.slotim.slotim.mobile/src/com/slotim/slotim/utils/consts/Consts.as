package com.slotim.slotim.utils.consts
{
	public class Consts
	{
		public static const GOLDEN_RATIO:Number = 1.6180339887498948482045868343656;
		public static const GOLDEN_PART_BIG:Number = 0.61803398874989484820458683436564;
		public static const GOLDEN_PART_SMALL:Number = 0.38196601125010515179541316563436;
		
		public static const DEFAULT_VOLUME:int = 1;
		
		public static const SERVER_URL:String = "http://www.slotim.com/apps/slotim";
		
		public static const APPLE_APP_ID:String = "1052280343";
		public static const APPLE_ITUNES_APP_URL:String = "https://itunes.apple.com/us/app/slotim/id1052280343?ls=1&mt=8";
		
		public static const GOOGLE_PLAY_LICENSE_KEY:String = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhzuGG8c8CPp7bUleDNGSd5mKC2dwWO9R0brLBl/fyEsXBcxQgqnHwFg9gs4a9lVexdYFQVYmkwxZ4oYtTLkDZR6KSYm4rnl3tJrLTylRNVGeaoKNs7MPq7QYCzdyZULfZjF1qAwyczyhZHaR/hvxoRqLyaZQ5RIiWlYXrH4I1Rj8NaQFWdh198xvElu1y6H6xUH8SlvMqixDR7/vTlJ1kHRJzUycQmhf6iF5imInh8q+Hd6MgwszUjrla3fIav5Prch1vSmHIVGiEb54jgR9HzcSOgX59UXuWQPfQbOU2NtAMMFAbhanz70T07aCHcdHGu/aHHXVsNqUF3D20mvVrQIDAQAB";
		public static const GOOGLE_PLAY_APP_URL:String = "https://play.google.com/store/apps/details?id=air.com.gotchaslots.slots";
		
		// facebook - ronnie.kleinfeld@gmail.com
		public static const FACEBOOK_APP_ID:String = "1435949483287908";
		public static const FACEBOOK_APP_PATH:String = "https://apps.facebook.com/slotim/";
		
		public static const AD_MOB_INTERSTITIAL_ANDROID:String = "ca-app-pub-6845341395144613/4423899486";
		public static const AD_MOB_BANNER_ANDROID:String = "ca-app-pub-6845341395144613/4770369483";
		public static const AD_MOB_INTERSTITIAL_IOS:String = "ca-app-pub-6845341395144613/8993699887";
		public static const AD_MOB_BANNER_IOS:String = "ca-app-pub-6845341395144613/7723835887";
		
		// parse
		SLOTS::ITUNES
			{
				public static const APP_HOST:String = AppHostsEnum.ITUNES;
				public static const APP_NAME:String = "Slotim";
				
				// google - ronnie.kleinfeld@gmail.com
				public static const GOOGLE_ANALYTICS_TRACKING_ID:String = "UA-69283621-1";
				
				public static const PARSE_APPLICATION_ID:String = "mxNN84Epj9Xh49VDY51RfmBYMqFzjJKVI2KkQDlM";
				public static const PARSE_REST_API_KEY:String = "t9pMvO8XlKpFdJFTKM4f6qCn3poxrrOZZ2xTH9BU";
			}
			SLOTS::GOOGLE_PLAY
			{
				public static const APP_HOST:String = AppHostsEnum.GOOGLE_PLAY;
				public static const APP_NAME:String = "GotchaSlots";
				
				// google - ronnie.kleinfeld@gmail.com
				public static const GOOGLE_ANALYTICS_TRACKING_ID:String = "UA-43695973-1";
				
				public static const PARSE_APPLICATION_ID:String = "2427pvd0O8hZMUtyc1h2qY7A3pc5O3KeiK3m8P4Q";
				public static const PARSE_REST_API_KEY:String = "fBwJrtiaDbVwdCSONRBwklpTrC9xhIjWjR7sAVjE";
			}
			SLOTS::AMAZON
			{
				public static const APP_HOST:String = AppHostsEnum.AMAZON;
				public static const APP_NAME:String = "Slotim";
				
				// google - ronnie.kleinfeld@gmail.com
				public static const GOOGLE_ANALYTICS_TRACKING_ID:String = "UA-69283621-1";
				
				public static const PARSE_APPLICATION_ID:String = "mxNN84Epj9Xh49VDY51RfmBYMqFzjJKVI2KkQDlM";
				public static const PARSE_REST_API_KEY:String = "t9pMvO8XlKpFdJFTKM4f6qCn3poxrrOZZ2xTH9BU";
			}
	}
}