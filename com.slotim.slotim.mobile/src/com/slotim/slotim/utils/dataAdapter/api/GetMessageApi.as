package com.slotim.slotim.utils.dataAdapter.api
{
	import com.slotim.slotim.data.application.message.MessageData;
	import com.slotim.slotim.utils.dataAdapter.api.base.BaseApi;
	import com.slotim.slotim.utils.helpers.JsonHelper;
	
	import flash.net.URLRequestMethod;
	
	public class GetMessageApi extends BaseApi
	{
		// consts
		public static const URL:String = "http://www.slotim.com/apps/common/message/message.json";
		
		// properties
		public override function get Url():String
		{
			return URL;
		}
		protected override function get Method():String
		{
			return URLRequestMethod.GET;
		}
		
		// methods
		public override function GetResponse():*
		{
			return MessageData(JsonHelper.MapJsonToClass(GetString(), MessageData));
		}
	}
}