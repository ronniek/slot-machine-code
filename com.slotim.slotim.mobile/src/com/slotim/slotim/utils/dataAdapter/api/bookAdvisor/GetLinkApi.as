package com.slotim.slotim.utils.dataAdapter.api.bookAdvisor
{
	import com.slotim.slotim.data.application.bookAdvisor.api.GetLinkApiData;
	import com.slotim.slotim.utils.dataAdapter.api.base.BaseApi;
	import com.slotim.slotim.utils.helpers.JsonHelper;
	
	import flash.net.URLRequestMethod;
	
	public class GetLinkApi extends BaseApi
	{
		// consts
		public static const URL:String = "http://www.slotim.com/apps/bookadvisor/GetLinkApi.php";
		
		// properties
		public override function get Url():String
		{
			return URL;
		}
		protected override function get Method():String
		{
			return URLRequestMethod.GET;
		}
		
		// methods
		public override function GetResponse():*
		{
			return GetLinkApiData(JsonHelper.MapJsonToClass(GetString(), GetLinkApiData));
		}
	}
}