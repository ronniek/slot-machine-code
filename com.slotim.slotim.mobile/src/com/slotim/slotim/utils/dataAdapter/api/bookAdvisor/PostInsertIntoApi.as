package com.slotim.slotim.utils.dataAdapter.api.bookAdvisor
{
	import com.slotim.slotim.data.application.bookAdvisor.api.PostInsertIntoApiData;
	import com.slotim.slotim.utils.dataAdapter.api.base.BaseApi;
	import com.slotim.slotim.utils.helpers.JsonHelper;
	
	import flash.net.URLRequestMethod;
	
	public class PostInsertIntoApi extends BaseApi
	{
		// consts
		public static const URL:String = "http://www.slotim.com/apps/bookadvisor/PostInsertIntoApi.php";
		
		// properties
		public override function get Url():String
		{
			return URL;
		}
		protected override function get Method():String
		{
			return URLRequestMethod.POST;
		}
		
		// class
		public function PostInsertIntoApi(sql:String)
		{
			super();
			
			AddParam("SQL", sql);
		}
		
		// methods
		public override function GetResponse():*
		{
			return PostInsertIntoApiData(JsonHelper.MapJsonToClass(GetString(), PostInsertIntoApiData));
		}
	}
}