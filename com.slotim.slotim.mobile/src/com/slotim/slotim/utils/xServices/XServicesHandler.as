package com.slotim.slotim.utils.xServices
{
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.utils.consts.DeviceTypeEnum;
	import com.slotim.slotim.utils.error.MustOverrideError;
	import com.slotim.slotim.utils.ex.EventDispatcherEx;
	import com.slotim.slotim.utils.xServices.Network.NetworkHandler;
	import com.slotim.slotim.utils.xServices.ads.AdMobHandler;
	import com.slotim.slotim.utils.xServices.bookAdvisor.BookAdvisorHandler;
	import com.slotim.slotim.utils.xServices.deviceID.DeviceIDHandler;
	import com.slotim.slotim.utils.xServices.googleAnalytics.GoogleAnalyticsHandler;
	import com.slotim.slotim.utils.xServices.inAppPurchase.AppStoreInAppPurchaseHandler;
	import com.slotim.slotim.utils.xServices.inAppPurchase.GooglePlayInAppPurchaseHandler;
	import com.slotim.slotim.utils.xServices.inAppPurchase.base.BaseInAppPurchaseHandler;
	import com.slotim.slotim.utils.xServices.internetTime.InternetTimeHandler;
	import com.slotim.slotim.utils.xServices.localNotifications.LocalNotificationsHandler;
	import com.slotim.slotim.utils.xServices.nativeDialogs.NativeDialogsHandler;
	import com.slotim.slotim.utils.xServices.parse.ParseHandler;
	import com.slotim.slotim.utils.xServices.social.SocialHandler;
	import com.slotim.slotim.utils.xServices.vibrate.VibrateHandler;
	
	public class XServicesHandler extends EventDispatcherEx
	{
		// members
		private var _adMob:AdMobHandler;
		private var _bookAdvisor:BookAdvisorHandler;
		private var _deviceID:DeviceIDHandler;
		private var _googleAnalytics:GoogleAnalyticsHandler;
		private var _inAppPurchase:BaseInAppPurchaseHandler;
		private var _internetTime:InternetTimeHandler;
		private var _localNotifications:LocalNotificationsHandler;
		private var _nativeDialogs:NativeDialogsHandler;
		private var _network:NetworkHandler;
		private var _parse:ParseHandler;
		private var _social:SocialHandler;
		private var _vibrate:VibrateHandler;
		
		// properties
		public function get AdMob():AdMobHandler
		{
			return _adMob;
		}
		public function get BookAdvisor():BookAdvisorHandler
		{
			return _bookAdvisor;
		}
		public function get DeviceID():DeviceIDHandler
		{
			return _deviceID;
		}
		public function get GoogleAnalytics():GoogleAnalyticsHandler
		{
			return _googleAnalytics;
		}
		public function get InAppPurchase():BaseInAppPurchaseHandler
		{
			return _inAppPurchase;
		}
		public function get InternetTime():InternetTimeHandler
		{
			return _internetTime;
		}
		public function get LocalNotifications():LocalNotificationsHandler
		{
			return _localNotifications;
		}
		public function get NativeDialogs():NativeDialogsHandler
		{
			return _nativeDialogs;
		}
		public function get Network():NetworkHandler
		{
			return _network;
		}
		public function get Parse():ParseHandler
		{
			return _parse;
		}
		public function get Social():SocialHandler
		{
			return _social;
		}
		public function get Vibrate():VibrateHandler
		{
			return _vibrate;
		}
		
		public function get AppUrl():String
		{
			throw new MustOverrideError();
		}
		
		// class
		public function XServicesHandler()
		{
			super();
			
			_adMob = new AdMobHandler();
			_bookAdvisor = new BookAdvisorHandler();
			_deviceID = new DeviceIDHandler();
			_internetTime = new InternetTimeHandler();
			_localNotifications = new LocalNotificationsHandler();
			_nativeDialogs = new NativeDialogsHandler();
			_network = new NetworkHandler();
			_parse = new ParseHandler();
			_social = new SocialHandler();
			_vibrate = new VibrateHandler();
		}
		public function Init():void
		{
			InitGoogleAnalytics();
			InitInAppPurchaseHandler();
			
			_adMob.LoadInterstitial();
		}
		private function InitGoogleAnalytics():void
		{
			_googleAnalytics = new GoogleAnalyticsHandler();
		}
		private function InitInAppPurchaseHandler():void
		{
			switch (Main.Instance.Device.DeviceType)
			{
				case DeviceTypeEnum.ANDORID:
					_inAppPurchase = new GooglePlayInAppPurchaseHandler();			
					break;
				case DeviceTypeEnum.IPHONE:
					_inAppPurchase = new AppStoreInAppPurchaseHandler();			
					break;
				default:
					_inAppPurchase = new GooglePlayInAppPurchaseHandler();
					break;
			}
		}
	}
}