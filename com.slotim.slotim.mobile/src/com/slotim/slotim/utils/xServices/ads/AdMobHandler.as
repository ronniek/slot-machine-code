package com.slotim.slotim.utils.xServices.ads
{
	import com.milkmangames.nativeextensions.AdMob;
	import com.milkmangames.nativeextensions.events.AdMobErrorEvent;
	import com.milkmangames.nativeextensions.events.AdMobEvent;
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.utils.consts.Consts;
	import com.slotim.slotim.utils.ex.EventDispatcherEx;
	
	public class AdMobHandler extends EventDispatcherEx
	{
		// members
		private var _initialized:Boolean;
		private var _adHeight:int;
		
		// properties
		public function get AdHeight():int
		{
			return _adHeight;
		}
		public function set AdHeight(adHeight:int):void
		{
			if (adHeight > _adHeight)
			{
				_adHeight = adHeight;
			}
		}
		public function get IsSupported():Boolean
		{
			return AdMob.isSupported;
		}
		
		// class
		public function AdMobHandler()
		{
			super();
			
			Init();
		}
		protected function Init():Boolean
		{
			if (!_initialized)
			{
				if(AdMob.isSupported)
				{
					AdMob.init(Consts.AD_MOB_BANNER_ANDROID, Consts.AD_MOB_BANNER_IOS);
					AdMob.addEventListener(AdMobEvent.RECEIVED_AD, OnReceiveAd);							// AdLoaded
					AdMob.addEventListener(AdMobEvent.SCREEN_PRESENTED, OnScreenPresented);					// AdShown
					AdMob.addEventListener(AdMobEvent.LEAVE_APPLICATION, OnLeaveApplication);				// UserInteraction
					AdMob.addEventListener(AdMobEvent.SCREEN_DISMISSED, OnScreenDismissed);					// AdFinished
					AdMob.addEventListener(AdMobErrorEvent.FAILED_TO_RECEIVE_AD, OnFailedToReceiveAd);		// FailedToLoadAd
					
					_initialized = true;
					
					CONFIG::DEBUG
						{
							// remove this line when you're done testing!
							//AdMob.enableTestDeviceIDs(AdMob.getCurrentTestDeviceIDs());
						}
				}
				else
				{
					_initialized = false;
				}
			}
			
			return _initialized;
		}
		
		// banner
		public function ShowBannerEx(type:String, horizontalAlign:String, verticalAlign:String, offsetX:int = 0, offsetY:int = 0):void
		{
			if (Init())
			{
				Main.Instance.XServices.GoogleAnalytics.TrackAdsShowBannerEx();
				AdMob.showAd(type, horizontalAlign, verticalAlign);
			}
		}
		public function RemoveBanner():void
		{
			if (Init())
			{
				AdMob.destroyAd();
			}
		}
		
		// interstitial
		public function LoadInterstitial():void
		{
			if (Init())
			{
				AdMob.loadInterstitial(Consts.AD_MOB_INTERSTITIAL_ANDROID, false, Consts.AD_MOB_INTERSTITIAL_IOS);
			}
		}
		public function ShowInterstitial():void
		{
			if (Init())
			{
				if (AdMob.isInterstitialReady())
				{
					AdMob.showPendingInterstitial();
				}
				else
				{
					AdMob.loadInterstitial(Consts.AD_MOB_INTERSTITIAL_ANDROID, true, Consts.AD_MOB_INTERSTITIAL_IOS);
				}
			}
		}
		public function RemoveInterstitial():void
		{
			if (Init())
			{
				AdMob.destroyAd();
			}
		}
		
		// events
		private function OnReceiveAd(event:AdMobEvent):void
		{
			Main.Instance.XServices.GoogleAnalytics.TrackAdsOnReceiveAd();
			
			AdHeight = event.dimensions.height;
			
			dispatchEvent(new AdsEvent(AdsEvent.AdLoaded, event.isInterstitial));
		}
		private function OnScreenPresented(event:AdMobEvent):void
		{
			dispatchEvent(new AdsEvent(AdsEvent.AdShown, event.isInterstitial));
		}
		private function OnLeaveApplication(event:AdMobEvent):void
		{
			Main.Instance.XServices.GoogleAnalytics.TrackAdsOnLeaveApplication();
			dispatchEvent(new AdsEvent(AdsEvent.UserInteraction, event.isInterstitial));
		}
		private function OnScreenDismissed(event:AdMobEvent):void
		{
			dispatchEvent(new AdsEvent(AdsEvent.AdFinished, event.isInterstitial));
		}
		private function OnFailedToReceiveAd(event:AdMobErrorEvent):void
		{
			Main.Instance.XServices.GoogleAnalytics.TrackAdsOnFailedToReceiveAd();
			dispatchEvent(new AdsEvent(AdsEvent.FailedToLoadAd, event.isInterstitial));
		}
	}	
}