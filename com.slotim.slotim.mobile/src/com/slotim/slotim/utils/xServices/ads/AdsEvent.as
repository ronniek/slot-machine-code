package com.slotim.slotim.utils.xServices.ads
{
	import flash.events.Event;
	
	public class AdsEvent extends Event
	{
		// consts
		public static const AdLoaded:String = "7cefe3d2d5824e7c88c04bcd4bb11525";
		public static const AdShown:String = "ec19aba8474d4027b9f99645cfeffd38";
		public static const UserInteraction:String = "1b915d711a9141d49b88badaaa581daa";
		public static const AdFinished:String = "192072bdeb5449f386fed3cd57a8d354";
		public static const FailedToLoadAd:String = "641328c6b1624e79b93e204efaa53763";
		
		//AdMob.addEventListener(AdMobEvent.RECEIVED_AD, OnReceiveAd);								// AdLoaded
		//AdMob.addEventListener(AdMobEvent.SCREEN_PRESENTED, OnScreenPresented);					// AdShown
		//AdMob.addEventListener(AdMobEvent.LEAVE_APPLICATION, OnLeaveApplication);					// UserInteraction
		//AdMob.addEventListener(AdMobEvent.SCREEN_DISMISSED, OnScreenDismissed);					// AdFinished
		//AdMob.addEventListener(AdMobErrorEvent.FAILED_TO_RECEIVE_AD, OnFailedToReceiveAd);		// FailedToLoadAd
		
		//IAd.iAd.addEventListener("BANNER_AD_LOADED", OnBannerAdLoader);							// AdLoaded
		//IAd.iAd.addEventListener("BANNER_ACTION_BEGIN", OnBannerActionBegin);						// UserInteraction
		//IAd.iAd.addEventListener("BANNER_AD_FAILED", OnBannerAdFailed);							// FailedToLoadAd
		//IAd.iAd.addEventListener("BANNER_ACTION_FINISHED", OnBannerActionFinished);
		
		//IAd.iAd.addEventListener("INTERSTITIAL_AD_LOADED", OnInterstitialAdLoaded);				// AdLoaded
		//IAd.iAd.addEventListener("INTERSTITIAL_SHOWN", OnInterstitialShown);						// AdShown
		//IAd.iAd.addEventListener("INTERSTITIAL_ACTION_BEGIN", OnInterstitialActionBegin);			// UserInteraction
		//IAd.iAd.addEventListener("INTERSTITIAL_DISMISSED", OnInterstitialDismissed);				// AdFinished
		//IAd.iAd.addEventListener("INTERSTITIAL_AD_FAILED", OnInterstitialAdFailed);				// FailedToLoadAd
		//IAd.iAd.addEventListener("INTERSTITIAL_ACTION_FINISHED", OnInterstitialActionFinished);
		//IAd.iAd.addEventListener("INTERSTITIAL_AD_UNLOADED", OnInterstitialAdUnloaded);
		
		// members
		private var _isInterstitial:Boolean;
		
		// properties
		public function get IsInterstitial():Boolean
		{
			return _isInterstitial;
		}
		
		// class
		public function AdsEvent(type:String, isInterstitial:Boolean)
		{
			super(type);
			
			_isInterstitial = isInterstitial;
		}
	}
}