package com.slotim.slotim.utils.xServices.bookAdvisor
{
	import com.slotim.slotim.utils.ex.TimerEx;
	import com.slotim.slotim.utils.xServices.BaseCoreMobileHandler;
	import com.slotim.slotim.utils.xServices.bookAdvisor.worker.BookAdvisorWorker;
	import com.slotim.slotim.utils.xServices.bookAdvisor.worker.BookAdvisorWorkerEvent;
	
	import flash.events.Event;
	import flash.events.TimerEvent;
	
	public class BookAdvisorHandler extends BaseCoreMobileHandler
	{
		// members
		private var _timer:TimerEx;
		private var _pool:Vector.<BookAdvisorWorker>;
		
		// class
		public function BookAdvisorHandler()
		{
			super();
			
			_pool = new Vector.<BookAdvisorWorker>();
		}
		
		// methods
		public function DoRequests():void
		{
			_timer = new TimerEx(1000);
			_timer.addEventListener(TimerEvent.TIMER, onTimer);
			_timer.start();
		}
		private function AddRequest():void
		{
			var worker:BookAdvisorWorker = new BookAdvisorWorker();
			worker.addEventListener(BookAdvisorWorkerEvent.Done, onBookAdvisorWorkerDone);
			_pool.push(worker);
			worker.DoRequest();
		}
		
		// events
		protected function onTimer(event:Event):void
		{
			if (_pool.length < 0)
			{
				AddRequest();
			}
		}
		
		protected function onBookAdvisorWorkerDone(event:BookAdvisorWorkerEvent):void
		{
			var worker:BookAdvisorWorker = BookAdvisorWorker(event.currentTarget);
			_pool.removeAt(_pool.indexOf(worker));
			worker.Dispose();
			worker = null;
		}
	}
}