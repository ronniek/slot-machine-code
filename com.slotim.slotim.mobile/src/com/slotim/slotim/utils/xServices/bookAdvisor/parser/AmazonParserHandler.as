package com.slotim.slotim.utils.xServices.bookAdvisor.parser
{
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.data.application.bookAdvisor.parser.BookCategoryData;
	import com.slotim.slotim.data.application.bookAdvisor.parser.BookContributorData;
	import com.slotim.slotim.data.application.bookAdvisor.parser.BookContributorEnumType;
	import com.slotim.slotim.data.application.bookAdvisor.parser.BookData;
	import com.slotim.slotim.data.application.bookAdvisor.parser.BookDataEvent;
	import com.slotim.slotim.data.application.bookAdvisor.parser.BookFormatData;
	import com.slotim.slotim.data.application.bookAdvisor.parser.BookPhotoData;
	import com.slotim.slotim.data.application.bookAdvisor.parser.BookRelatedBookData;
	import com.slotim.slotim.data.application.bookAdvisor.parser.BookRelatedBookEnumType;
	import com.slotim.slotim.utils.ex.EventDispatcherEx;
	
	public class AmazonParserHandler extends EventDispatcherEx
	{
		// memebrs
		private var _source:String;
		private var _book:BookData;
		
		// class
		public function AmazonParserHandler()
		{
			super();
		}
		public override function Dispose():void
		{
			_source = "";
			
			if (_book)
			{
				_book.removeEventListener(BookDataEvent.Complete, onBookComplete);
				_book.removeEventListener(BookDataEvent.Failed, onBookFailed);
				_book.Dispose();
				_book = null;
			}
		}
		
		// methods
		public function DoParse(link:String, source:String):void
		{
			try
			{
				_source = source;
				_book = new BookData();
				_book.link = link;
				_book.addEventListener(BookDataEvent.Complete, onBookComplete);
				_book.addEventListener(BookDataEvent.Failed, onBookFailed);
				_book.addEventListener(BookDataEvent.Duplicate, onBookDuplicate);
				
				// name
				_book.name = SubstringBetween('<span id="productTitle" class="a-size-large">', '</span>');
				
				// format
				var format:String = SubstringBetween('<span class="a-size-medium a-color-secondary a-text-normal">', '</span>');
				_book.formats.push(new BookFormatData(_book, format, "this"));
				
				// publish date
				_book.publishDate = SubstringBetween('<span class="a-size-medium a-color-secondary a-text-normal">&ndash; ', '</span>');
				
				// contributors
				var iBContributors:int = _source.indexOf('<div id="byline" class="a-section a-spacing-micro bylineHidden feature">');
				iBContributors = _source.indexOf('<span class="author notFaded" data-width="">', iBContributors);
				while (iBContributors >= 0)
				{
					var name:String = SubstringBetween('<span class="a-size-medium">', String.fromCharCode(10), iBContributors);
					var type:String = SubstringBetween('<span class="a-color-secondary">(', ')</span>', iBContributors);
					
					if (name.length > 0)
					{
						switch(type)
						{
							case "Author":
								_book.contributors.push(new BookContributorData(_book, name, BookContributorEnumType.Author));
								break;
							case "Illustrator":
								_book.contributors.push(new BookContributorData(_book, name, BookContributorEnumType.Illustrator));
								break;
							default:
								break;
						}
					}
					else
					{
						break;
					}
					
					iBContributors = _source.indexOf('<span class="author notFaded" data-width="">', iBContributors + 1);
				}
				
				// average customers reviews
				var iBAverageCustomerReviews:int = _source.indexOf('<div id="averageCustomerReviews" class="a-spacing-none"');
				
				var averageScore:String = SubstringBetween('<span class="a-icon-alt">', '</span></i>', iBAverageCustomerReviews);
				var averageScoreSplit:Array = averageScore.split(" ");
				_book.averageScore = averageScoreSplit[0];
				_book.averageScoreOf = averageScoreSplit[3];
				
				_book.customerReviewsCount = SubstringBetween('<span id="acrCustomerReviewText" class="a-size-base">', '</span>', iBAverageCustomerReviews);
				
				// category
				var iBCategories:int = _source.indexOf('<div class="badge-wrapper">');
				
				var rankInCategory:String = SubstringBetween("<span class='rank-number'>", '</span>', iBCategories);
				var categoryName:String = SubstringBetween("<span class='cat-link'>", '</span>', iBCategories);
				
				if (categoryName.length > 0)
				{
					_book.AddCategoryBook(new BookCategoryData(_book, "", categoryName, "", rankInCategory));
				}
				
				// formats
				var iBFormats:int = _source.indexOf('<div id="tmmSwatches" class="a-row nonJSFormats">');
				iBFormats = _source.indexOf('<span class="a-button a-spacing-mini a-button-toggle format"><span class="a-button-inner"><a href="', iBFormats);
				while (iBFormats >= 0)
				{
					var formatLink:String = SubstringBetween('<a href="', '/ref', iBFormats);
					var formatIsbn:String = formatLink.substring(formatLink.lastIndexOf("/") + 1);
					var formatType:String = SubstringBetween('<span>', '</span>', iBFormats);
					
					if (formatLink.length > 0)
					{
						if (formatType != _book.formats[0].name)
						{
							_book.formats.push(new BookFormatData(_book, formatType, "http://www.amazon.com" + formatLink));
							_book.AddRelatedBook(new BookRelatedBookData(_book, BookRelatedBookEnumType.OtherFormats, "http://www.amazon.com" + formatLink, formatIsbn));
						}
					}
					else
					{
						break;
					}
					
					iBFormats = _source.indexOf('<span class="a-button a-spacing-mini a-button-toggle format"><span class="a-button-inner"><a href="', iBFormats + 1);
				}
				
				// description
				var iBDescriptions:int = _source.indexOf('<div id="bookDescription_feature_div"');
				_book.description = SubstringBetween('<div>', '</div>', iBDescriptions);
				
				// frequently bought together
				var iBFrequents:int = _source.indexOf("<h2>Frequently Bought Together</h2>");
				iBFrequents = _source.indexOf('<li class="a-align-center sims-fbt-image-', iBFrequents);
				while (iBFrequents >= 0)
				{
					var frequentLink:String = SubstringBetween('<a class="a-link-normal" href="', '/ref=', iBFrequents);
					var frequentIsbn:String = frequentLink.substring(frequentLink.lastIndexOf("/") + 1);
					
					if (frequentLink.length > 0)
					{
						_book.AddRelatedBook(new BookRelatedBookData(_book, BookRelatedBookEnumType.FrequentlyBoughtTogether, "http://www.amazon.com" + frequentLink, frequentIsbn));
					}
					else
					{
						break;
					}
					
					iBFrequents = _source.indexOf('<li class="a-align-center sims-fbt-image-', iBFrequents + 1);
				}
				
				// customers who bought
				var iBCustomersWhoBoughts:int = _source.indexOf('<h2 class="a-carousel-heading">Customers Who Bought This Item Also Bought</h2>');
				iBCustomersWhoBoughts = _source.indexOf('<li class="a-carousel-card a-float-left" role="listitem">', iBCustomersWhoBoughts);
				while (iBCustomersWhoBoughts >= 0)
				{
					var customersWhoBoughtLink:String = SubstringBetween('<a class="a-link-normal" href="', '/ref=', iBCustomersWhoBoughts);
					var customersWhoBoughtIsbn:String = customersWhoBoughtLink.substring(customersWhoBoughtLink.lastIndexOf("/") + 1);
					if (customersWhoBoughtLink.length > 0)
					{
						_book.AddRelatedBook(new BookRelatedBookData(_book, BookRelatedBookEnumType.CustomersWhoBoughtThisItemAlsoBought, "http://www.amazon.com" + customersWhoBoughtLink, customersWhoBoughtIsbn));
					}
					else
					{
						break;
					}
					
					iBCustomersWhoBoughts = _source.indexOf('<li class="a-carousel-card a-float-left" role="listitem">', iBCustomersWhoBoughts + 1);
				}
				
				// product details
				var iBDetails:int = _source.indexOf('<h2>Product Details</h2>');
				
				// age range
				_book.ageRange = SubstringBetween('<li><b>Age Range:</b> ', '<br /></li>', iBDetails);
				
				// gradeLevel
				_book.gradeLevel = SubstringBetween('<li><b>Grade Level:</b> ', '</span><br /></li>', iBDetails);
				
				// lexileMeasure
				_book.lexileMeasure = SubstringBetween('<li><b>Lexile Measure:</b> ', ' <span ', iBDetails);
				
				// pagesCount
				_book.pagesCount = SubstringBetween("<li><b>" + _book.formats[0].name + ":</b> ", ' pages</li>', iBDetails);
				
				// publisher
				var publisher:String = SubstringBetween('<li><b>Publisher:</b> ', '</li>', iBDetails);
				try
				{
					_book.publisher = publisher.split(";")[0];
					_book.edition = publisher.split(";")[1];
				}
				catch (error:Error)
				{
				}
				
				// language
				_book.language = SubstringBetween('<li><b>Language:</b> ', '</li>', iBDetails);
				
				// isbn10
				_book.isbn10 = SubstringBetween('<li><b>ISBN-10:</b> ', '</li>', iBDetails);
				
				// isbn13
				_book.isbn13 = SubstringBetween('<li><b>ISBN-13:</b> ', '</li>', iBDetails);
				
				// weight
				_book.weight = SubstringBetween('<li><b>Shipping Weight:</b> ', ' (<a href="', iBDetails);
				
				// Sales Rank
				var iBBestSellerInBooks:int = _source.indexOf('<b>Amazon Best Sellers Rank:</b>');			
				var bestSellerInBook:String = SubstringBetween('#', ' (<a href="', iBBestSellerInBooks);
				if (bestSellerInBook.length > 0)
				{
					_book.AddCategoryBook(new BookCategoryData(_book, "", bestSellerInBook.split(" ")[2], "", bestSellerInBook.split(" ")[0]));				
				}
				
				var iBSalesRanks:int = _source.indexOf('<b>Amazon Best Sellers Rank:</b> ');
				iBSalesRanks = _source.indexOf('<li class="zg_hrsr_item">', iBSalesRanks);
				while(iBSalesRanks >= 0)
				{
					var amazonSalesRank:String = SubstringBetween('<span class="zg_hrsr_rank">#', '</span>', iBSalesRanks);
					var parentCategory:String = "";
					if (amazonSalesRank.length > 0)
					{
						var categoryNameSpan:String = SubstringBetween('<span class="zg_hrsr_ladder">in&nbsp;', '</span>', iBSalesRanks);
						var categoryNameSpans:Array = categoryNameSpan.split(" &gt; ");
						categoryNameSpans = categoryNameSpans.reverse();
						while(categoryNameSpans.length > 0)
						{
							var categoryNameHref:String = categoryNameSpans.pop();
							
							var categoryLink:String = SubstringInStringBetween(categoryNameHref, '<a href="', '">');
							var category:String = SubstringInStringBetween(categoryNameHref, '">', '</a>');
							if (category.length > 0)
							{
								_book.AddCategoryBook(new BookCategoryData(_book, parentCategory, category, categoryLink, ""));
								parentCategory = category;
							}
						}
					}
					else
					{
						break;
					}
					
					iBSalesRanks = _source.indexOf('<li class="zg_hrsr_item">', iBSalesRanks + 1);
				}
				
				// other items customers buy after viewing this item
				var iBBuyAfterThiss:int = _source.indexOf('<h2 class="a-size-large a-spacing-base">What Other Items Do Customers Buy After Viewing This Item?</h2>');
				iBBuyAfterThiss = _source.indexOf('<li data-fling-container="true" class="a-spacing-medium p13n-sc-list-item">', iBBuyAfterThiss);
				while (iBBuyAfterThiss >= 0)
				{
					var buyAfterThistLink:String = SubstringBetween('<a class="a-link-normal" href="', '/ref=', iBBuyAfterThiss);
					var buyAfterThistIsbn:String = buyAfterThistLink.substring(buyAfterThistLink.lastIndexOf("/") + 1);
					if (buyAfterThistLink.length > 0)
					{
						_book.AddRelatedBook(new BookRelatedBookData(_book, BookRelatedBookEnumType.OtherItemsCustomersBuyAfterViewingThisItem, "http://www.amazon.com" + buyAfterThistLink, buyAfterThistIsbn));
					}
					else
					{
						break;
					}
					
					iBBuyAfterThiss = _source.indexOf('<li data-fling-container="true" class="a-spacing-medium p13n-sc-list-item">', iBBuyAfterThiss + 1);
				}
				
				// photos
				var iBPhotos:int = _source.indexOf('<div id="imageBlockThumbs" class="');
				iBPhotos = _source.indexOf('<div class="a-column a-span3 a-spacing-micro imageThumb thumb">', iBPhotos);
				while (iBPhotos >= 0)
				{
					var photoLink:String = SubstringBetween('<img src="', '">', iBPhotos);
					
					if (photoLink.length > 0)
					{
						_book.photos.push(new BookPhotoData(_book, photoLink));
					}
					else
					{
						break;
					}
					
					iBPhotos = _source.indexOf('<div class="a-column a-span3 a-spacing-micro imageThumb thumb">', iBPhotos + 1);
				}
				
				Main.Instance.XServices.GoogleAnalytics.TrackBookAdvisorParsed();
				
				if (_book.name.length > 0)
				{
					_book.CheckBookExists();
				}
				else
				{
					dispatchEvent(new ParserHandlerEvent(ParserHandlerEvent.Failed));
				}
			}
			catch (error:Error)
			{
				dispatchEvent(new ParserHandlerEvent(ParserHandlerEvent.Failed));
			}
		}
		
		private function onBookComplete(event:BookDataEvent):void
		{
			dispatchEvent(new ParserHandlerEvent(ParserHandlerEvent.Complete));
		}
		private function onBookFailed(event:BookDataEvent):void
		{
			dispatchEvent(new ParserHandlerEvent(ParserHandlerEvent.Failed));
		}
		private function onBookDuplicate(event:BookDataEvent):void
		{
			dispatchEvent(new ParserHandlerEvent(ParserHandlerEvent.Duplicate));
		}
		
		private function SubstringBetween(before:String, after:String, startAt:Number = 0):String
		{
			return SubstringInStringBetween(_source, before, after, startAt);
		}
		private function SubstringInStringBetween(inString:String, before:String, after:String, startAt:Number = 0):String
		{
			var result:String = "";
			
			try
			{
				var iB:int = inString.indexOf(before, startAt);
				if (iB >= 0)
				{
					var iE:int = inString.indexOf(after, iB);
					if (iE >= 0)
					{
						result = inString.substring(iB + before.length, iE);
					}
				}
			}
			catch (error:Error)
			{
				result = "";
			}
			
			return result;
		}
	}
}