package com.slotim.slotim.utils.xServices.bookAdvisor.parser
{
	import flash.events.Event;
	
	public class ParserHandlerEvent extends Event
	{
		// consts
		public static const Complete:String = "cec3d67823684907a765c29be6712490";
		public static const Failed:String = "de4fece4c04a47ce973a15de230fe827";
		public static const Duplicate:String = "3c7f9c85247c45fc823434d9f147892d";
		
		// class
		public function ParserHandlerEvent(type:String)
		{
			super(type);
		}
	}
}