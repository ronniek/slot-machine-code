package com.slotim.slotim.utils.xServices.bookAdvisor.worker
{
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.data.application.bookAdvisor.api.GetLinkApiData;
	import com.slotim.slotim.data.application.bookAdvisor.base.BaseData;
	import com.slotim.slotim.utils.dataAdapter.api.bookAdvisor.GetLinkApi;
	import com.slotim.slotim.utils.dataAdapter.api.bookAdvisor.PostUpdateDeleteApi;
	import com.slotim.slotim.utils.dataAdapter.manager.DataAdapterHandler;
	import com.slotim.slotim.utils.ex.EventDispatcherEx;
	import com.slotim.slotim.utils.xServices.bookAdvisor.parser.AmazonParserHandler;
	import com.slotim.slotim.utils.xServices.bookAdvisor.parser.ParserHandlerEvent;
	
	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	public class BookAdvisorWorker extends EventDispatcherEx
	{
		// members
		private var _link:GetLinkApiData;
		
		private var _urlRequest:URLRequest;
		private var _urlLoader:URLLoader;
		
		// class
		public function BookAdvisorWorker()
		{
			super();
		}
		
		public function DoRequest():void
		{
			// transaction
			// 	get link
			// 	mark as fetching
			var dataAdapter:DataAdapterHandler = new DataAdapterHandler();
			dataAdapter.AddApi(new GetLinkApi());
			dataAdapter.addEventListener(Event.COMPLETE, OnGetDataAdapterComplete);
			dataAdapter.addEventListener(DataAdapterHandler.ServerError, OnGetDataAdapterServerError);
			dataAdapter.Execute();
		}
		protected function OnGetDataAdapterComplete(event:Event):void
		{
			try
			{
				var dataAdapter:DataAdapterHandler = DataAdapterHandler(event.currentTarget);
				_link = dataAdapter.GetResponse(GetLinkApi.URL);
				if (_link.ResultMessage == BaseData.OK)
				{
					DoRequestSource();
				}
			}
			catch (error:Error)
			{
			}
			finally
			{
				dataAdapter.Dispose();
			}
		}
		protected function OnGetDataAdapterServerError(event:Event):void
		{
			var dataAdapter:DataAdapterHandler = DataAdapterHandler(event.currentTarget);
			dataAdapter.Dispose();
		}
		
		private function DoRequestSource():void
		{
			// fetch
			_urlRequest = new URLRequest(_link.LNK_LINK);
			_urlLoader = new URLLoader(_urlRequest);
			_urlLoader.addEventListener(Event.COMPLETE, onURLRequestComplete);
			_urlLoader.load(_urlRequest);
		}
		private function onURLRequestComplete(event:Event):void
		{
			var source:String = String(_urlLoader.data);
			
			Main.Instance.XServices.GoogleAnalytics.TrackBookAdvisorGotSource();
			
			var amazonParser:AmazonParserHandler = new AmazonParserHandler();
			amazonParser.addEventListener(ParserHandlerEvent.Complete, onAmazonParserComplete);
			amazonParser.addEventListener(ParserHandlerEvent.Failed, onAmazonParserFailed);
			amazonParser.addEventListener(ParserHandlerEvent.Duplicate, onAmazonParserDuplicate);
			amazonParser.DoParse(_link.LNK_LINK, source);
		}
		public function onAmazonParserComplete(event:ParserHandlerEvent):void
		{
			var amazonParser:AmazonParserHandler = AmazonParserHandler(event.currentTarget);
			amazonParser.Dispose();
			
			UpdateAsFetched(3);
		}
		public function onAmazonParserFailed(event:ParserHandlerEvent):void
		{
			var amazonParser:AmazonParserHandler = AmazonParserHandler(event.currentTarget);
			amazonParser.Dispose();
			
			UpdateAsFetched(4);
		}
		public function onAmazonParserDuplicate(event:ParserHandlerEvent):void
		{
			var amazonParser:AmazonParserHandler = AmazonParserHandler(event.currentTarget);
			amazonParser.Dispose();
			
			UpdateAsFetched(5);
		}
		
		private function UpdateAsFetched(status:int):void
		{
			var sql:String = 'UPDATE T_LINK SET `LNK_STATUS_ID` = ' + status + ' WHERE `LNK_ID` = ' + _link.LNK_ID + ';';
			
			var dataAdapter:DataAdapterHandler = new DataAdapterHandler();
			dataAdapter.AddApi(new PostUpdateDeleteApi(sql));
			dataAdapter.addEventListener(Event.COMPLETE, OnPostFetchedDataAdapterComplete);
			dataAdapter.addEventListener(DataAdapterHandler.ServerError, OnPostFetchedDataAdapterServerError);
			dataAdapter.Execute();
		}
		protected function OnPostFetchedDataAdapterComplete(event:Event):void
		{
			try
			{
				var dataAdapter:DataAdapterHandler = DataAdapterHandler(event.currentTarget);
				dataAdapter.Dispose();
				
				Main.Instance.XServices.GoogleAnalytics.TrackBookAdvisorFetched();
				
				dispatchEvent(new BookAdvisorWorkerEvent(BookAdvisorWorkerEvent.Done));
			}
			catch (error:Error)
			{
				dispatchEvent(new BookAdvisorWorkerEvent(BookAdvisorWorkerEvent.Done));
			}
		}
		protected function OnPostFetchedDataAdapterServerError(event:Event):void
		{
			var dataAdapter:DataAdapterHandler = DataAdapterHandler(event.currentTarget);
			dataAdapter.Dispose();
			
			dispatchEvent(new BookAdvisorWorkerEvent(BookAdvisorWorkerEvent.Done));
		}
	}
}


