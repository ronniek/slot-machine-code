package com.slotim.slotim.utils.xServices.bookAdvisor.worker
{
	import flash.events.Event;
	
	public class BookAdvisorWorkerEvent extends Event
	{
		// consts
		public static const Done:String = "9c73a4029a274fd7ba3ce936d6fc0d9f";
		
		// class
		public function BookAdvisorWorkerEvent(type:String)
		{
			super(type);
		}
	}
}