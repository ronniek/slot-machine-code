package com.slotim.slotim.utils.xServices.googleAnalytics
{
	import com.milkmangames.nativeextensions.GAnalytics;
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.utils.consts.Consts;
	
	// http://www.google.com/analytics/
	// https://www.google.com/analytics/web/?hl=en#report/app-overview/a43695973w73793511p76190361/
	// https://www.google.com/analytics/web/?hl=en#realtime/rt-app-overview/a43695973w73793511p76190361/
	public class GoogleAnalyticsHandler
	{
		// consts
		private const MANUAL_DISPATCH:Boolean = false;
		
		// memebrs
		private var _initialized:Boolean;
		
		// properties
		private function get IsSupported():Boolean
		{
			return _initialized && GAnalytics.isSupported();
		}
		
		// class
		public function GoogleAnalyticsHandler()
		{
			if (GAnalytics.isSupported())
			{
				GAnalytics.create(Consts.GOOGLE_ANALYTICS_TRACKING_ID);
				
				_initialized = true;
			}
		}
		
		// methods
		public function ForseDispatch():void
		{
			if (IsSupported)
			{
				GAnalytics.analytics.forceDispatchHits();
			}
		}
		
		// view
		public function TrackMainView():void
		{
			TrackView("Main");
		}
		public function TrackLobbyView():void
		{
			TrackView("Lobby");
		}
		public function TrackMachineView(MachineID:int):void
		{
			TrackView("Machine:" + MachineID);
		}
		public function TrackBonusGameView(MachineID:int):void
		{
			TrackView("BonusGame for Machine:" + MachineID);
		}
		public function TrackPurchasePopupView():void
		{
			TrackView("PurchasePopup");
		}
		private function TrackView(view:String):void
		{
			if (IsSupported)
			{
				GAnalytics.analytics.defaultTracker.trackScreenView(view);
			}
		}
		
		// event
		public function TrackLogin():void
		{
			TrackEvent("Facebook", "Login", Main.Instance.XServices.Social.FacebookName);
		}
		public function TrackLogout():void
		{
			TrackEvent("Facebook", "Logout", Main.Instance.XServices.Social.FacebookName);
		}
		public function TrackInvite(count:int):void
		{
			TrackEvent("Facebook", "Invite", Main.Instance.XServices.Social.FacebookName, count);
		}
		public function TrackShare(share:String):void
		{
			TrackEvent("Facebook", share, Main.Instance.XServices.Social.FacebookName);
		}
		
		public function TrackCollectTimerBonus(chips:Number):void
		{
			TrackEvent("TimerBonus", "Collect(chips)", Main.Instance.Device.DeviceID, chips);
		}
		
		public function TrackMachineClose(machineName:String, timeOnMachine:Number, spinsOnMachine:int, freeSpinsOnMachine:int, chipsSpentOnMachine:Number):void
		{
			TrackEvent("Machine", "Time(ms)", machineName, timeOnMachine);
			TrackEvent("Machine", "Spins", machineName, spinsOnMachine);
			TrackEvent("Machine", "Free spins", machineName, freeSpinsOnMachine);
			TrackEvent("Machine", "Chips spent", machineName, chipsSpentOnMachine);
		}
		
		public function TrackAdsShowBannerEx():void
		{
			TrackEvent("Ads2", "1.ShowBannerEx", "", 1);
		}
		public function TrackAdsOnReceiveAd():void
		{
			TrackEvent("Ads2", "2.OnReceiveAd", "", 1);
		}
		public function TrackAdsOnLeaveApplication():void
		{
			TrackEvent("Ads2", "3.OnLeaveApplication", "", 1);
		}
		public function TrackAdsOnFailedToReceiveAd():void
		{
			TrackEvent("Ads2", "4.Failed", "", 1);
		}
		
		public function TrackPurchaseEventShowBuyChipsPopup():void
		{
			TrackEvent("Purchase4", "1.0.BuyChipsPopup_Show", "BuyChipsPopup", 1);
		}
		public function TrackPurchaseEventClickBuyChipsPopup(price:Number):void
		{
			TrackEvent("Purchase4", "1.1.BuyChipsPopup_ClickAnOption", "BuyChipsPopup", price);
		}
		public function TrackPurchaseEventCloseBuyChipsPopup():void
		{
			TrackEvent("Purchase4", "1.2.BuyChipsPopup_Close", "BuyChipsPopup", -1);
		}
		public function TrackPurchaseEventShowBuyingChipsPopup(price:Number):void
		{
			TrackEvent("Purchase4", "2.0.BuyingChipsPopup_Show", "BuyingChipsPopup", price);
		}
		public function TrackPurchaseEventSuccessBuyingChipsPopup(price:Number):void
		{
			TrackEvent("Purchase4", "2.1.BuyingChipsPopup_Success", "BuyingChipsPopup", price);
		}
		public function TrackPurchaseEventFailedBuyingChipsPopup(price:Number):void
		{
			TrackEvent("Purchase4", "2.2.BuyingChipsPopup_Failed", "BuyingChipsPopup", price);
		}
		public function TrackPurchaseEventCancelBuyingChipsPopup(price:Number):void
		{
			TrackEvent("Purchase4", "2.3.BuyingChipsPopup_Cancel", "BuyingChipsPopup", price);
		}
		public function TrackPurchaseEventShowChipsBoughtPopup(price:Number):void
		{
			TrackEvent("Purchase4", "3.0.ChipsBoughtPopup_Show", "ChipsBoughtPopup", price);
		}
		public function TrackPurchaseEventCollectChipsBoughtPopup(price:Number):void
		{
			TrackEvent("Purchase4", "3.1.ChipsBoughtPopup_Collect", "ChipsBoughtPopup", price);
			TrackEvent("$$$", "Purchase", "", price);
		}
		public function TrackPurchaseEventShowFreeChipsPopup(chips:Number):void
		{
			TrackEvent("Purchase4", "4.0.FreeChipsPopup_Show", "FreeChipsPopup", chips);
		}
		public function TrackPurchaseEventCollectFreeChipsPopup(seconds:int):void
		{
			TrackEvent("Purchase4", "4.1.FreeChipsPopup_Collect(seconds)", "FreeChipsPopup(seconds)", seconds);
		}
		public function TrackPurchaseEventCollectAmazonFreeChipsPopup(chips:Number):void
		{
			TrackEvent("Purchase4", "5.0.CollectAmazonFreeChipsPopup", "Show", chips);
		}
		public function TrackPurchaseEventCollectedAmazonFreeChipsPopup(chips:Number):void
		{
			TrackEvent("Purchase4", "5.1.CollectAmazonFreeChipsPopup", "Collected", chips);
		}
		public function TrackPurchaseEventShowNoChargePopup():void
		{
			TrackEvent("Purchase4", "9.0.NoChargePopup_Show", "NoChargePopup", -1);
		}
		public function TrackPurchaseEventOKNoChargePopup():void
		{
			TrackEvent("Purchase4", "9.1.NoChargePopup_OK", "NoChargePopup", -1);
		}
		
		public function TrackRateSelected():void
		{
			TrackEvent("Rate", "Rate Selected");
		}
		public function TrackLaterSelected():void
		{
			TrackEvent("Rate", "Later Selected");
		}
		public function TrackNeverSelected():void
		{
			TrackEvent("Rate", "Never Selected");
		}
		public function TrackPromptDisplayed():void
		{
			TrackEvent("Rate", "Prompt Displayed");
		}
		
		public function TrackOnLocalNotificationReceived(localNotificationID:int):void
		{
			TrackEvent("Local Notification Received", String(localNotificationID), String(Main.Instance.Session.Rare.LocalNotificationCounterReceived), 1);
		}
		public function TrackOnLocalNotificationOnResumed(localNotificationID:int):void
		{
			TrackEvent("Local Notification OnResumed", String(localNotificationID), String(Main.Instance.Session.Rare.LocalNotificationCounterOnResumed), 1);
		}
		
		// book advisor
		public function TrackBookAdvisorGotSource():void
		{
			TrackEvent("BookAdvisor", "GotSource", "", 1);
		}
		public function TrackBookAdvisorParsed():void
		{
			TrackEvent("BookAdvisor", "Parsed", "", 1);
		}
		public function TrackBookAdvisorBookAdded():void
		{
			TrackEvent("BookAdvisor", "BookAdded", "", 1);
		}
		public function TrackBookAdvisorBookCompleted():void
		{
			TrackEvent("BookAdvisor", "BookCompleted", "", 1);
		}
		public function TrackBookAdvisorFetched():void
		{
			TrackEvent("BookAdvisor", "Fetched", "", 1);
		}
		
		private function TrackEvent(category:String, action:String, label:String = null, value:Number = Number.NaN):void
		{
			if (IsSupported)
			{	
				GAnalytics.analytics.defaultTracker.trackEvent(category, action, label, value);
			}
		}
		
		// exception
		public function TrackFatalError(errorMessage:String):void
		{
			if (IsSupported)
			{
				GAnalytics.analytics.defaultTracker.trackException("Device:" + Main.Instance.Device.DeviceID + ":" + errorMessage, true);
			}
		}
		
		// ecommerce transaction
		// tracker.buildTransaction(UIDUtil.createUID(), 10.5)
		// 	.createProduct("cr-300", "300 credits pack", 7, 1).inCategory("credits").add()
		// 	.createProduct("it-156", "extra life item", 1.5, 2).add()
		// 	.track();
		public function TrackPurchaseTransaction(name:String, cost:Number, chips:Number, productID:String, description:String):void
		{
			TrackTransaction(Main.Instance.Device.DeviceID, cost, productID, name, cost, chips, description); 
		}
		private function TrackTransaction(id:String, cost:Number, sku:String, name:String, price:Number, quantity:uint, description:String):void
		{
			if (IsSupported)
			{
				GAnalytics.analytics.defaultTracker.trackTransaction(id, "", price - cost);
				GAnalytics.analytics.defaultTracker.trackItem(id, name, sku, price, null, quantity)
			}
		}
	}
}