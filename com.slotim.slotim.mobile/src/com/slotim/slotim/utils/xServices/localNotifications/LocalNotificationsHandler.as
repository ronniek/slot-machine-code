package com.slotim.slotim.utils.xServices.localNotifications
{
	import com.milkmangames.nativeextensions.CoreMobile;
	import com.milkmangames.nativeextensions.events.CMLocalNotificationEvent;
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.ui.notifications.NotificationsHandler;
	import com.slotim.slotim.utils.xServices.BaseCoreMobileHandler;
	
	public class LocalNotificationsHandler extends BaseCoreMobileHandler
	{
		// consts
		private const LOCAL_NOTIFICATION_TIMER_BONUS_ID:int = 0;
		
		// class
		public function LocalNotificationsHandler()
		{
			super();
		}
		
		// methods
		public function Register():void
		{
			if (Init())
			{
				CoreMobile.mobile.registerForNotifications();
				CoreMobile.mobile.addEventListener(CMLocalNotificationEvent.LOCAL_NOTIFICATION_RECEIVED, OnLocalNotificationReceived);
				CoreMobile.mobile.addEventListener(CMLocalNotificationEvent.RESUMED_FROM_LOCAL_NOTIFICATION, OnResumedFromLocalNotification);
				SetNotificationBadgeNumber(0);
				CancelAll();
				
				CoreMobile.mobile.scheduleRepeatingLocalNotification(LOCAL_NOTIFICATION_TIMER_BONUS_ID,
					CoreMobile.futureTimeSeconds(Main.Instance.Session.Rare.IsTimerBonusReady ? CoreMobile.MINUTE * 30 : CoreMobile.HOUR * 2),
					CoreMobile.futureTimeSeconds(CoreMobile.DAY * 5),
					"Timer Bonus Ready",
					"Get FREE chips",
					"Play now",
					{localNotificationID:LOCAL_NOTIFICATION_TIMER_BONUS_ID});
			}
		}
		
		// schedule
		/*
		// ScheduleLocalNotification(1, 3600, "Come back!", "Hey, you should come play some more.", "Play now", {chips:50, name:"Bob", type:"OneHour"});
		public function ScheduleLocalNotification(id:int, timeSeconds:Number, title:String, message:String, alertLabel:String = null, extraData:Object = null, badgeNumber:uint = 1, soundName:String = null, bigPictureUri:String = null, largeIconUri:String = null):void
		{
		if (Init())
		{
		CoreMobile.mobile.scheduleLocalNotification(id, timeSeconds, title, message, alertLabel, extraData, badgeNumber, soundName, bigPictureUri, largeIconUri);
		}
		}
		// ScheduleRepeatingLocalNotification(1, 3600, 60, "Come back!", "Hey, you should come play some more.", "Play now", {chips:50, name:"Bob", type:"OneHour"});
		public function ScheduleRepeatingLocalNotification(id:int, timeSeconds:Number, thenEverySeconds:Number, title:String, message:String, alertLabel:String = null, extraData:Object = null, badgeNumber:uint = 1, soundName:String = null, bigPictureUri:String = null, largeIconUri:String = null):void
		{
		if (Init())
		{
		CoreMobile.mobile.scheduleRepeatingLocalNotification(id, timeSeconds, thenEverySeconds, title, message, alertLabel, extraData, badgeNumber, soundName, bigPictureUri, largeIconUri);
		}
		}
		*/
		
		public function SetNotificationBadgeNumber(number:uint):void
		{
			if (Init())
			{
				CoreMobile.mobile.setNotificationBadgeNumber(number);
			}
		}
		
		public function Cancel(id:int):void
		{
			if (Init())
			{
				CoreMobile.mobile.cancelLocalNotification(id);
			}
		}
		public function CancelAll():void
		{
			if (Init())
			{
				CoreMobile.mobile.cancelAllLocalNotifications();
			}
		}
		
		// listener
		private function OnLocalNotificationReceived(event:CMLocalNotificationEvent):void
		{
			Main.Instance.Session.Rare.IncreaseLocalNotificationCounterReceived();
			Main.Instance.XServices.GoogleAnalytics.TrackOnLocalNotificationReceived(event.extra.localNotificationID);
			dispatchEvent(new LocalNotificationsEvent(LocalNotificationsEvent.LOCAL_NOTIFICATION_RECEIVED, event.title, event.message, event.extra, event.extraJson));
			switch (event.extra.localNotificationID)
			{
				case LOCAL_NOTIFICATION_TIMER_BONUS_ID:
					SetNotificationBadgeNumber(1);
					break;
				default:
					SetNotificationBadgeNumber(0);
			}
		}
		private function OnResumedFromLocalNotification(event:CMLocalNotificationEvent):void
		{
			Main.Instance.Session.Rare.IncreaseLocalNotificationCounterOnResumed();
			Main.Instance.XServices.GoogleAnalytics.TrackOnLocalNotificationOnResumed(event.extra.localNotificationID);
			dispatchEvent(new LocalNotificationsEvent(LocalNotificationsEvent.RESUMED_FROM_LOCAL_NOTIFICATION, event.title, event.message, event.extra, event.extraJson));
			switch (event.extra.localNotificationID)
			{
				case LOCAL_NOTIFICATION_TIMER_BONUS_ID:
					NotificationsHandler.Instance.ShowTimerBonusLocalNotificationPopup();
					break;
				default:
					break;
			}
			SetNotificationBadgeNumber(0);
		}
	}
}