package com.slotim.slotim.utils.xServices.parse
{
	import com.slotim.slotim.data.Main;
	import com.slotim.slotim.data.application.prices.ProductID;
	import com.slotim.slotim.utils.consts.Consts;
	import com.slotim.slotim.utils.xServices.parse.base.BaseParseHandler;
	
	import flash.system.Capabilities;
	
	public class AppParseHandler extends BaseParseHandler
	{
		// consts
		public static const REST_URL:String = 'https://api.parse.com/1/';
		public static const CONTENT_TYPE:String = 'application/json';
		
		// properties
		public override function get ApplicationID():String
		{
			return Consts.PARSE_APPLICATION_ID;
		}
		public override function get RestApiKey():String
		{
			return Consts.PARSE_REST_API_KEY;
		}
		
		// class
		public function AppParseHandler()
		{
		}
		
		// methods
		public function RegisterDevice(success:Function, error:Function):void
		{
			try
			{
				Post('Devices', {
					deviceID: Main.Instance.XServices.DeviceID.DeviceID,
					os: Capabilities.os,
					deviceWidth: Main.Instance.Device.DeviceRectangle.width,
					deviceHeight: Main.Instance.Device.DeviceRectangle.height,
					deviceType: Main.Instance.Device.DeviceType,
					version: Main.Instance.CurrentVersion}, success, error);
			}
			catch (error:Error)
			{
			}
		}
		
		public function FacebookConnect():void
		{
			try
			{
				Post('Facebook', {
					deviceID: Main.Instance.XServices.DeviceID.DeviceID,
					os: Capabilities.os,
					facebookUid: Main.Instance.XServices.Social.FacebookUid,
					facebookName: Main.Instance.XServices.Social.FacebookName,
					facebookAccessToken: Main.Instance.XServices.Social.FacebookAccessToken},
					null, null);
			}
			catch (error:Error)
			{
			}
		}
		
		public function PurchaseComplete(_productID:String, _chips:int):void
		{
			try
			{
				Post('Purchase', {
					deviceID: Main.Instance.XServices.DeviceID.DeviceID,
					os: Capabilities.os,
					facebookUid: Main.Instance.XServices.Social.FacebookUid,
					facebookName: Main.Instance.XServices.Social.FacebookName,
					facebookAccessToken: Main.Instance.XServices.Social.FacebookAccessToken,
					balance: Main.Instance.Session.Wallet.GetBalance,
					xp: Main.Instance.Session.Wallet.GetXP,
					level: Main.Instance.Session.Wallet.GetLevel.LevelNumber,
					localTime: Main.Instance.XServices.InternetTime.CurrentLocalDateTime,
					chips: _chips,
					productID: _productID,
					productDescription: ProductID.DescriptionByProductID(_productID),
					productPrice: ProductID.PriceByProductID(_productID)},
					null, null);
			}
			catch (error:Error)
			{
			}
		}
	}
}
