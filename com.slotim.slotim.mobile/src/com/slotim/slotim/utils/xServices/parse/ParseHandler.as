package com.slotim.slotim.utils.xServices.parse
{
	public class ParseHandler
	{
		// members
		private var _app:AppParseHandler;
		
		// properties
		public function get App():AppParseHandler
		{
			return _app;
		}
		
		// class
		public function ParseHandler()
		{
			_app = new AppParseHandler();
		}
	}
}