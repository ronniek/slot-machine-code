package com.slotim.slotim.utils.xServices.parse.base
{
	import com.slotim.slotim.utils.error.MustOverrideError;
	
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	
	public class BaseParseHandler
	{
		// consts
		public static const REST_URL:String = 'https://api.parse.com/1/';
		public static const CONTENT_TYPE:String = 'application/json';
		
		// members
		private var User:Object =
			{
				Delete: function (objectId:String, success:Function = null, error:Function = null):void
				{
					var url:String = REST_URL + 'users/' + objectId;
					Call(url, URLRequestMethod.DELETE, null, null, success, error);
				},
				Get: function (objectId:String, success:Function = null, error:Function = null):void
				{
					var url:String = REST_URL + 'users/' + objectId;
					Call(url, URLRequestMethod.GET, null, null, success, error);
				},
				Post: function (params:Object, success:Function = null, error:Function = null):void
				{
					var url:String = REST_URL + 'users';
					Call(url, URLRequestMethod.POST, params, null, success, error);				
				},
				Put: function (objectId:String, params:Object, success:Function = null, error:Function = null):void
				{
					var url:String = REST_URL + 'users/' + objectId;
					Call(url, URLRequestMethod.PUT, params, null, success, error);
				},
				Search: function (where:Object, success:Function = null, error:Function = null):void
				{
					var url:String = REST_URL + 'users';
					Call(url, URLRequestMethod.GET, null, where, success, error);
				}
			}
		
		// properties
		public function get ApplicationID():String
		{
			throw new MustOverrideError();
		}
		public function get RestApiKey():String
		{
			throw new MustOverrideError();
		}
		
		// class
		public function BaseParseHandler()
		{
		}
		
		// methods
		protected function Delete(className:String, objectId:String, success:Function = null, error:Function = null):void
		{
			var url:String = REST_URL + 'classes/' + className + '/' + objectId;
			Call(url, URLRequestMethod.DELETE, null, null, success, error);
		}
		protected function Retrieve(className:String, objectId:String, success:Function = null, error:Function = null):void
		{
			var url:String = REST_URL + 'classes/' + className + '/' + objectId;
			Call(url, URLRequestMethod.GET, null, null, success, error);
		}
		protected function Query(className:String, params:Object = null, where:Object = null, success:Function = null, error:Function = null):void
		{
			var url:String = REST_URL + 'classes/' + className;
			Call(url, URLRequestMethod.GET, params, where, success, error);
		}
		protected function Post(className:String, params:Object = null, success:Function = null, error:Function = null):void
		{
			var url:String = REST_URL + 'classes/' + className;
			Call(url, URLRequestMethod.POST, params, null, success, error);
		}
		protected function Put(className:String, objectId:String, params:Object = null, success:Function = null, error:Function = null):void
		{
			var url:String = REST_URL + 'classes/' + className + '/' + objectId;
			Call(url, URLRequestMethod.PUT, params, null, success, error);
		}
		protected function SignIn(username:String, password:String, success:Function = null, error:Function = null):void
		{
			var params:Object = {username: username, password: password};
			var url:String = REST_URL + 'login';
			Call(url, URLRequestMethod.GET, params, null, success, error);
		}
		protected function SignUp(params:Object, success:Function = null, error:Function = null):void
		{
			User.Post(params, success, error);
		}
		protected function ResetPassword(email:String, success:Function = null, error:Function = null):void
		{
			var params:Object = {email: email};
			var url:String = REST_URL + 'requestPasswordReset';
			Call(url, URLRequestMethod.POST, params, null, success, error);
		}
		
		protected function Batch(requests:Array, success:Function, error:Function):void
		{
			var params:Object = {requests: requests};
			var url:String = REST_URL + 'batch';
			Call(url, URLRequestMethod.POST, params, null, success, error);
		}
		
		// methods
		private function Call(url:String, method:String, params:Object = null, where:Object = null, successFunction:Function = null, errorFunction:Function = null):void
		{
			try
			{
				var urlLoader:URLLoader = new URLLoader();
				var urlRequest:URLRequest = new URLRequest(url);
				
				if (where != null)
				{
					if (params == null)
					{
						params = {};
					}
					params['where'] = JSON.stringify(where); 
				}
				
				if (params != null) { 
					if (method == URLRequestMethod.GET)
					{
						var vars:URLVariables = new URLVariables();
						for (var p:String in params)
						{
							vars[p] = params[p];
						}
						urlRequest.data = vars;
					}
					else
					{ 
						urlRequest.data = JSON.stringify(params); 
					}
				}
				urlRequest.contentType = CONTENT_TYPE;
				urlRequest.cacheResponse = false;
				urlRequest.method = method;
				urlRequest.useCache = false;
				urlRequest.requestHeaders.push(new URLRequestHeader('X-Parse-Application-Id', ApplicationID));
				urlRequest.requestHeaders.push(new URLRequestHeader('X-Parse-REST-API-Key', RestApiKey));
				
				if (successFunction != null)
				{
					urlLoader.addEventListener(Event.COMPLETE, function (event:Event):void
					{
						var data:Object = JSON.parse(event.target.data);
						successFunction(data);
					}, false, 0, true);
				}
				if (errorFunction != null)
				{
					urlLoader.addEventListener(IOErrorEvent.IO_ERROR, errorFunction, false, 0, true);
				}
				
				urlLoader.load(urlRequest);
			}
			catch (error:Error)
			{
				errorFunction(null);
			}
		}
	}
}